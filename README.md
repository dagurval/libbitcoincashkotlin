![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/bitcoinunlimited/libbitcoincashkotlin/master)
![License MIT](https://img.shields.io/badge/license-MIT-blue)

# libbitcoincashkotlin — Kotlin library for Bitcoin Cash

Bitcoin Cash library for Kotlin developers and with Android support. The
library links directly with the popular [Bitcoin Unlimited](https://gitlab.com/bitcoinunlimited/BCHUnlimited) full
node to provide efficient and consensus compatible features (via libbitcoincash.so).

Our philosophy is *batteries included*, meaning it is intended to come with
a large set of features to cover all needs for Bitcoin Cash application developers.

### [Reference documentation](https://bitcoinunlimited.gitlab.io/libbitcoincashkotlin/dokka/)

## Use in Android app

Add this repository to build.gradle

```
allprojects {
    repositories {
       maven {
            url "https://bitcoinunlimited.gitlab.io/libbitcoincashkotlin/repos/"
        }
    }
}
```

Then add as a dependency

```
    implementation "info.bitcoinunlimited:libbitcoincash:0.4.1"
```

## Building the library

It is not required to build the library to use it. It suffices to add it as a
depedency. If you want to improve on the library itself, you'll need build it.

To build, run:

`./gradlew build`

To enable experimental ring signature support, add `ringsignatures=true'
to `local.properties` file in the project root first.

### Build dependencies

#### Debian/Ubuntu

```bash
apt-get install cmake python3 libtool automake build-essential ninja-build
```

Install cmake 3.12 or greater.  This installs a modern cmake from kitware.

```bash
wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | sudo tee /etc/apt/trusted.gpg.d/kitware.gpg >/dev/null
sudo apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main'
sudo apt-get update
sudo apt-get install cmake
cmake --version
```

```
cmake version 3.18.4
CMake suite maintained and supported by Kitware (kitware.com/cmake).
```

#### Mac
```bash
brew install cmake ninja
```

### Ring signature support

Building with experimental ring signatures requires Rust. Install from
https://rustup.rs/

For support for cross-compiling for android:
```
cargo install cbindgen
rustup target add \
    aarch64-linux-android \
    armv7-linux-androideabi \
    i686-linux-android \
    x86_64-linux-android
````

### Android SDK Installation

- Go to https://developer.android.com/studio/index.html
- Find section "Command Line Tools only"
- Download `commandlinetools-<platform>-XXXXXXX_latest.zip`
- Unzip and set envoronment var `ANDROID_SDK_ROOT` to the unzipped folder

### Android SDK Licenses

You'll need to accept a bunch of licenses in order to proceed. Run
`./accept-licenses.sh`.

### Android NDK Installation

- Download version r21b from https://developer.android.com/ndk/downloads
    - For Linux, the direct URL is: https://dl.google.com/android/repository/android-ndk-r21b-linux-x86_64.zip
- Unzip and set environment var `ANDROID_NDK_ROOT` to the unzipped folder

## Troubleshooting

### Cannot accept NDK license

sdkmanager does not support Java 11 and you either need to downgrade to older
version of Java, or hack it to use older version.
See: https://github.com/flutter/flutter/issues/16025#issuecomment-468009198

### UnsatisfiedLinkError

Error similar to
```
    java.lang.UnsatisfiedLinkError: No implementation found for byte[] bitcoinunlimited.libbitcoincash.Hash.sha256(byte[]) (tried Java_bitcoinunlimited_libbitcoincash_Hash_sha256 and Java_bitcoinunlimited_libbitcoincash_Hash_sha256___3B)
```

Solution: Load and initialize the library:
```
System.loadLibrary("bitcoincashandroid")
Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
```

### java.lang.NullPointerException (no error message)

Error similar to
```
/Users/xxxx/libbitcoincashkotlin/libbitcoincash/src/main/cpp/CMakeLists.txt : C/C++ debug|armeabi-v7a : null

Task :libbitcoincash:generateJsonModelDebug in libbitcoincash Finished
:libbitcoincash:generateJsonModelDebug (Thread[Execution worker for ':',5,main]) completed. Took 0.135 secs.

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':libbitcoincash:generateJsonModelDebug'.
> java.lang.NullPointerException (no error message)
```

... could be a result of incompatible cmake version. The above error happens with cmake 3.20.

Solution: Dowload cmake 3.19.7 and set `cmake.dir` in `local.properties`.
"local.properties" is a file located in your project root directory that specifies variables specific to your environment.
Another option is to set cmake.dir to your distribution's installed cmake (for blah/bin/cmake, use cmake.dir=blah).
This is an example file:


```
## This file must *NOT* be checked into Version Control Systems,
# as it contains information specific to your local configuration.
#
# Location of the SDK. This is only used by Gradle.
# For customization when using a Version Control System, please read the
# header note.

sdk.dir=/**WHERE YOU INSTALLED ANDRIOD SDK**/Android/Sdk
cmake.dir=/usr
```

### org.jetbrains.kotlin.resolve.lazy.NoDescriptorForDeclarationException: Descriptor wasn't found for declaration SCRIPT

A kotlin file has file extension `kts` when it should have `kt`

### Caused by: java.lang.IllegalAccessError: class org.jetbrains.kotlin.kapt3.base.KaptContext (in unnamed module @0x7e98a1ee) cannot access class com.sun.tools.javac.util.Context (in module jdk.compiler) because module jdk.compiler does not export com.sun.tools.javac.util to unnamed module @0x7e98a1ee

Kapt is not compatible with JDK 16+. See https://youtrack.jetbrains.com/issue/KT-45545 for workarounds.

### Task :libbitcoincash:kaptDebugKotlin FAILED java.lang.reflect.InvocationTargetException (no error message)

Kapt has compatibility issues with Java 16+. See https://youtrack.jetbrains.com/issue/KT-45545

Workaround for Java 16 is to add `--illegal-access=permit` to
`org.gradle.jvmargs` in `gradle.properties`.
