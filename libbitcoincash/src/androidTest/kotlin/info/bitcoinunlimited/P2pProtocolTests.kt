package info.bitcoinunlimited.libbitcoincash

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import bitcoinunlimited.libbitcoincash.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import org.junit.Test
import org.junit.Before
import java.math.BigDecimal
import java.util.concurrent.Executors
import java.util.logging.Logger
import kotlin.coroutines.CoroutineContext
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import wf.bitcoin.javabitcoindrpcclient.BitcoinJSONRPCClient

val REGTEST_RPC_USER="z"
val REGTEST_RPC_PASSWORD="z"
val REGTEST_RPC_PORT=19444


class P2pProtocolTests
{
    companion object
    {
        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHREGTEST.v)
        }
    }

    @Before
    fun beforeMethod() {
        val coCtxt: CoroutineContext = Executors.newFixedThreadPool(2).asCoroutineDispatcher()
        val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)
        val coCond = CoCond<Boolean>(coScope)
        val cnxn = try
        {
            BCHp2pClient(ChainSelector.BCHREGTEST, EMULATOR_HOST_IP, BCHregtestPort, "regtest@${EMULATOR_HOST_IP}",coScope,coCond).connect()
        } catch (e: java.net.ConnectException) {
            LogIt.warning("Skipping P2P tests: ${e}")
            null
        }

        cnxn?.let {
            it.close()
        }
        org.junit.Assume.assumeTrue(cnxn != null)
    }

    @Test
    fun connectToP2P()
    {
        LogIt.info("This test requires a bitcoind full node running on regtest at ${EMULATOR_HOST_IP}:${BCHregtestPort}")

        val coCtxt: CoroutineContext = Executors.newFixedThreadPool(2).asCoroutineDispatcher()
        val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)
        val coCond = CoCond<Boolean>(coScope)

        var cnxn = BCHp2pClient(ChainSelector.BCHREGTEST, EMULATOR_HOST_IP, BCHregtestPort, "regtest@${EMULATOR_HOST_IP}",coScope,coCond).connect()
        GlobalScope.launch { cnxn.processForever() }

        var txValResponses = 0

        cnxn.waitForReady()

        cnxn.send(NetMsgType.PING, BCHserialized(SerializationType.NETWORK) + 1L)

        val loc = BlockLocator()
        loc.add(Hash256())  // This will ask for the genesis block because no hashes will match
        val stop = Hash256()
        var headers: MutableList<BlockHeader>? = null
        val waiter = ThreadCond()
        cnxn.getHeaders(loc, stop, { lst, _  -> headers = lst; waiter.wake(); true})

        waiter.delay(5000)
        check(headers != null)  // If its null we didn't get the headers

        for (hdr in headers!!)
        {
            LogIt.info(hdr.height.toString() + " hash: " + hdr.hash.toHex() + " prev: " + hdr.hashPrevBlock.toHex())
        }

        var tx : BCHtransaction = BCHtransaction(ChainSelector.BCHREGTEST)
        cnxn.sendTxVal(tx) {
            s -> LogIt.info("tx Validation response: " + s)
            check("bad-txns-vin-empty" in s)
            check("txn-undersize" in s)
            txValResponses++
        }

        var rawAddr: ByteArray = byteArrayOf(20)
        tx.outputs.add(BCHoutput(10000, BCHscript.p2pkh(rawAddr, ChainSelector.BCHREGTEST)))
        cnxn.sendTxVal(tx) {
            s -> LogIt.info("tx2 Validation response: " + s)
            check("bad-txns-in-belowout" in s)
            txValResponses++
        }

        rawAddr = byteArrayOf(20)
        tx.inputs.add(BCHinput(ChainSelector.BCHREGTEST, BCHspendable(ChainSelector.BCHREGTEST, Hash256(), 1, 10000),BCHscript(ChainSelector.BCHREGTEST) + OP.TRUE))
        //tx.outputs[0] = BCHoutput(10000, BCHscript.p2pkh(rawAddr, ChainSelector.BCHREGTEST))
        cnxn.sendTxVal(tx) {
            s -> LogIt.info("tx3 Validation response: " + s)
            check("input-does-not-exist" in s)
            check("inputs-are-missing" in s)
            txValResponses++
        }

        // set up a quick way to get blocks back from a node
        var blk: BCHblock? = null
        val blkChannel = Channel<BCHblock>()
        cnxn.onBlockCallback.add({ incomingblk, client -> blkChannel.send(incomingblk) })

        // Spend an immature coinbase
        cnxn.sendGetData(listOf(Inv(Inv.Types.BLOCK, headers!![headers!!.size - 1].hash)))
        launch { blk = blkChannel.receive() }
        while(blk == null) Thread.sleep(100)

        // Spend the coinbase of a block that can't be spent yet
        tx.inputs[0] = BCHinput(ChainSelector.BCHREGTEST, BCHspendable(ChainSelector.BCHREGTEST, blk!!.txes[0].hash, 0, 5000000000),BCHscript(ChainSelector.BCHREGTEST) + OP.TRUE)
        //tx.outputs[0] = BCHoutput(10000, BCHscript.p2pkh(rawAddr, ChainSelector.BCHREGTEST))
        cnxn.sendTxVal(tx) {
            s -> LogIt.info("tx4 Validation response: " + s)
            check("bad-txns-premature-spend-of-coinbase" in s)
            txValResponses++
        }

        // Since I'm looking now at the coinbase 100 blocks ago, this must be spendable and unspent
        blk = null
        cnxn.sendGetData(listOf(Inv(Inv.Types.BLOCK, headers!![headers!!.size - 100].hash)))
        launch { blk = blkChannel.receive() }
        while(blk == null) Thread.sleep(100)

        // Spend the coinbase of a block that just matured, so we know it can't have been spent.  Spend too much to ensure that we still check sigs
        tx.outputs[0].amount = 10000000000
        tx.inputs[0] = BCHinput(ChainSelector.BCHREGTEST, BCHspendable(ChainSelector.BCHREGTEST, blk!!.txes[0].hash, 0, 5000000000),BCHscript(ChainSelector.BCHREGTEST) + OP.TRUE)
        //tx.outputs[0] = BCHoutput(10000, BCHscript.p2pkh(rawAddr, ChainSelector.BCHREGTEST))
        cnxn.sendTxVal(tx) {
            s -> LogIt.info("tx5 Validation response: " + s)
            check("mandatory-script-verify-flag-failed (Non-canonical DER signature)" in s)
            check("txn-undersize" in s)
            check("bad-txns-in-belowout" in s)
            txValResponses++
            waiter.wake()
        }

        waiter.delay(30000)

        if (txValResponses == 0) LogIt.info("TX validation request is being ignored.  DID YOU ENABLE IT IN THE FULL NODE?")
        check(txValResponses == 5)  // Make sure that
        LogIt.info("shutting down")
        cnxn.close()
        LogIt.info("TestCompleted")
    }

    @Test
    fun rpcAndWalletSetup()
    {
        LogIt.info("This test requires a bitcoind full node running on " +
                "regtest at ${EMULATOR_HOST_IP} and ports ${BCHregtestPort} and ${REGTEST_RPC_PORT}")

        // Set up RPC connection
        val rpcConnection = "http://" + REGTEST_RPC_USER + ":" + REGTEST_RPC_PASSWORD + "@" + SimulationHostIP + ":" + REGTEST_RPC_PORT
        LogIt.info("Connecting to: " + rpcConnection)
        var rpc = BitcoinJSONRPCClient(rpcConnection)
        var peerInfo = rpc.peerInfo

        // Generate blocks until we get coins to spend. This is needed inside the ci testing.
        // But the code checks first so that lots of extra blocks aren't created during dev testing
        var rpcBalance = rpc.getBalance()
        LogIt.info(rpcBalance.toPlainString())
        while (rpcBalance < BigDecimal(50))
        {
            rpc.generate(1)
            rpcBalance = rpc.getBalance()
        }

        // Set up connection between full and light nodes
        val coCtxt: CoroutineContext = Executors.newFixedThreadPool(2).asCoroutineDispatcher()
        val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)
        val coCond = CoCond<Boolean>(coScope)
        var client = BCHp2pClient(ChainSelector.BCHREGTEST, EMULATOR_HOST_IP, BCHregtestPort, "regtest@${EMULATOR_HOST_IP}",coScope,coCond).connect()
        GlobalScope.launch { client.processForever() }
        client.waitForReady()
        client.send(NetMsgType.PING, BCHserialized(SerializationType.NETWORK) + 1L)

        // Set up blockchain
        var applicationContext = ApplicationProvider.getApplicationContext<Context>()
        val ctxt = PlatformContext(applicationContext)
        val cnxnMgr: CnxnMgr = MultiNodeCnxnMgr("test" ?: "RBCH", ChainSelector.BCHREGTEST, arrayOf(EMULATOR_HOST_IP))
        val chain = Blockchain(
                ChainSelector.BCHREGTEST,
                "test" ?: "RBCH",
                cnxnMgr,
                // If checkpointing the genesis block, set the prior block id to the genesis block as well
                Hash256(),
                Hash256(),
                Hash256(),
                0,
                0.toBigInteger(),
                ctxt, dbPrefix
        )
        val chainThread = Thread {
            println("Starting blockchain")
            chain.start()
        }
        chainThread.start()

        // Set up wallet
        val kvp = OpenKvpDB(ctxt, "db")
        val wallet = Bip44Wallet(kvp!!, "test", ChainSelector.BCHREGTEST, "secret word")
        wallet.addBlockchain(chain, chain.checkpointHeight, 0)
        val walletThread = Thread {
            println("Starting wallet")
            wallet.run()
        }
        walletThread.start()

        // Generate new address
        val addr = wallet.getnewaddress()

        // Add address to bloom filter and send it to full node
        val data = Array<Any>(1, { Unit })
        data[0] = addr.data;
        val bloom = Wallet.CreateBloomFilter(data, 0.1, data.size * 10, Wallet.MAX_BLOOM_SIZE, Wallet.Companion.BloomFlags.BLOOM_UPDATE_ALL.v, 1)
        client.sendBloomFilter(bloom, 1)

        // Full node sends 1 satoshi to light
        val spend = BigDecimal(1)
        LogIt.info("Sending ${spend} to address ${addr.toString()}")
        rpc.sendToAddress(addr.toString(), spend)

        // Full node generates a block
        val latestBlock = rpc.generate(1).removeFirst()

        // Light client asks for header of new block
        val loc = BlockLocator()
        loc.add(Hash256())  // This will ask for the genesis block because no hashes will match
        val stop = Hash256(latestBlock)
        var headers: MutableList<BlockHeader>? = null
        val waiter = ThreadCond()
        client.getHeaders(loc, stop, { lst, _  -> headers = lst; waiter.wake(); true})

        waiter.delay(5000)

        check(headers != null)  // If its null we didn't get the headers

        for (hdr in headers!!)
        {
            LogIt.info(hdr.height.toString() + " hash: " + hdr.hash.toHex() + " prev: " + hdr.hashPrevBlock.toHex())
        }

        // Cleanup
        LogIt.info("shutting down")
        wallet.stop()
        chain.stop()
        client.close()
        walletThread.join()
        chainThread.join()
        LogIt.info("TestCompleted")
    }
}
