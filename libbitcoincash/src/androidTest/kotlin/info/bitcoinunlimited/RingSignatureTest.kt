package bitcoinunlimited.libbitcoincash
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class RingSignatureTest {

    @Before
    fun beforeMethod() {
        val localProperties = java.util.Properties()
        var runTests = true
        try {
            System.loadLibrary("ringsigandroid")
        } catch (e: java.lang.UnsatisfiedLinkError) {
            // Ring signature library has not been built. To build it and run
            // these tests, set `ringsignatures=true` in `local.properties`.
            runTests = false
        }

        // When `runTests` is false, the tests will be skipped.
        org.junit.Assume.assumeTrue(runTests)
    }

    @Test
    fun generateKey() {
        val keypair = RingSignature.newKeyPair()
        assertNotEquals(keypair, 0)
        RingSignature.freeKeyPair(keypair)
    }

    @Test
    fun generateKeyWithBits() {
        val seed1 = ByteArray(32, { 'A'.toByte() })
        val seed2 = ByteArray(32, { 'B'.toByte() })
        val keypair1 = RingSignature.newKeyPairFromBits(seed1)
        val keypair2 = RingSignature.newKeyPairFromBits(seed1)
        val keypair3 = RingSignature.newKeyPairFromBits(seed2)
        assertNotEquals(0, keypair1)
        assertNotEquals(0, keypair2)
        assertNotEquals(0, keypair3)

        assertTrue(
            RingSignature.getPrivKey(keypair1).contentEquals(
                RingSignature.getPrivKey(keypair1)
            )
        )

        assertTrue(
            RingSignature.getPrivKey(keypair1).contentEquals(
                RingSignature.getPrivKey(keypair2)
            )
        )

        assertFalse(
            RingSignature.getPrivKey(keypair1).contentEquals(
                RingSignature.getPrivKey(keypair3)
            )
        )

        RingSignature.freeKeyPair(keypair1)
        RingSignature.freeKeyPair(keypair2)
        RingSignature.freeKeyPair(keypair3)
    }

    @Test
    fun getPrivKey() {
        val keypair = RingSignature.newKeyPair()
        assertEquals(RingSignature.getPrivKey(keypair).size, 64)
        RingSignature.freeKeyPair(keypair)
    }

    @Test
    fun getPubKey() {
        val keypair = RingSignature.newKeyPair()
        val pubkey = RingSignature.getPubKey(keypair)
        assertEquals(pubkey.size, 32)
        RingSignature.freeKeyPair(keypair)
    }

    @Test
    fun createTag() {
        val keypair1 = RingSignature.newKeyPair()
        val keypair2 = RingSignature.newKeyPair()
        val tag = RingSignature.createTag(
            "Election #11".toByteArray(),
            arrayOf(
                RingSignature.getPubKey(keypair1),
                RingSignature.getPubKey(keypair2)
            )
        )
        assertNotEquals(tag, 0)
        RingSignature.freeTag(tag)
        RingSignature.freeKeyPair(keypair1)
        RingSignature.freeKeyPair(keypair2)
    }

    @Test
    fun sign() {
        val keypair1 = RingSignature.newKeyPair()
        val keypair2 = RingSignature.newKeyPair()
        val tag = RingSignature.createTag(
            "Election #11".toByteArray(),
            arrayOf(
                RingSignature.getPubKey(keypair1),
                RingSignature.getPubKey(keypair2)
            )
        )
        val signature: ByteArray = RingSignature.signMessage("YES".toByteArray(), tag, RingSignature.getPrivKey(keypair1))
        assertEquals(176, signature.size)
        RingSignature.freeTag(tag)
        RingSignature.freeKeyPair(keypair1)
        RingSignature.freeKeyPair(keypair2)
    }

    @Test
    fun verify() {
        val keypair1 = RingSignature.newKeyPair()
        val keypair2 = RingSignature.newKeyPair()
        val tag = RingSignature.createTag(
            "Election #11".toByteArray(),
            arrayOf(
                RingSignature.getPubKey(keypair1),
                RingSignature.getPubKey(keypair2)
            )
        )
        val message1 = "YES".toByteArray()
        val message2 = "NO".toByteArray()

        val signature1 = RingSignature.signMessage(message1, tag, RingSignature.getPrivKey(keypair1))
        val signature2 = RingSignature.signMessage(message2, tag, RingSignature.getPrivKey(keypair2))

        assertTrue(RingSignature.verify(message1, tag, signature1))
        assertTrue(RingSignature.verify(message2, tag, signature2))

        // Mixing signatures fails
        assertFalse(RingSignature.verify(message2, tag, signature1))
        assertFalse(RingSignature.verify(message1, tag, signature2))

        RingSignature.freeTag(tag)
        RingSignature.freeKeyPair(keypair1)
        RingSignature.freeKeyPair(keypair2)
    }

    @Test
    fun trace() {
        val keypair1 = RingSignature.newKeyPair()
        val keypair2 = RingSignature.newKeyPair()
        val tag = RingSignature.createTag(
            "Election #11".toByteArray(),
            arrayOf(
                RingSignature.getPubKey(keypair1),
                RingSignature.getPubKey(keypair2)
            )
        )
        val message1 = "YES".toByteArray()
        val message2 = "NO".toByteArray()

        val signature1 = RingSignature.signMessage(message1, tag, RingSignature.getPrivKey(keypair1))

        // Signing same message twice is exposed as linked
        do {
            val signature2 = RingSignature.signMessage(message1, tag, RingSignature.getPrivKey(keypair1))
            val (result, revealedPubkey) = RingSignature.trace(
                message1,
                signature1,
                message1,
                signature2,
                tag
            )

            assertEquals(TraceResult.Linked, result)
            assertEquals(null, revealedPubkey)
        } while (false)

        // Signing a distinct messages with same private key reveals pubkey
        do {
            val signature3 = RingSignature.signMessage(message2, tag, RingSignature.getPrivKey(keypair1))
            val (result, revealedPubkey) = RingSignature.trace(
                message1,
                signature1,
                message2,
                signature3,
                tag
            )
            assertEquals(TraceResult.Revealed, result)
            assertTrue(revealedPubkey != null)
        } while (false)

        do {
            // Independent signers
            val signature4 = RingSignature.signMessage(message1, tag, RingSignature.getPrivKey(keypair2))
            val (result, revealedPubkey) = RingSignature.trace(
                message1,
                signature1,
                message1,
                signature4,
                tag
            )
            assertEquals(TraceResult.Indep, result)
            assertTrue(revealedPubkey == null)
        } while (false)

        RingSignature.freeTag(tag)
        RingSignature.freeKeyPair(keypair1)
        RingSignature.freeKeyPair(keypair2)
    }
}