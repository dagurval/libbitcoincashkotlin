package info.bitcoinunlimited.libbitcoincash

import bitcoinunlimited.libbitcoincash.UtilStringEncoding
import bitcoinunlimited.libbitcoincash.UtilVote
import bitcoinunlimited.libbitcoincash.vote.TwoOptionVote
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test

class TwoOptionVoteTests {
    @Before
    fun loadlib() {
        System.loadLibrary("bitcoincashandroid")
    }

    @Test
    fun test_hash160_salted() {
        var salt = "unittest".toByteArray()
        var barhash = UtilVote.hash160Salted(salt, "Bar".toByteArray())
        var bazhash = UtilVote.hash160Salted(salt, "Baz".toByteArray())

        assertThat("7563cd871bc9af8f5bcd89a298f94d03135185f5")
            .isEqualTo(UtilStringEncoding.toHexString(barhash))
        assertThat("a7557a06649059c24c7f8b13b955a6550444aa24")
            .isEqualTo(UtilStringEncoding.toHexString(bazhash))
    }

    @Test
    fun test_calculate_proposal_id() {
        var salt = "unittest".toByteArray()
        var description = "Foo?"
        var optA = "Bar"
        var optB = "Baz"
        var endheight = 636821
        var participants = arrayOf(
            UtilStringEncoding.hexToByteArray("0e2f5d8a017c4983cb56320d18f66bf01587aa44"),
            UtilStringEncoding.hexToByteArray("3790bb07029831ec90f8eb1ed7c1c09aac178185"),
            UtilStringEncoding.hexToByteArray("aaa8a14f0658c44809580b24299a592d7623976c"),
            UtilStringEncoding.hexToByteArray("9b1772d9287b9a3587b0a1e147f34714697c780c"),
            UtilStringEncoding.hexToByteArray("ca9c60699d3d4c6b71b1fa5b71934feaf9f5a004"),
            UtilStringEncoding.hexToByteArray("f7d1358c4baa20b31e5f84c238e964473d733f75")
        )

        var id = TwoOptionVote.calculate_proposal_id(
            salt, description, optA, optB, endheight, participants
        )

        var hex = UtilStringEncoding.toHexString(id)
        assertThat(hex).isEqualTo("6a36bf0c5dd6e5e1daddf3ad67d4356a657ea994")
    }

    @Test
    fun testRedeemScript() {
        val contract = VoteTestUtil.twoOptionVoteContractInstances()

        assertThat(UtilStringEncoding.toHexString(contract[0].redeemScript()))
            .isEqualTo(
                (
                    "14485beda9a544ee01effccef8d2be1e2778f5997e14a7557a0" +
                        "6649059c24c7f8b13b955a6550444aa24147563cd871bc9af8f5bcd89a2" +
                        "98f94d03135185f5140e2f5d8a017c4983cb56320d18f66bf01587aa445" +
                        "479a988547a5479ad557a5579557abb537901147f75537a887b01147f77" +
                        "767b8778537a879b7c14beeffffffffffffffffffffffffffffffffffff" +
                        "f879b"
                    )
            )
    }

    @Test
    fun createVoteMessage() {
        val salt = "unittest".toByteArray()
        val id = UtilVote.hash160Salted(salt, "Bar".toByteArray())
        val vote = UtilVote.hash160Salted(salt, "Baz".toByteArray())
        val msg = TwoOptionVote.createVoteMessage(id, vote)
        assertThat(msg.size).isEqualTo(40)
        assertThat(msg.copyOfRange(0, 20)).isEqualTo(id)
        assertThat(msg.copyOfRange(20, 40)).isEqualTo(vote)
    }

    @Test
    fun signVoteMessage() {
        val salt = "unittest".toByteArray()
        val id = UtilVote.hash160Salted(salt, "Bar".toByteArray())
        val vote = UtilVote.hash160Salted(salt, "Baz".toByteArray())
        val msg = TwoOptionVote.createVoteMessage(id, vote)

        val secret = VoteTestUtil.mocPrivateKey()
        val signature = TwoOptionVote.signVoteMessage(secret, msg)

        // This framework produces a Schnorr signed signature
        assertThat(signature.size).isEqualTo(64)
    }
}