package info.bitcoinunlimited.libbitcoincash

import bitcoinunlimited.libbitcoincash.*
import bitcoinunlimited.libbitcoincash.vote.*

class VoteTestUtil {
    companion object {
        fun twoOptionVoteContractInstances(): List<TwoOptionVoteContract> {
            val salt = "unittest".toByteArray()
            val description = "Foo?"
            val optA = "Bar"
            val optB = "Baz"
            val optAHash = UtilVote.hash160Salted(salt, optA.toByteArray())
            val optBHash = UtilVote.hash160Salted(salt, optB.toByteArray())
            val endHeight = 642042
            val participants = arrayOf(
                UtilStringEncoding.hexToByteArray("0e2f5d8a017c4983cb56320d18f66bf01587aa44"),
                UtilStringEncoding.hexToByteArray("3790bb07029831ec90f8eb1ed7c1c09aac178185"),
                UtilStringEncoding.hexToByteArray("aaa8a14f0658c44809580b24299a592d7623976c"),
                UtilStringEncoding.hexToByteArray("9b1772d9287b9a3587b0a1e147f34714697c780c"),
                UtilStringEncoding.hexToByteArray("ca9c60699d3d4c6b71b1fa5b71934feaf9f5a004"),
                UtilStringEncoding.hexToByteArray("f7d1358c4baa20b31e5f84c238e964473d733f75")
            )

            val id = TwoOptionVote.calculate_proposal_id(
                salt, description, optA, optB, endHeight, participants
            )

            return participants.map { TwoOptionVoteContract(id, optAHash, optBHash, it) }
        }

        fun ringSignatureVoteInstances(): Pair<
            List<RingSignatureVote>,
            List<ByteArray>> {
            val salt = "unittest".toByteArray()
            val description = "Foo?"
            val optA = "Bar"
            val optB = "Baz"
            val optC = "Qux"
            val beginHeight = 100
            val endHeight = 200

            val participantSeeds = listOf(
                ByteArray(32) { _ -> 0xBE.toByte() },
                ByteArray(32) { _ -> 0xEF.toByte() },
                ByteArray(32) { _ -> 0xF0.toByte() },
                ByteArray(32) { _ -> 0x0D.toByte() },
            )

            val participants = participantSeeds.map {
                val keypair = RingSignature.newKeyPairFromBits(it)
                val pubkey = RingSignature.getPubKey(keypair)
                RingSignature.freeKeyPair(keypair)
                pubkey
            }.toTypedArray()

            val contract = RingSignatureVote(
                salt, description, beginHeight, endHeight,
                arrayOf(optA, optB, optC),
                participants
            )

            return Pair(listOf(contract), participantSeeds)
        }

        fun mocPrivateKey(fill: Char = 'A'): ByteArray = ByteArray(32) { _ -> fill.toByte() }

        fun mocPublicKey(secret: ByteArray = mocPublicKey()): ByteArray {
            return PayDestination.GetPubKey(secret)
        }

        fun toPKH(pubkey: ByteArray): ByteArray {
            if (pubkey.size != NetworkConstants.PUBLIC_KEY_COMPACT_SIZE) {
                error("Invalid public key size")
            }
            return Hash.hash160(pubkey)
        }

        fun p2pkhAddress(chain: ChainSelector, privateKey: ByteArray): PayAddress {
            val pkh = toPKH(PayDestination.GetPubKey(privateKey))
            return PayAddress(chain, PayAddressType.P2PKH, pkh)
        }

        fun mocInput(chain: ChainSelector, privateKey: ByteArray, amount: Long): BCHinput {
            var input = BCHinput(chain)
            input.spendable.priorOutScript = p2pkhAddress(chain, privateKey).outputScript()
            input.spendable.amount = amount
            input.spendable.secret = privateKey
            return input
        }

        fun outputToInput(
            chain: ChainSelector,
            tx: BCHtransaction,
            outputIndex: Int,
            secret: ByteArray
        ): BCHinput {
            var input = BCHinput(chain)
            var out: BCHoutput = tx.outputs[outputIndex]
            input.spendable.priorOutScript = out.script
            input.spendable.amount = out.amount
            input.spendable.outpoint = BCHoutpoint(tx.calcHash(), outputIndex.toLong())
            input.spendable.secret = secret
            return input
        }
    }
}