// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash

import java.lang.IndexOutOfBoundsException

val CHOP_SIZE: Int = 8192

open class DeserializationException(msg: String, shortMsg: String? = null) : BUException(msg, shortMsg, ErrorSeverity.Expected)


enum class SerializationType
{
    NETWORK, DISK, HASH, SCRIPTHASH, UNKNOWN
}

class Codec
{
    companion object
    {
        /** Convert a byte array to a string using 64 values per character.  This format is used in Bitcoin to convert message signatures to a printable representation */
        @JvmStatic
        external fun encode64(data: ByteArray): String

        /** Convert an encoded string (using 64 values per character) to bytes.  This format is used in Bitcoin to convert message signatures to a printable representation */
        @JvmStatic
        external fun decode64(data: String): ByteArray
    }
}

/** This class behaves like a function and its sole purpose is to wrap a bytearray into another class so that when we serialize it we know not to include the length */
class exactBytes(val data: ByteArray)

/** This class behaves like a function and its sole purpose is to wrap a bytearray into another class so that when we serialize it we know to include the length */
class variableSized(val data: ByteArray)

// Base class allowing derived classes to specify BCH serialization capability
// For deserialization, create a constructor that accepts a BCHSerialized object
open class BCHserializable()
{
    // Store this object into a BCHserialized stream and return it
    open fun BCHserialize(format: SerializationType = SerializationType.UNKNOWN): BCHserialized
    {
        throw NotImplementedError()
    }

    // Load this object with data from the passed stream
    open fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        throw NotImplementedError()
    }
}

fun Byte.toPositiveInt() = toInt() and 0xFF
fun Byte.toPositiveLong() = toLong() and 0xFF

// This class serializes or deserializes bitcoin cash data
class BCHserialized(_format: SerializationType) // = SerializationType.UNKNOWN)
{
    var data = MutableList<ByteArray>(0, { _ -> ByteArray(0) })
    var format: SerializationType = _format
    var deserPos: Int = 0
    var size: Int = 0

    constructor(contents: ByteArray, _format: SerializationType) : this(_format) // =SerializationType.UNKNOWN): this()
    {
        format = _format
        data.add(contents)
        size = contents.size
    }

    constructor(contents: MutableList<ByteArray>, _format: SerializationType) : this(_format)
    {
        add(contents)
    }

    constructor(contents: BCHserialized) : this(contents.format)
    {
        add(contents)
    }

    /** Return how many bytes are left to be read */
    fun availableBytes(): Int = size - deserPos

    // Converts the internal representation to a single ByteArray
    fun flatten(): ByteArray
    {
        if (data.size > 1)
            data = MutableList<ByteArray>(1, { _ -> data.join() })
        if (data.size == 0) return ByteArray(0)

        if (deserPos > CHOP_SIZE)
        {
            // reclaim memory by chopping off data that we've already consumed
            data[0] = data[0].sliceArray(deserPos..(data[0].size - 1))
            deserPos = 0
            size = data[0].size
        }
        return data[0]
    }

    // Serialize a 32 bit integer into this object
    fun addUint8(num: Int): BCHserialized
    {
        var data = ByteArray(1)
        data[0] = num.toByte()
        return add(data)
    }

    // Serialize a 32 bit integer into this object
    fun addInt32(num: Long): BCHserialized
    {
        var data = ByteArray(4)
        var cur = num
        for (i in 0..3)
        {
            data[i] = (cur and 0xFF).toByte()
            cur = cur ushr 8;
        }
        return add(data)
    }

    // Serialize a 32 bit integer into this object
    fun addInt32(num: Int): BCHserialized
    {
        var data = ByteArray(4)
        var cur = num
        for (i in 0..3)
        {
            data[i] = (cur and 0xFF).toByte()
            cur = cur ushr 8;
        }
        return add(data)
    }

    // Serialize a 64 bit integer into this object
    fun addUint64(num: Long): BCHserialized
    {
        var data = ByteArray(8)
        var cur = num
        for (i in 0..7)
        {
            data[i] = (cur and 0xFF).toByte()
            cur = cur ushr 8;
        }
        return add(data)
    }

    /** Append a serialized object to this serialized object */
    fun add(contents: BCHserializable) = add(contents.BCHserialize(this.format))

    /** Append a serialized object to this serialized object */
    fun addNullable(contents: BCHserializable?): BCHserialized
    {
        assert(format == SerializationType.DISK)  // The network formats doesn't support this
        if (contents == null) addUint8(0)
        else
        {
            addUint8(1)
            add(contents)
        }
        return this
    }

    /** Append a serialized object to this serialized object */
    fun add(contents: BCHserialized): BCHserialized
    {
        assert((contents.format == SerializationType.UNKNOWN) || (contents.format == this.format))
        // Need to copy anyway, might as well concat while doing so
        val content = contents.data.join()
        if (content.size == 0) return this
        this.add(content)
        this.size += content.size
        // add without compaction
        // contents.data.forEach { this.data.add(it); this.size += it.size }
        return this
    }

    /** Append a serialized object to this serialized object */
    fun plusAssign(contents: BCHserialized) = add(contents)

    fun add(contents: MutableList<ByteArray>): BCHserialized
    {
        contents.forEach { this.data.add(it); this.size += it.size }
        return this
    }

    // add some new data on to the end of this buffer
    fun add(newdata: ByteArray): BCHserialized
    {
        data.add(newdata)
        size += newdata.size
        return this
    }

    fun add(b: Boolean): BCHserialized
    {
        var data = byteArrayOf(0)
        if (b) data[0] = 1
        add(data)
        return this
    }


    // Start deserialization at a specified position (by default, restart)
    fun deserializeReset(pos: Int = 0)
    {
        deserPos = pos
    }

    /** Deserialize certain number of bytes as a ByteArray */
    fun debytes(len: Long): ByteArray
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val tmp = deserPos
        deserPos += len.toInt()
        try
        {
            return data[0].sliceArray(IntRange(tmp, (tmp + len - 1).toInt()))
        }
        catch (e: IndexOutOfBoundsException)
        {
            throw DeserializationException(e.toString())
        }
    }

    fun deboolean(): Boolean
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        val v: Byte = buf[deserPos]
        deserPos += 1
        return v != 0.toByte()
    }

    /** Deserialize an 8 bit integer */
    fun deuint8(): Byte
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        val ret: Byte = buf[deserPos]
        deserPos += 1
        return ret
    }

    /** Deserialize a character */
    fun dechar(): Char
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        val ret: Byte = buf[deserPos]
        deserPos += 1
        return Char(ret.toInt())
    }

    /** Deserialize a 16 bit integer */
    fun deuint16(): Int
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        var ret: Int = 0
        for (i in 1 downTo 0)
        {
            val piece = buf[deserPos + i].toPositiveInt()
            ret = (ret shl 8) or piece

        }
        deserPos += 2
        return ret
    }

    @OptIn(kotlin.ExperimentalUnsignedTypes::class)
      /** Deserialize a 16 bit BigEndian unsigned integer */
    fun deBigEndianUint16(): UInt
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        var ret: UInt = (buf[deserPos].toUByte().toUInt() shl 8) + buf[deserPos + 1].toUByte()
        deserPos += 2
        return ret
    }

    // Deserialize a 32 bit integer
    fun deint32(): Int
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        var ret: Int = 0
        for (i in 3 downTo 0)
        {
            val piece = buf[deserPos + i].toPositiveInt()
            ret = (ret shl 8) or piece

        }
        deserPos += 4
        return ret
    }

    // Deserialize a 32 bit unsigned integer -- must be returned as a long since kotlin integers
    // are signed
    fun deuint32(): Long
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        var ret: Long = 0
        for (i in 3 downTo 0)
        {
            val piece = buf[deserPos + i].toPositiveLong()
            ret = (ret shl 8) or piece

        }
        deserPos += 4
        return ret
    }

    // Deserialize a 64 bit unsigned integer
    fun deuint64(): Long
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        var ret: Long = 0
        for (i in 7 downTo 0)
        {
            val piece = buf[deserPos + i].toPositiveLong()
            ret = (ret shl 8) or piece

        }
        deserPos += 8
        return ret
    }

    // Deserialize a 64 bit signed integer
    fun deint64(): Long = deuint64()

    // Deserialize a Bitcoin-style "compact" integer
    fun decompact(): Long
    {
        assert(data.size == 1)  // For now object must be flattened before deserialization
        val buf = data[0]
        var sz: Int = buf[deserPos].toPositiveInt()
        deserPos++
        if (sz < 253)
        {
            return sz.toLong()
        }
        if (sz == 253)
        {
            return deuint16().toLong()
        }
        if (sz == 254)
        {
            return deuint32().toLong()
        }
        else return deuint64()
    }

    fun deString(): String
    {
        val length = decompact()
        return String(debytes(length))
    }

    /** Deserialize a list of some object.  You must pass a factory function that takes a buffer
    and returns an instance of the object (consuming some of the buffer) */
    fun <T> delist(Tfactory: (BCHserialized) -> T): MutableList<T>
    {
        val len = decompact().toInt()
        var ret = MutableList<T>(len, { _ -> Tfactory(this) })
        return ret
    }

    /** Deserialize a map of key value pair objects.  You must pass 2 factory functions that each take a buffer
    and returns an instance of the key or value object (consuming some of the buffer) */
    fun <K, V> demap(Kfactory: (BCHserialized) -> K, Vfactory: (BCHserialized) -> V): MutableMap<K, V>
    {
        val len = decompact().toInt()
        var ret: MutableMap<K, V> = mutableMapOf()
        var count = 0
        while (count < len)
        {
            count += 1
            val k = Kfactory(this)
            val v = Vfactory(this)
            ret[k] = v
        }
        return ret
    }


    /** Deserialize a variable length array (vector) of bytes */
    fun deByteArray(): ByteArray
    {
        val len = decompact().toLong()
        var ret = debytes(len)
        return ret
    }

/*
    fun <BCHSerializable> delist(): MutableList<BCHSerializable>
    {
        val len = decompact().toInt()
        var ret = MutableList<BCHSerializable>(len, { ???????? })
        return ret
    }
    */

    companion object
    {
        //val SER_NETWORK = (1 shl 0)
        //val SER_DISK = (1 shl 1)
        //val SER_GETHASH = (1 shl 2)

        fun <T> list(lst: List<T>, Tserializer: (T) -> BCHserialized, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(lst.size, format)
            for (i in lst)
            {
                ret.add(Tserializer(i))
            }
            return ret
        }

        fun list(lst: List<BCHserializable>, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(lst.size, format)
            for (i in lst)
            {
                ret.add(i.BCHserialize(format))
            }
            return ret
        }

        /** Serializes a map object with the provided key and value serialization functions */
        fun <K, V> map(mp: Map<K, V>, Kserializer: (K) -> BCHserialized, Vserializer: (V) -> BCHserialized, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(mp.size, format)
            for (i in mp)
            {
                ret.add(Kserializer(i.key))
                ret.add(Vserializer(i.value))
            }
            return ret
        }

        /** Serializes a map object that contains serializable keys and values */
        fun map(mp: Map<out BCHserializable, out BCHserializable>, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(mp.size, format)
            for (i in mp)
            {
                ret.add(i.key.BCHserialize(format))
                ret.add(i.value.BCHserialize(format))
            }
            return ret
        }

        /** Serializes a map object that contains serializable keys and values */
        fun mutableMap(mp: MutableMap<out BCHserializable, out BCHserializable>, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var ret = BCHserialized.compact(mp.size, format)
            for (i in mp)
            {
                ret.add(i.key.BCHserialize(format))
                ret.add(i.value.BCHserialize(format))
            }
            return ret
        }


        // Serialize into a 32 bit integer
        fun int32(num: Long, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var data = ByteArray(4)
            var cur = num
            for (i in 0..3)
            {
                data[i] = (cur and 0xFF).toByte()
                cur = cur ushr 8;
            }
            return BCHserialized(data, format)
        }

        fun int32(num: Int, format: SerializationType = SerializationType.UNKNOWN): BCHserialized = int32(num.toLong(), format)

        // Serialize into a 16 bit integer
        fun uint16(num: Long, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            var data = ByteArray(2)
            var cur = num
            for (i in 0..1)
            {
                data[i] = (cur and 0xFF).toByte()
                cur = cur ushr 8;
            }
            return BCHserialized(data, format)
        }

        fun boolean(b: Boolean): BCHserialized
        {
            var data = byteArrayOf(0)
            if (b) data[0] = 1
            return BCHserialized(data, SerializationType.UNKNOWN)
        }

        /** Serialize into an 8 bit number */
        fun uint8(num: Long): BCHserialized
        {
            var data = ByteArray(1)

            data[0] = (num and 0xFF).toByte()
            return BCHserialized(data, SerializationType.UNKNOWN)
        }

        /** Serialize into an 8 bit number */
        fun uint8(num: Byte, format: SerializationType = SerializationType.UNKNOWN): BCHserialized = BCHserialized(byteArrayOf(num), format)

        /** Serialize into a 32 bit unsigned number */
        fun uint32(num: Long, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            return int32(num, format)
        }

        /** Serialize a bitcoin-style "compact" integer */
        fun compact(num: Long, format: SerializationType = SerializationType.UNKNOWN): BCHserialized
        {
            if (num < 253)
            {
                return uint8(num.toByte(), format)
            }
            if (num < 0x10000)
            {
                return uint8(253.toByte(), format) + uint16(num)
            }
            if (num < 0x100000000)
            {
                return uint8(254.toByte(), format) + uint32(num)
            }
            return uint8(255.toByte()).addUint64(num)
        }

        fun compact(num: Int, format: SerializationType = SerializationType.UNKNOWN): BCHserialized = compact(num.toLong(), format)
    }

    /** Return a hex string of this serialized data */
    fun ToHex(): String
    {
        val ret = StringBuilder()
        for (array in data)
        {
            for (b in array)
            {
                //ret.append(String.format("%02x", b))
                ret.append(hexRep[b.toUint() shr 4])
                ret.append(hexRep[b.toUint() and 0xf])
            }
        }
        return ret.toString()
    }

    /** Return a new serialization object that contains this and the passed parameter */
    operator fun plus(b: exactBytes): BCHserialized
    {
        var ret = BCHserialized(this)
        ret.add(b.data)
        return ret
    }

    /** Return a new serialization object that contains this and the passed parameter serialized as a 64 bit number */
    @OptIn(kotlin.ExperimentalUnsignedTypes::class)
    operator fun plus(num: Long): BCHserialized = plus(num.toULong())

    /** Return a new serialization object that contains this and the passed parameter serialized as a 64 bit number */
    @OptIn(kotlin.ExperimentalUnsignedTypes::class)
    operator fun plus(num: ULong): BCHserialized
    {
        var data = ByteArray(8)
        var cur = num
        for (i in 0..7)
        {
            data[i] = (cur and 0xFF.toULong()).toByte()
            cur = cur shr 8;
        }
        var ret = BCHserialized(this)
        ret.data.add(data)
        return ret

    }

    /** Return a new serialization object that contains this and the passed parameter serialized as a 32 bit number */
    operator fun plus(num: Int): BCHserialized
    {
        return BCHserialized(this) + BCHserialized.int32(num.toLong(), this.format)
    }

    /** Return a new serialization object that contains this and the passed parameter serialized as a 32 bit number */
    @OptIn(kotlin.ExperimentalUnsignedTypes::class)
    operator fun plus(num: UInt): BCHserialized
    {
        return BCHserialized(this) + BCHserialized.uint32(num.toLong(), this.format)
    }

    /** Return a new serialization object that contains this and the passed parameter serialized as a 32 bit number */
    operator fun plus(value: Boolean): BCHserialized
    {
        return BCHserialized(this) + BCHserialized.uint8(if (value == true) 1 else 0, this.format)
    }

    /** Construct containing a serialized string */
    constructor(s: String, format: SerializationType) : this(format)
    {
        this += s
    }

    /** Append a serialized string to this object */
    operator fun plusAssign(s: String): Unit
    {
        add(s)
    }

    /** Append a serialized string to this object */
    fun add(s: String): BCHserialized
    {
        add(compact(s.length))
        add(s.toByteArray())
        return this
    }

    /** Append a single character to this object */
    fun add(c: Char): BCHserialized
    {
        data.add(byteArrayOf(c.code.toByte()))
        size += 1
        return this
    }

    /** Return a new serialization object that contains this and the passed parameter serialized */
    operator fun plus(s: String): BCHserialized
    {
        var ret = BCHserialized(this)
        ret.plusAssign(s)
        return ret
    }

    /** Return a new serialization object that contains this and the passed parameter serialized */
    operator fun plus(s: variableSized): BCHserialized
    {
        var ret = BCHserialized(this)
        ret.add(compact(s.data.size))
        ret.add(s.data)
        return ret
    }

    /** Return a new serialization object that contains this and the passed parameter */
    operator fun plus(b: BCHserialized): BCHserialized
    {
        assert((b.format == SerializationType.UNKNOWN) || (b.format == this.format))
        var ret = BCHserialized(this)
        ret.add(b)
        return ret
    }

    /** Return a new serialization object that contains this and the passed parameter */
    operator fun plus(b: BCHserializable): BCHserialized = this + b.BCHserialize(format)

    /** Return a new serialization object that contains this and the serialization of an array of serializable objects */
    operator fun plus(b: Array<out BCHserializable>): BCHserialized
    {
        var ret = BCHserialized(this)
        ret = ret + BCHserialized.compact(b.size.toLong())
        for (i in b)
        {
            ret = ret + i.BCHserialize(format)
        }
        return ret
    }

    // Return a new serialization object that contains this and the serialization of
    // a MutableList of serializable objects
    operator fun plus(b: MutableList<out BCHserializable>): BCHserialized
    {
        var ret = BCHserialized(this)
        ret = ret + BCHserialized.compact(b.size.toLong())
        for (i in b)
        {
            ret = ret + i.BCHserialize(format)
        }
        return ret
    }


}

/** Deserialize any object that is nullable, but prepending a byte indicating whether this object is null or not.  This API should only be used for DISK serialization because this nullable technique is not part of the network protocol */
fun <T : BCHserializable> BCHserialized.deNullable(Tfactory: (BCHserialized) -> T): T?
{
    assert(this.format == SerializationType.DISK)
    if (deuint8() == 0.toByte()) return null
    val ret = Tfactory(this)
    return ret
}

