package bitcoinunlimited.libbitcoincash

class NetworkConstants {
    companion object {
        /**
         * As defined in policy/policy.cpp in Bitcoin Unlimited
         */
        const val MAX_TX_IN_SCRIPT_SIG_SIZE: Int = 1650

        /**
         * Size of a Schnorr signature in a Bitcoin Cash transaction.
         */
        const val SCHNORR_SIGNATURE_SIZE: Int = 65

        /**
         * Size of a public key (compact representation)
         */
        const val PUBLIC_KEY_COMPACT_SIZE: Int = 33

        /**
         * The minimum output amount (as defined in policy/policy.h in Bitcoin Unlimited)
         */
        const val DEFAULT_DUST_THRESHOLD: Int = 546

        /**
         * Size of a transaction input with an empty script.
         */
        const val EMPTY_TX_INPUT_SIZE: Int = (
            32 + /* txid */
                4 + /* vout */
                2 + /* scriptsig size varint */
                0 + /* scriptsig */
                4 /* sequence */
            )

        /**
         * Size of a P2PKH input signed using Schnorr
         */
        const val P2PKH_SCHNORR_INPUT_SIZE: Int = (
            EMPTY_TX_INPUT_SIZE +
                1 + /* extra byte for the scriptsig size varint */
                99 /* scriptsig: push <signature 64 bytes> push <pubkey 33 bytes> */
            )

        /**
         * Size of a P2PKH output
         */
        const val P2PKH_OUTPUT_SIZE: Int = (
            8 + /* value */
                2 + /* scriptPubKey size */
                25 /* ScriptPubKey: OP_DUP OP_HASH160 push <hash> OP_EQUALVERIFY OP_CHECKSIG */
            )

        /**
         * Size of a P2SH output
         */
        const val P2SH_OUTPUT_SIZE: Int = (
            8 + /* value */
                2 + /* scriptPubKey size */
                23 /* ScriptPubKey: OP_HASH160 push <hash> OP_EQUAL */
            )
    }
}