// Copyright (c) 2021 Bitcoin Unlimited
// Distributed under the MIT software license, see the accompanying file
// COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash

class PayloadContractDestination(
    val chain: ChainSelector,
    override val secret: Secret,
    val payload: ByteArray
) : PayDestination(chain) {

    override val pubkey = GetPubKey(secret.getSecret())
    val payloadHash: ByteArray
        get() = PayloadContract.calcPayloadHash(pubkey, payload)

    override fun redeemScript(): BCHscript =
        PayloadContract.redeemScript(chain, pubkey, payloadHash)

    override fun outputScript(): BCHscript =
        PayloadContract.lockingScript(chain, pubkey, payloadHash)

    /**
     *  @param [params] Provide a single parameter which is the signature
     *                  derived from [secret] of the transaction that is
     *                  including this spend script
     */
    override fun spendScript(vararg params: ByteArray): BCHscript =
        PayloadContract.unlockingScript(chain, params[0], pubkey, payload)

    override val address: PayAddress
        get() = PayloadContract.contractAddress(chain, pubkey, payloadHash)
}