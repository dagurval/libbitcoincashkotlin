// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash

import kotlinx.coroutines.*
import kotlinx.serialization.json.jsonObject
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import java.lang.IllegalStateException
import java.lang.Math.ceil
import java.lang.NumberFormatException
import java.lang.System.currentTimeMillis
import java.math.BigDecimal
import java.security.SecureRandom
import java.time.Instant
import java.util.*
import kotlin.Exception

import kotlin.concurrent.thread
import kotlin.math.max
import kotlin.text.StringBuilder
import kotlin.time.TimeSource

const val MinFeeSatPerByte = 1.01
const val DesiredFeeSatPerByte = 1.1
const val MaxFeePerByte = 5.0
var MaxFee = 20000  //!< Sanity check by refusing to create a tx bigger than this fee

const val GEN_ADDRESS_CHUNK_SIZE = 100 //!< How many addresses to generate in a single "chunk"
const val PREHISTORY_SAFEFTY_FACTOR = 60 * 60 * 2 //!< 2 hours in seconds

// Approximate size of signature in a script -- used for guessing fees
const val APPROX_P2PKH_SIG_SCRIPT_LEN = 75 + 32; // sig + pubkey
const val TX_SCRIPTLESS_INPUT_SIZE = 32 + 4 + 4  // prevout hash + prevout index + sequence

private val LogIt = GetLog("BU.wallet")

open class WalletException(msg: String, shortMsg: String? = null, severity: ErrorSeverity = ErrorSeverity.Abnormal) :
  BUException(msg, shortMsg, severity)

open class WalletFeeException(msg: String, severity: ErrorSeverity = ErrorSeverity.Abnormal) :
  BUException(msg, appI18n(RexcessiveFee), severity)

open class WalletDustException(msg: String) : BUException(msg, appI18n(RsendDust))
open class WalletNotEnoughBalanceException(msg: String) : BUException(msg, appI18n(RinsufficentBalance), ErrorSeverity.Expected)
open class WalletImplementationException(msg: String) :
  BUException(msg, appI18n(RbadWalletImplementation), ErrorSeverity.Abnormal)

open class WalletIncompatibleAddress(msg: String) :
  BUException(msg, appI18n(RwalletAndAddressIncompatible), ErrorSeverity.Expected)

open class WalletNotSupportedException(msg: String) : WalletException(msg, appI18n(RnotSupported))
open class WalletKeyDataMissingException() : WalletException("", appI18n(RnotSupported))
open class WalletDisconnectedException() : WalletException("", appI18n(RwalletDisconnectedFromBlockchain), ErrorSeverity.Abnormal)

open class WalletNotEnoughTokenBalanceException(msg: String) :
  BUException(msg, appI18n(RinsufficentBalance), ErrorSeverity.Expected)

/** Provide random bytes to the C layer (needed on Android platforms where C does not have access to a cryptographically secure random number generator) */
fun SecRandom(data: ByteArray): ByteArray
{
    val random = SecureRandom()
    random.nextBytes(data)
    return data
}

class AddressDerivationKey
{
    companion object
    {
        val BIP43 = 0x2C.toLong()

        /** BIP-44 coin types are described (here)[https://github.com/satoshilabs/slips/blob/master/slip-0044.md] */
        val BTC = 0.toLong()
        val TESTNET = 1.toLong()
        val BCH = 0x91.toLong()
        val ION = 0x737978.toLong()

        /** This is used for identity and payments on any blockchain (there is value in having the same address across blockchains sometimes, but its also potentially
        dangerous if two blockchains use exactly the same tx format, sighash scheme, and a reproducible (non-UTXO) input form.  This seems unlikely.
         */
        val ANY = 0x1c3b1c3b.toLong()

        fun hardened(v: Long): Long
        {
            return 0x800000000 or v
        }

        fun hardened(v: Int): Long
        {
            return 0x800000000 or v.toLong()
        }

        /** This function calculates the BIP44 key from the specified path.  BIP44 requires that purpose, coinType and account are "hardened".
         * This function will automatically harden those parameters if you pass unhardened values.
         * Therefore this function cannot be used for generalized (non-BIP44) child key derivation.
         */
        @JvmStatic
        external fun Hd44DeriveChildKey(masterSecret: ByteArray, purpose: Long, coinType: Long, account: Long, change: Int, index: Int): ByteArray

    }
}

/** A destination for payments.  This includes all the information needed to send and spend from this destination.
It is often assumed that Bitcoin addresses are payment destinations, but this is not true.  You really need an output script, and also (if P2SH) a redeem script.
The reason why P2PKH addresses "work" as payment destinations is because they imply a specific script.  But "baking in" this assumption will make the wallet a
lot less flexible.
 */
@cli(Display.Simple, "A destination for payments.  This includes all the information needed to send and spend, although you may need to unlock the wallet when spending.")
abstract class PayDestination(val chainSelector: ChainSelector) : BCHserializable()
{
    companion object
    {
        @JvmStatic
        external fun GetPubKey(privateKey: ByteArray): ByteArray
    }

    /** Get the [P2SH](%budoc/glossary/P2SH) [redeem script](%budoc/glossary/redeem_script) needed to spend this destination.
     * @return an empty script if this destination is not P2SH */
    @cli(Display.Simple, "Get the [P2SH](%budoc/glossary/P2SH) [redeem script](%budoc/glossary/redeem_script) needed to spend this destination, or an empty script if not P2SH")
    open fun redeemScript(): BCHscript = BCHscript(chainSelector)

    /** Get the [constraint script](https://bitcoinunlimited.net/glossary/output_script) needed to send coins to this destination.  This script will contain spend constraints in the non-P2SH case, or in the
     * [P2SH](https://bitcoinunlimited.net/glossary/P2SH) case this script only constrains spending to execution of the redeemScript and any additional constraints are located in the redeemScript
     * @return the outputscript needed to constrain coins to this destination */
    open fun outputScript(): BCHscript = BCHscript(chainSelector)

    /** Get the [constraint script](https://bitcoinunlimited.net/glossary/output_script) needed to send coins to this destination.  This script will contain spend constraints in the non-P2SH case, or in the
     * [P2SH](https://bitcoinunlimited.net/glossary/P2SH) case this script only constrains spending to execution of the redeemScript and any additional constraints are located in the redeemScript
     * @return the outputscript needed to constrain coins to this destination */
    @cli(Display.Simple, """"Get the [constraint script](https://bitcoinunlimited.net/glossary/output_script) needed to send coins to this destination.  This is also called the "output script" or the "scriptPubKey".""")
    fun constraintScript(): BCHscript = outputScript()

    /** Get the [output script](https://bitcoinunlimited.net/glossary/output_script) needed to send tokens to this group destination.  This script will contain spend constraints in the non-P2SH case, or in the
     * [P2SH](https://bitcoinunlimited.net/glossary/P2SH) case this script only constrains spending to execution of the redeemScript and any additional constraints are located in the redeemScript
     * @return the outputscript needed to constrain coins to this destination */
    open fun groupedOutputScript(groupId: GroupId, groupAmount: Long): BCHscript = BCHscript(chainSelector)

    @cli(Display.Simple, """"Get the [constraint script](https://bitcoinunlimited.net/glossary/output_script) needed to send tokens and coins to this destination.  This is also called the "output script" or the "scriptPubKey".""")
    fun groupedConstraintScript(groupId: GroupId, groupAmount: Long): BCHscript = groupedOutputScript(groupId, groupAmount)


    /** Create a spend (input) script that will satisfy the constraints specified by the [outputScript] and the [redeemScript] (if applicable).  This script will contain the redeemScript in the P2SH case.
     * @param [params] provide needed, script-specific args in the params field, like the signature
     */
    open fun spendScript(vararg params: ByteArray): BCHscript = BCHscript(chainSelector)

    /** Create a spend (input) script that will satisfy the constraints specified by the [outputScript] and the [redeemScript] (if applicable).  This script will contain the redeemScript in the P2SH case.
     * Many scripts require signatures.  This API provides the data needed for this class to craft signatures, and provides additional derived-class defined params.
     * @param [params] provide needed, script-specific args in the params field
     */
    open fun spendScript(flatTx: ByteArray, inputIdx: Long, sigHashType: Int, inputAmount: Long, vararg params: ByteArray): BCHscript = BCHscript(chainSelector)


    @cli(Display.Simple, """"Get the script that can spend this destination (AKA input script, sigScript, spend script).  Many constraints require the transaction info (for signatures) so this data is supplied explicitly.  Additional script-specific data can be passed in the params field.""")
    fun satisfierScript(flatTx: ByteArray, inputIdx: Long, sigHashType: Int, inputAmount: Long, vararg params: ByteArray): BCHscript = spendScript(flatTx, inputIdx, sigHashType, inputAmount, *params)

    /**
     * Calculate the public key hash (PKH)
     */
    @cli(Display.Simple, """Return the public key hash (if this destination has a public key) otherwise return null""")
    fun pkh(): ByteArray?
    {
        pubkey?.let {
            return Hash.hash160(it)
        }
        return null
    }

    /** Get the [P2SH](%budoc/glossary/P2SH) or [P2PKH](%budoc/glossary/P2PKH) address associated with this destination
     *  Note that only a subset of PayDestinations have addresses.  A PayAddress will only exist if this destination constrains spending to require a signature (in the P2PKH case) or a script (P2SH case)
     *  @return Payment address associated with this destination or null if no address exists
     */
    @cli(Display.Simple, """Return the address (if this destination has an address) otherwise return null.""")
    open val address: PayAddress?
        get() = PayAddress(chainSelector, PayAddressType.P2PKH, ByteArray(0))

    /** Get an output (constrained to this destination) suitable for placing in a transaction  */
    @cli(Display.Simple, """Get an output (constrained to this destination) suitable for placing in a transaction""")
    open fun output(satoshis: Long): BCHoutput
    {
        return BCHoutput(chainSelector, satoshis, outputScript())
    }

    /** Get the public key if a single signature is needed to spend this destination
     *   @return If no pubkey exists, returns null
     */
    @cli(Display.Simple, """Get the public key if a single signature is needed to spend this destination, otherwise null""")
    abstract val pubkey: ByteArray?

    /** Get the secret key if a single signature is needed to spend this destination
     *   @return If no secret is needed to spend, return null
     */
    @cli(Display.Simple, """Get the secret if a single signature is needed to spend this destination, otherwise null""")
    abstract val secret: Secret?
}

/** Represents a "standard" P2PKH payment destination
 *
 */
class Pay2PubKeyHashDestination(chainSelector: ChainSelector, override var secret: Secret) : PayDestination(chainSelector)
{
    override var pubkey = GetPubKey(secret.getSecret())
    var pubkeyHash = Hash.hash160(pubkey)

    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this(chainSelector, UnsecuredSecret(stream.deByteArray()))
    {
    }

    fun updateSecret(s: Secret)
    {
        secret = s
        pubkey = GetPubKey(secret.getSecret())
        pubkeyHash = Hash.hash160(pubkey)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        // check format so the secret isn't accidentally sent over the network
        if (format == SerializationType.DISK)
        {
            return BCHserialized(format) + variableSized(secret.getSecret())  // TODO encrypt secret
        }
        throw NotImplementedError()
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        val s = BCHserialized(SerializationType.UNKNOWN).deByteArray()
        updateSecret(UnsecuredSecret(s))
        return stream
    }

    override fun redeemScript(): BCHscript
    {
        return BCHscript(chainSelector)
    }

    override fun outputScript(): BCHscript
    {
        return BCHscript.p2pkh(pubkeyHash, chainSelector)
    }

    override fun groupedOutputScript(groupId: GroupId, groupAmount: Long): BCHscript
    {
        return BCHscript.gp2pkh(groupId, groupAmount, pubkeyHash, chainSelector)
    }


    /**
     *  @param [params] Provide a single parameter which is the signature derived from [secret] of the transaction that is including this spend script
     */
    /*
    override fun spendScript(vararg params: ByteArray): BCHscript
    {
        return BCHscript(chainSelector, pubkey, params[0])
    }
     */
    fun spendScriptECDSA(flatTx: ByteArray, inputIdx: Long, sigHashType: Int, inputAmount: Long, vararg params: ByteArray): BCHscript
    {
        assert(params.size == 0)  // params is unused in this contract
        val sig = Wallet.signOneInputUsingECDSA(flatTx, sigHashType, inputIdx, inputAmount, outputScript().flatten(), secret.getSecret())
        val pubkey = pubkey // PayDestination.GetPubKey(secret.getSecret())
        return BCHscript(chainSelector, OP.push(sig), OP.push(pubkey))
    }

    override fun spendScript(flatTx: ByteArray, inputIdx: Long, sigHashType: Int, inputAmount: Long, vararg params: ByteArray): BCHscript
    {
        assert(params.size == 0)  // params is unused in this contract
        val sig = Wallet.signOneInputUsingSchnorr(flatTx, sigHashType, inputIdx, inputAmount, outputScript().flatten(), secret.getSecret())
        val pubkey = pubkey // PayDestination.GetPubKey(secret.getSecret())
        return BCHscript(chainSelector, OP.push(sig), OP.push(pubkey))
    }

    override val address: PayAddress
        get() = PayAddress(chainSelector, PayAddressType.P2PKH, pubkeyHash)
}

/** Represents a P2PK (pay to public key) destination wrapped in a P2SH script
 */
class Pay2ScriptPubKeyDestination(chainSelector: ChainSelector, override val secret: Secret) : PayDestination(chainSelector)
{
    override val pubkey = GetPubKey(secret.getSecret())
    val pubkeyHash = Hash.hash160(pubkey)

    val redeemScriptHash: ByteArray
        get() = Hash.hash160(redeemScript().flatten())

    override fun redeemScript(): BCHscript
    {
        var ret = BCHscript(chainSelector, OP.DUP, OP.HASH160, OP.push(pubkeyHash), OP.EQUALVERIFY, OP.CHECKSIG)
        return ret
    }

    override fun outputScript(): BCHscript
    {
        return BCHscript.p2sh(redeemScriptHash, chainSelector)
    }

    override fun groupedOutputScript(groupId: GroupId, groupAmount: Long): BCHscript
    {
        // If this group holds native tokens, then it must either be an authority or have 0 tokens
        assert(!groupId.isFenced() || (groupAmount <= 0))
        return BCHscript.gp2sh(groupId, groupAmount, redeemScriptHash, chainSelector)
    }

    /**
     *  @param [params] Provide a single parameter which is the signature derived from [secret] of the transaction that is including this spend script
     */
    override fun spendScript(vararg params: ByteArray): BCHscript
    {
        var redeem = redeemScript().flatten()
        return BCHscript(chainSelector, OP.push(redeem), pubkey, params[0])
    }

    override val address: PayAddress
        get() = PayAddress(chainSelector, PayAddressType.P2PKH, pubkeyHash)
}

/** Represents an arbitrary P2SH destination
 */
open class Pay2ScriptHashDestination(chainSelector: ChainSelector) : PayDestination(chainSelector)
{
    override var secret: Secret? = null
    override var pubkey: ByteArray? = null

    var redeemScript = BCHscript(chainSelector)

    constructor(chainSelector: ChainSelector, redeem: BCHscript) : this(chainSelector)
    {
        redeemScript = redeem
    }

    override fun redeemScript(): BCHscript
    {
        return redeemScript
    }

    override fun outputScript(): BCHscript
    {
        //return BCHscript(chainSelector, OP.HASH160, OP.push(address.data), OP.EQUAL)
        return BCHscript.p2sh(address.data, chainSelector)
    }

    override fun groupedOutputScript(groupId: GroupId, groupAmount: Long): BCHscript
    {
        // If this group holds native tokens, then it must either be an authority or have 0 tokens
        assert(!groupId.isFenced() || (groupAmount <= 0))
        return BCHscript.gp2sh(groupId, groupAmount, address.data, chainSelector)
    }

    /**
     *  @param [params] Provide all the data needed to spend the redeem script.  Each parameter will be pushed to the stack in the order passed (so the last parameter ends up on the top of the stack).
     */
    override fun spendScript(vararg params: ByteArray): BCHscript
    {
        var redeem = redeemScript().flatten()
        val ret = BCHscript(chainSelector)
        for (p in params)
        {
            ret.add(OP.push(p))
        }
        ret.add(OP.push(redeem))
        return ret
    }

    override val address: PayAddress
        get() = PayAddress(chainSelector, PayAddressType.P2SH, redeemScript().scriptHash160())
}

class MultisigDestination(
  chain: ChainSelector,
  val minSigs: Int,  // mininum number of signatures (n)
  val pubkeys: Array<ByteArray>,    // Order defines order in script, count defines total number of multisig signers (m)
  val privkeys: Array<Secret?>,  // Pass null for privkeys you don't know
) : Pay2ScriptHashDestination(chain)
{

    init
    {
        redeemScript = redeemScript()
    }

    companion object
    {
        @cli(Display.Dev, "Construct a Multisig payment destination from its redeem script and some wallets.  If you do not have enough wallets to fully sign, then the resulting satisfier script will not be fully signed (but could be given to another wallet to finish (TBD)).")
        fun fromRedeem(redeemScript: BCHscript, vararg wallets: Wallet): PayDestination
        {
            val redeem = redeemScript.parsed()
            val pubkeys = mutableListOf<ByteArray>()
            val privkeys = mutableListOf<Secret?>()
            val minSigs = scriptNumFrom(redeem[0])
            for (item in redeem)  // all data items in the redeem scripts are the wallet pubkeys
            {
                val pubkey = scriptDataFrom(item)
                if (pubkey != null)  // If its null, its not data (not a pubkey)
                {
                    pubkeys.add(pubkey)
                    var found = false
                    for (w in wallets)
                    {
                        val privkey = w.pubkeyToSecret(pubkey)
                        if (privkey != null)
                        {
                            privkeys.add(privkey)
                            found = true
                            break
                        }
                    }
                    if (!found)
                    {
                        privkeys.add(null)
                    }
                }
            }
            return MultisigDestination(redeemScript.chainSelector, minSigs.toInt(), pubkeys.toTypedArray(), privkeys.toTypedArray())
        }
    }

    val redeemScriptHash: ByteArray
        get() = Hash.hash160(redeemScript().flatten())

    override fun redeemScript(): BCHscript
    {
        val ret = BCHscript(chainSelector) + OP.push(minSigs)
        for (pk in pubkeys)
            ret.add(OP.push(pk))
        ret.add(OP.push(pubkeys.size))
        ret.add(OP.CHECKMULTISIG)
        return ret
    }

    fun spendScriptECDSA(flatTx: ByteArray, inputIdx: Long, sigHashType: Int, inputAmount: Long, vararg params: ByteArray): BCHscript
    {
        assert(params.size == 0)  // No params needed for multisig
        val ret = BCHscript(chainSelector)

        ret.add(OP.push(0))
        for (priv in privkeys)
        {
            if (priv != null)
            {
                val sc = redeemScript().flatten()
                val secret = priv.getSecret()
                val sig: ByteArray = Wallet.signOneInputUsingECDSA(flatTx, sigHashType, inputIdx, inputAmount, sc, secret)
                //val sig: ByteArray = Wallet.signOneInputUsingSchnorr(flatTx, sigHashType, inputIdx, inputAmount, sc, secret)
                ret.add(OP.push(sig))
                // println("sighashtype: 0x${sigHashType.toString(16)}, input: ${inputIdx} amount: ${inputAmount} constraintScript: ${sc.toHex()} tx: ${flatTx.toHex()}\npriv: ${secret.toHex()} -> sig: ${sig.toHex()}")
            }
        }

        var redeem = redeemScript().flatten()
        ret.add(OP.push(redeem))
        return ret
    }


    override fun spendScript(flatTx: ByteArray, inputIdx: Long, sigHashType: Int, inputAmount: Long, vararg params: ByteArray): BCHscript
    {
        assert(params.size == 0)  // No params needed for multisig
        val ret = BCHscript(chainSelector)

        var spendMap = 0
        var privCount = 0
        for (priv in privkeys.reversed())
        {
            spendMap = spendMap shl 1
            if (priv != null)
            {
                spendMap = spendMap or 1
                privCount++
            }
        }

        ret.add(OP.push(spendMap))
        for (priv in privkeys)
        {
            if (priv != null)
            {
                val sc = redeemScript().flatten()
                val secret = priv.getSecret()
                val sig: ByteArray = Wallet.signOneInputUsingSchnorr(flatTx, sigHashType, inputIdx, inputAmount, sc, secret)
                ret.add(OP.push(sig))
                // println("sighashtype: 0x${sigHashType.toString(16)}, input: ${inputIdx} amount: ${inputAmount} constraintScript: ${sc.toHex()} tx: ${flatTx.toHex()}\npriv: ${secret.toHex()} -> sig: ${sig.toHex()}")
            }
        }

        var redeem = redeemScript().flatten()
        ret.add(OP.push(redeem))
        return ret
    }
}

val BCHidentityParams = arrayOf("attest", "ava", "billing", "dob", "email", "hdl", "realname", "ph", "postal", "sm")

@cli(Display.Simple, "An entity that requires identity information")
class IdentityDomain(@cli(Display.Simple, "Address of entity") var domain: String,
  @cli(Display.Simple, "Which crypto public key is used") var useIdentity: Long) : BCHserializable()
{
    companion object
    {
        const val IDENTITY_BY_HASH: Long = 0
        const val COMMON_IDENTITY: Long = 1
    }

    /* these variables indicate permission to provide certain data to this domain */
    @cli(Display.Simple, "provide attestations, if requested")
    var attestP: Boolean = false

    @cli(Display.Simple, "provide avatar image, if requested")
    var avaP: Boolean = false

    @cli(Display.Simple, "provide billing address, if requested")
    var billingP: Boolean = false

    @cli(Display.Simple, "provide birthday, if requested")
    var dobP: Boolean = false

    @cli(Display.Simple, "provide email, if requested")
    var emailP: Boolean = false

    @cli(Display.Simple, "provide handle/username, if requested")
    var hdlP: Boolean = false

    @cli(Display.Simple, "provide phone number, if requested")
    var phoneP: Boolean = false

    @cli(Display.Simple, "provide postal address, if requested")
    var postalP: Boolean = false

    @cli(Display.Simple, "provide real name, if requested")
    var realnameP: Boolean = false

    /* these variables indicate the requirement from the domain (mandatory, recommended, optional, x not used) */
    @cli(Display.Simple, "attestation requirement")
    var attestR: Char = 'x'

    @cli(Display.Simple, "avatar image requirement")
    var avaR: Char = 'x'

    @cli(Display.Simple, "billing address requirement")
    var billingR: Char = 'x'

    @cli(Display.Simple, "birthday requirement")
    var dobR: Char = 'x'

    @cli(Display.Simple, "email requirement")
    var emailR: Char = 'x'

    @cli(Display.Simple, "username/handle requirement")
    var hdlR: Char = 'x'

    @cli(Display.Simple, "phone number requirement")
    var phoneR: Char = 'x'

    @cli(Display.Simple, "postal address requirement")
    var postalR: Char = 'x'

    @cli(Display.Simple, "real name requirement")
    var realnameR: Char = 'x'

    /** Deserializataion constructor */
    constructor(stream: BCHserialized) : this("", 0)
    {
        BCHdeserialize(stream)
        LogIt.info("identityDomain: " + domain + " " + useIdentity)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        return BCHserialized(format).add(domain).addUint64(useIdentity)
          .add(attestP).add(avaP).add(billingP).add(dobP).add(emailP).add(hdlP).add(phoneP).add(postalP).add(realnameP)
          .add(attestR).add(avaR).add(billingR).add(dobR).add(emailR).add(hdlR).add(phoneR).add(postalR).add(realnameR)
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        domain = stream.deString()
        useIdentity = stream.deuint64()
        attestP = stream.deboolean()
        avaP = stream.deboolean()
        billingP = stream.deboolean()
        dobP = stream.deboolean()
        emailP = stream.deboolean()
        hdlP = stream.deboolean()
        phoneP = stream.deboolean()
        postalP = stream.deboolean()
        realnameP = stream.deboolean()
        attestR = stream.dechar()
        avaR = stream.dechar()
        billingR = stream.dechar()
        dobR = stream.dechar()
        emailR = stream.dechar()
        hdlR = stream.dechar()
        phoneR = stream.dechar()
        postalR = stream.dechar()
        realnameR = stream.dechar()
        return stream
    }

    fun getPerms(perms: MutableMap<String, Boolean>)
    {
        perms["attest"] = attestP
        perms["ava"] = avaP
        perms["billing"] = billingP
        perms["dob"] = dobP
        perms["email"] = emailP
        perms["hdl"] = hdlP
        perms["phone"] = phoneP
        perms["postal"] = postalP
        perms["realname"] = realnameP
    }

    fun setPerms(perms: MutableMap<String, Boolean>): Boolean
    {
        var changed = false;

        var tmp = perms["attest"]
        if ((tmp != null) && (tmp != attestP))
        {
            changed = true; attestP = tmp
        }

        tmp = perms["ava"]
        if ((tmp != null) && (tmp != avaP))
        {
            changed = true; avaP = tmp
        }

        tmp = perms["billing"]
        if ((tmp != null) && (tmp != billingP))
        {
            changed = true; billingP = tmp
        }

        tmp = perms["dob"]
        if ((tmp != null) && (tmp != dobP))
        {
            changed = true; dobP = tmp
        }

        tmp = perms["email"]
        if ((tmp != null) && (tmp != emailP))
        {
            changed = true; emailP = tmp
        }

        tmp = perms["hdl"]
        if ((tmp != null) && (tmp != hdlP))
        {
            changed = true; hdlP = tmp
        }

        tmp = perms["phone"]
        if ((tmp != null) && (tmp != phoneP))
        {
            changed = true; phoneP = tmp
        }

        tmp = perms["postal"]
        if ((tmp != null) && (tmp != postalP))
        {
            changed = true; postalP = tmp
        }

        tmp = perms["realname"]
        if ((tmp != null) && (tmp != realnameP))
        {
            changed = true; realnameP = tmp
        }

        return changed
    }

    fun getReqs(perms: MutableMap<String, String>)
    {
        perms["attest"] = attestR.toString()
        perms["ava"] = avaR.toString()
        perms["billing"] = billingR.toString()
        perms["dob"] = dobR.toString()
        perms["email"] = emailR.toString()
        perms["hdl"] = hdlR.toString()
        perms["phone"] = phoneR.toString()
        perms["postal"] = postalR.toString()
        perms["realname"] = realnameR.toString()
    }


    fun setReqs(reqs: MutableMap<String, String>): Boolean
    {
        var changed = false;

        var tmp = reqs["attest"]
        if ((tmp != null) && (tmp[0] != attestR))
        {
            changed = true; attestR = tmp[0]
        }

        tmp = reqs["ava"]
        if ((tmp != null) && (tmp[0] != avaR))
        {
            changed = true; avaR = tmp[0]
        }

        tmp = reqs["billing"]
        if ((tmp != null) && (tmp[0] != billingR))
        {
            changed = true; billingR = tmp[0]
        }

        tmp = reqs["dob"]
        if ((tmp != null) && (tmp[0] != dobR))
        {
            changed = true; dobR = tmp[0]
        }

        tmp = reqs["email"]
        if ((tmp != null) && (tmp[0] != emailR))
        {
            changed = true; emailR = tmp[0]
        }

        tmp = reqs["hdl"]
        if ((tmp != null) && (tmp[0] != hdlR))
        {
            changed = true; hdlR = tmp[0]
        }

        tmp = reqs["phone"]
        if ((tmp != null) && (tmp[0] != phoneR))
        {
            changed = true; phoneR = tmp[0]
        }

        tmp = reqs["postal"]
        if ((tmp != null) && (tmp[0] != postalR))
        {
            changed = true; postalR = tmp[0]
        }

        tmp = reqs["realname"]
        if ((tmp != null) && (tmp[0] != realnameR))
        {
            changed = true; realnameR = tmp[0]
        }

        return changed
    }
}


/** Let's be blunt: Kotlin enums are unusable */
class TxCompletionFlags
{
    companion object
    {
        const val USE_GROUP_AUTHORITIES = 1
        const val FUND_GROUPS = 2
        const val FUND_NATIVE = 4
        const val SIGN = 8
        const val BIND_OUTPUT_PARAMETERS = 0x10
        const val PARTIAL = 0x20 // use sighash single or 0thru to create a partial transaction
    }
}


@cli(Display.Simple, "A wrapper to a secret so the secret may remain encrypted for as long as possible (or not)")
abstract class Secret
{
    abstract fun getSecret(): ByteArray
}

class UnsecuredSecret(private val secretBytes: ByteArray) : Secret()
{
    override fun getSecret(): ByteArray
    {
        return secretBytes
    }
}

/*
// TODO: make this class's accessor function request the secret from the encrypted wallet
class SecuredSecret(private val secretBytes:ByteArray):Secret()
{
    override fun getSecret():ByteArray
    {
        return secretBytes
    }
}
*/

/** This class defines a wallet interface.  A wallet is a set of payment destinations that are being tracked on a particular blockchain.
 */
@cli(Display.Simple, "Wallet interface")
abstract class Wallet
{
    /** Install a callback handler for whenever this wallet changes */
    abstract fun setOnWalletChange(callback: ((wallet: Wallet) -> Unit)?)

    /** Return identity domain data if this domain has previously been used */
    @cli(Display.Simple, "Get information about an entity that has requested identity information")
    abstract fun lookupIdentityDomain(name: String): IdentityDomain?

    /** Add or update identity domain data */
    @cli(Display.Simple, "Insert or replace identity information relevant to a particular entity")
    abstract suspend fun upsertIdentityDomain(id: IdentityDomain)

    /** Return all configured identity domains */
    @cli(Display.Simple, "Get all entities that we have provided identity to")
    abstract fun allIdentityDomains(): Collection<IdentityDomain>

    /** Get a new address to receive funds.  Different wallets may support different payment destination types.  This API returns whatever is the "default" type for this wallet.  This allows generic algorithms to be created that
     * can be applied to many different wallet types.  When this API returns, the destination is ready for use (monitoring is installed in remote nodes, IF any remote nodes exist).  This API may pre-generate destinations.
     * @return A payment destination
     */
    @cli(Display.Dev, "Get a new address where money can be received (suspends)")
    abstract suspend fun newDestination(): PayDestination

    /** Tell the wallet that now is a good time to top-up a cache of unused destinations */
    abstract fun prepareDestinations(minAmt: Int = GEN_ADDRESS_CHUNK_SIZE, chunk: Int = GEN_ADDRESS_CHUNK_SIZE)

    /** Gets a new address: convenience function that is functionally similar to @newDestination but mirrors the bitcoin-cli command
     */
    fun getnewaddress(): PayAddress = getNewAddress()  // for the classic bitcoin RPC capitalization compatibility

    @cli(Display.Simple, "Get a new address where money can be received")
    fun getNewAddress(): PayAddress
    {
        val dest = runBlocking { newDestination() }
        return dest.address!! // !! Technically a destination may not have an address, but not ones that we create
    }

    /** Get a new address to receive funds.  Different wallets may support different payment destination types.  This API returns whatever is the "default" type for this wallet.  This allows generic algorithms to be created that
     * can be applied to many different wallet types.  When this API returns, the destination is ready for use (monitoring is installed in remote nodes, IF any remote nodes exist).  This API may pre-generate destinations.
     * @return A payment destination
     */
    @cli(Display.User, "Get a new address where money can be received (blocks)")
    fun getNewDestination(): PayDestination
    {
        val dest = runBlocking { newDestination() }
        return dest
    }

    /** Low level API to generate a new address to receive funds, called by newDestination.  Use "newDestination()" in almost all cases.  This API does not pre-generate (so may be slow), and
     * does not install the destination into bloom filters, etc, before returning.  This means that there may be a race condition between the use of the destination
     * returned here and the wallet's monitoring of that destination which could cause funds to be received but not noticed by this wallet.
     * @return A payment destination
     */
    @cli(Display.Dev, "Create a new payment destination, rather then using a pre-allocated one")
    abstract fun generateDestination(): PayDestination

    /** Get a repeatable destination, generally used for identity purposes.
     *  Given 2 different seeds, this API should at a minimum be statistically unlikely to produce the same address
     *  The seed may be public data (a domain name, for example) so wallet security must not depend on it being a secret.
     *  @param seed Unique data per destination.  Use the empty string "" to get the "common" destination
     * @return A payment destination
     */
    @cli(Display.Dev, "Create a new payment destination, based on the given seed")
    abstract fun destinationFor(seed: String): PayDestination

    /** Wallet implementations may allow access to addresses generated from specific private keys or nonstandard HD derivation paths.
     *  The wallet will never offer these destinations as current payment targets.  It will only spend them (and show their balance).

     *  To inject "retrieve only" type of destination into the core wallet operation, override and implement this function.
     *  This function is only called during wallet initialization and rediscovery.  Dynamic addition of destinations is not supported.
     *
     *  (note to future dev: dynamic addition is not hard in this architecture.  Use electrum to find the UTXOs (and the TXOs if you want
     *  to import history) and add them and the destination to the appropriate CommonWallet data structures.  Save and regenerate bloom.
     */
    open fun getRetrieveOnlyDestinations(): MutableList<PayDestination>
    {
        return mutableListOf()
    }

    /** Send funds to this destination.  This function will select input coins from the wallet to fill the passed quantity
     * @param amountSatoshis Provide how many coins to send, denominated in the fundamental (smallest possible) unit of this currency
     * @param destScript The output (constraint) script
     * @param deductFeeFromAmount Set to true to reduce the amount sent by the fee needed
     * @return a signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Simple, "Send money")
    abstract fun send(amountSatoshis: Long, destScript: BCHscript, deductFeeFromAmount: Boolean = false, sync: Boolean = false): BCHtransaction

    /** Send funds to this destination.  This function will select input coins from the wallet to fill the passed quantity
     * @param amountSatoshis Provide how many coins to send, denominated in the fundamental (smallest possible) unit of this currency
     * @param destAddress The destination address
     * @param deductFeeFromAmount Set to true to reduce the amount sent by the fee needed
     * @return a signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Simple, "Send money")
    abstract fun send(amountSatoshis: Long, destAddress: PayAddress, deductFeeFromAmount: Boolean = false, sync: Boolean = false): BCHtransaction

    /** Send funds to this destination.  This function will select input coins from the wallet to fill the passed quantity
     * @param amountSatoshis Provide how many coins to send, denominated in the fundamental (smallest possible) unit of this currency
     * @param destAddress The destination address as a string
     * @param deductFeeFromAmount Set to true to reduce the amount sent by the fee needed
     * @return a signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Simple, "Send money")
    abstract fun send(amountSatoshis: Long, destAddress: String, deductFeeFromAmount: Boolean = false, sync: Boolean = false): BCHtransaction


    /** Creates an unsigned transaction that sends to a list of outputs.  This function will select input coins from the wallet to fill the passed quantity
     * and sign the transaction, but will not relay the transaction to the network.
     * @param outputs A list of amounts and output (constraint) scripts
     * @param minConfirms: (Int = 0) minimum depth in the blockchain inputs must have to be eligible for inclusion in this transaction
     * @param deductFeeFromAmount Set to true to reduce the amount sent by the fee needed, rather than add more for the fee
     * @return A signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Dev, "Create a transaction, but do not send it")
    abstract fun prepareSend(outputs: MutableList<BCHoutput>, minConfirms: Int = 1, deductFeeFromAmount: Boolean = false): BCHtransaction

    /** Modify the passed transaction to complete it to the extent possible by this wallet, including:
    Find inputs needed to supply satoshis and/or group tokens for this transaction.  If change outputs are required, add them.  If mint baton passing outputs are possible then add them if equalizeAuthorities=true
    @param tx: The transaction to complete
    @param inputAmount: if inputAmount is non-null, assume existing inputs supply this number of satoshis (do not look up these inputs)
    @param flags: bit map of TxCompletionFlags:
    @param useAuthorities: If useAuthorities = true, pull in authorities if needed (and available) to handle (mint/melt) operations
    @param fund: If fund = true, add native crypto inputs to pay for the transaction
    @param signSingle: If signSingle = true, the signature just covers the corresponding (by idx) output
     */
    @cli(Display.Dev, "Fund the passed partial transaction.  Do not send it")
    abstract fun txCompleter(tx: BCHtransaction, minConfirms: Int, flags: Int, inputAmount: Long? = null) //, signSingle:Boolean = false)

    /** Post this transaction and update the wallet based on any inputs spent.  Typically the provided tx came from calling [prepareSend]
     * @param tx The transaction to be sent
     * @return A signed transaction
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Simple, "Send money")
    abstract fun send(tx: BCHtransaction, sync: Boolean = false)

    /** Abort this transaction, whose inputs were reserved by "prepareSend".  Update the wallet to release all inputs reserved by this transaction
     * @param tx The transaction to be aborted
     * @return Nothing
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.Dev, "Create a transaction, but do not send it")
    abstract fun abortTransaction(tx: BCHtransaction)

    /** Calculate a suggested fee for this transaction
     * @param tx The transaction that needs a fee
     * @param pad Presume that the transaction is actually this much bigger
     * @param priority Some blockchain defined measure of the importance of this tx.  0 is the highest priority.  Irrelevant in BCH
     * @return The fee in satoshis
     * @throws [WalletException] error specific subclasses of [WalletException] are thrown if the transaction cannot be constructed
     */
    @cli(Display.User, "Suggest a fee for the passed transaction")
    abstract fun suggestFee(tx: BCHtransaction, pad: Int = 0, priority: Int = 0): Long

    /** @return the current balance in the supplied address
     */
    @cli(Display.Simple, "Get the current balance in the supplied address")
    abstract fun getBalanceIn(dest: PayAddress): Long

    /** @return true if the passed address belongs to this wallet
     */
    @cli(Display.Simple, "true if the passed address belongs to this wallet")
    abstract fun isWalletAddress(dest: PayAddress): Boolean

    /** @return true if the passed address belongs to this wallet, and is currently active
     */
    @cli(Display.Simple, "true if the passed address belongs to this wallet and contains some balance")
    abstract fun isUnspentWalletAddress(dest: PayAddress): Boolean

    /** @return the sum of the amounts in all confirmed unspent outputs spendable by this wallet
     */
    @cli(Display.Simple, "Confirmed amount of satoshis in this wallet")
    abstract val balance: Long

    /** @return the sum of the amounts in all unconfirmed unspent outputs spendable by this wallet
     *  Unconfirmed spends are removed from this balance
     */
    @cli(Display.Simple, "Unconfirmed amount of satoshis in this wallet")
    abstract val balanceUnconfirmed: Long

    /** This wallet finds its coins on this blockchain
     */
    @cli(Display.Simple, "Access the wallet's blockchain")
    abstract val blockchain: Blockchain

    /** Any name you may want to call this wallet */
    @cli(Display.Simple, "Wallet name")
    abstract val name: String

    /** Returns the current position of this wallet in the blockchain.  Since a fork may have occurred, this information does not unambiguously locate the wallet, but it is valuable for UI.
     */
    @cli(Display.Simple,
      "Returns the current position of this wallet in the blockchain.  The wallet's state will not reflect any changes beyond this position")
    abstract val syncedHeight: Long

    /** Return whether this wallet is synced with its underlying blockchain.  If it is not synced, properties like balance and balanceUnconfirmed express some previous state.
     * In the unsynced case, this API will wait for it to do so, but no more than the provided time in milliseconds.
     * If a height is provided, this API returns true if the wallet is synced up to or beyond this height
     * If a height is not provided (or is -1), this API assumes you mean up to "now".  This is special cased with an extra check that the blockchain's tip timestamp is within an hour of
     * now.
     * Of course, since blocks can be discovered at any time, and connected nodes can be slow at processing blocks
     * one cannot ever absolutely know whether the wallet is really synced up to "now", so this function is more accurately described as "nearly synced" for any "now" call.
     */
    @cli(Display.Simple, "Return whether this wallet is synced with its underlying blockchain")
    abstract fun synced(epochTimeinMsOrBlockHeight: Long = -1L): Boolean

    /** Forget all transaction and blockchain state, and asynchronously redo the search for wallet transactions.
     * This is intended for testing and debug
     */
    @cli(Display.Dev, "Forget all transaction and blockchain state, and asynchronously redo the search for wallet transactions")
    abstract fun rediscover(forgetAddresses: Boolean = false): Unit

    /** Pause blockchain processing if true */
    @cli(Display.Dev, "Pause blockchain processing")
    abstract var pause: Boolean

    /** wallet history by transaction */
    @cli(Display.User, "wallet history by transaction")
    abstract val txHistory: MutableMap<Hash256, TransactionHistory>

    /** All payments (inputs or outputs of a transaction) that interest this wallet -- either sent or received */
    // @cli(Display.User, "All payments (inputs or outputs of a transaction) that interest this wallet -- either sent or received")
    abstract val paymentHistory: MutableMap<BCHoutpoint, PaymentHistory>

    /** all UTXOs that this wallet is capable of spending (and how to spend them)
     * READ ONLY */
    @cli(Display.Dev, "all UTXOs that this wallet is capable of spending")
    abstract val unspent: Map<BCHoutpoint, BCHspendable>

    @cli(Display.Dev, "find and return the secret corresponding to the passed pubkey.")
    abstract fun pubkeyToSecret(pubkey: ByteArray): Secret?

    /** Control whether saving the wallet to underlying storage is automatic (true, default), or initiated by API calls
     * Wallet performance could improve if automaticFlush is false and the wallet is saved rarely.  However state may be lost! */
    @cli(Display.User, "Control whether saving the wallet to underlying storage is automatic or initiated by the save() API call")
    var automaticSave: Boolean = true

    /** Write this wallet's changes to underlying storage. Called internally if @automaticSave is true, and/or explicitly called.
     * Synchronous.  An inability to save the wallet is a severe problem so exceptions are thrown. */
    @cli(Display.User, "Save wallet state to underlying storage")
    abstract fun save(force: Boolean = false)

    /** Give this wallet access to the spot price of its token in the provided fiat currency code */
    var spotPrice: ((String) -> BigDecimal)? = null

    /** Give this wallet access to the historical price (epoch seconds) of its token in the provided fiat currency code */
    var historicalPrice: ((String, Long) -> BigDecimal)? = null

    companion object
    {
        /** Network-defined bloom filter maximum size
         * TODO: associate this value with the blockchain
         */
        val MAX_BLOOM_SIZE = 36000

        /**  Bloom filter update flags
         * Must be consistent with same named fields in C++ code in bloom.h:bloomflags
         * */
        enum class BloomFlags(val v: Int)
        {
            BLOOM_UPDATE_NONE(0),
            BLOOM_UPDATE_ALL(1),
            BLOOM_UPDATE_P2PUBKEY_ONLY(2)
        }

        //val BLOOM_UPDATE_MASK = 3

        /** Given an array of items, creates a bloom filter and returns it serialized.
         * Typical items are addresses (just the raw 20 bytes), transaction hashes, and outpoints
         * @param items Array<ByteArray> of bitstrings to place into the bloom filter
         * @param falsePosRate Desired Bloom false positive rate
         * @param capacity Number of elements that can be placed into this bloom while maintaining the falsePosRate.  If this is < items.count(), items.count() is used.  The reason to provide a larger capacity is to allow items to be added into the bloom filter later
         * @param maxSize Maximum size of the bloom filter -- if the capacity and falsePosRate result in a bloom that's larger than this, this size is used instead
         * @param flags  See @BloomFlags for possible fields
         * @param tweak Change tweak to create a different bloom filter.  Used to ensure that collision attacks only work against one filter or node
         * @return Bloom filter serialized as in the P2P network format
         */
        @JvmStatic
        external fun CreateBloomFilter(items: Array<Any>, falsePosRate: Double, capacity: Int, maxSize: Int, flags: Int = 0, tweak: Int = 1): ByteArray

        /** Create an ECDSA signature for the passed transaction
         * @param txData transaction serialized for signing
         * @param sigHashType signature hash algorithm selection
         * @param inputIdx what input to sign
         * @param inputAmount how many satoshis this input contains
         * @param prevoutScript the input's constraint script
         * @secret 32 byte private key
         * @return signature in binary format
         */
        @JvmStatic
        external fun signOneInputUsingECDSA(txData: ByteArray, sigHashType: Int, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray

        /** Create a Schnorr signature for the passed transaction
         * @param txData transaction serialized for signing
         * @param sigHashType signature hash algorithm selection
         * @param inputIdx what input to sign
         * @param inputAmount how many satoshis this input contains
         * @param prevoutScript the input's constraint script
         * @secret 32 byte private key
         * @return signature in binary format
         */
        @JvmStatic
        external fun signOneInputUsingSchnorr(txData: ByteArray, sigHashType: Int, inputIdx: Long, inputAmount: Long, prevoutScript: ByteArray, secret: ByteArray): ByteArray

        /** Sign a message using the same algorithm as the original bitcoin wallet's signmessage functionality
         * @message The raw bytes to be signed
         * @secret 32 byte private key
         * @return signature in binary format.  Call encode64 to convert to the exact signature format used by the bitcoin wallet's signmessage */
        @JvmStatic
        external fun signMessage(message: ByteArray, secret: ByteArray): ByteArray

        /** Verify a message using the same algorithm as the original bitcoin wallet's signmessage functionality
         * @message The raw bytes of the message to be verified
         * @secret The address raw bytes (without the type prefix)
         * @return The pubkey that was used to sign, if the signature is valid, otherwise a zero size array. */
        @JvmStatic
        external fun verifyMessage(message: ByteArray, address: ByteArray, signature: ByteArray): ByteArray?
    }
}

/** Helper class that glues a wallet to a blockchain
 * @property [chain] a reference to an active blockchain object.  The wallet will access this object whenever it needs blockchain data.
 */
@cli(Display.Simple, "connection between this wallet and its blockchain")
class GlueWalletBlockchain(val chain: Blockchain) : BCHserializable()
{
    /** This wallet has synchronized its balances up to this block height.  Note, use syncedHash to be certain of a sync because the chain could have forks.
     */
    @Volatile
    @cli(Display.Simple, "This wallet has synchronized its balances up to this block height")
    var syncedHeight: Long = chain.checkpointHeight

    /** This wallet has synchronized its balances up to this block hash
     */
    @Volatile
    @cli(Display.Simple, "This wallet has synchronized its balances up to this block hash")
    var syncedHash: Hash256 = chain.checkpointId

    /** Information about what happened at a particular block, allowing us to unwind that block in case of a reorg.
     * If there is no entry, nothing happened.  This data also allows the wallet to provide transaction history
     */
    @cli(Display.Dev,
      "Information about what happened at a particular block, allowing us to unwind that block in case of a reorg")
    var blockRewind: MutableMap<Hash256, RewindData> = mutableMapOf()

    /** Any block before this one cannot have any transactions relevant to this wallet (the wallet was created after this block).
     *  The wallet uses this information to rapidly sync.
     */
    @cli(Display.Simple, "Any block before this date cannot have any transactions relevant to this wallet")
    var prehistoryDate: Long = 0

    /** Any block before this one cannot have any transactions relevant to this wallet (the wallet was created after this block).
     *  The wallet uses this information to rapidly sync.
     */
    @cli(Display.Simple,
      "Any block before this one cannot have any transactions relevant to this wallet.  If zero we don't know the height")
    var prehistoryHeight: Long = 0

    /** a handle to this wallet's bloom filter data installed in the Blockchain */
    var filterHandle: Int? = null

    /** Get or rewindData, create empty if nonexistent */
    @Synchronized
    fun getRewindData(blockHash: Hash256): RewindData
    {
        return blockRewind.getOrPut(blockHash, { RewindData(chain.chainSelector) })
    }

    init
    {
        chain.attachWallet()
    }

    @Synchronized
    fun delete()
    {
        chain.detachWallet(filterHandle)
    }

    /** Reset this wallet's blockchain state to the earliest point available in the blockchain */
    @Synchronized
    fun resetToCheckpoint()
    {
        blockRewind.clear()
        syncedHeight = chain.checkpointHeight
        syncedHash = chain.checkpointId
    }

    /** Reset this wallet's blockchain state to the wallet prehistory */
    @Synchronized
    fun resetToPrehistory()
    {
        if (prehistoryHeight != 0L)  // I know the prehistory block
        {
            blockRewind.clear()
            syncedHeight = prehistoryHeight
            syncedHash = chain.getBlockHeader(syncedHeight).hash
        }
        else resetToCheckpoint()  // I don't know it so reset do checkpoint to discover it by iteration
    }

    @Synchronized
    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        LogIt.info(sourceLoc() + " " + chain.name + " prehistory written synced is $syncedHeight time is $prehistoryDate height is $prehistoryHeight hash is $syncedHash")
        val tmp = BCHserialized(SerializationType.DISK).addUint64(syncedHeight).addUint64(prehistoryDate).addUint64(prehistoryHeight) + syncedHash + BCHserialized.mutableMap(blockRewind, SerializationType.DISK)
        return tmp
    }

    @Synchronized
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        syncedHeight = stream.deuint64()
        prehistoryDate = stream.deuint64()
        prehistoryHeight = stream.deuint64()
        syncedHash = Hash256(stream)
        blockRewind = stream.demap({ var v = Hash256(); v.BCHdeserialize(it); v },
          { var v = RewindData(chain.chainSelector); v.BCHdeserialize(it); v })
        LogIt.info(sourceLoc() + " " + chain.name + " prehistory read synced is $syncedHeight time is $prehistoryDate height is $prehistoryHeight hash is $syncedHash")
        // LogIt.info(sourceLoc() + " " + chain.name + " Loaded wallet blockchain state")
        return stream
    }
}

/** If a block is rewound, this is how to undo it.  Also constitutes all the data needed to show transaction history.
 */
class RewindData(val chainSelector: ChainSelector) : BCHserializable()
{
    /** If this block is rewound, these outputs become spendable again */
    var spent: MutableList<BCHspendable> = mutableListOf()

    /** If this block is rewound, these outputs disappear */
    var added: MutableList<BCHspendable> = mutableListOf()

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        return BCHserialized.list(spent, SerializationType.DISK) + BCHserialized.list(added, SerializationType.DISK)
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        spent = stream.delist({ BCHspendable(chainSelector, it) })
        added = stream.delist({ BCHspendable(chainSelector, it) })
        return stream
    }
}

/** Track the history of individual payments (inputs or outputs) */
class PaymentHistory() : BCHserializable()
{
    /** True if this payment is a tx input (and adding coins to this wallet), false if this is a tx output (and spending coins from this wallet) */
    var isInflow: Boolean = false

    /** True if this is a self payment (change) */
    var isChange: Boolean = false

    /** How much is received/spent in Satoshi */
    var amount: Long = 0

    /** When this payment occurred in epoch milliseconds */
    var date: Long = Instant.now().toEpochMilli()

    /** Index of the input/output in the transaction */
    var txIdx: Long = -1

    /** Transaction hash */
    var txHash: Hash256? = null

    var basisOverride: BigDecimal? = null
    var saleOverride: BigDecimal? = null

    /** What was the price (in wallet units) when this payment was issued */
    var priceWhenIssued: BigDecimal = BigDecimal.ZERO

    /** What currency is the price denoted in (currency code) */
    var priceWhatFiat: String = ""

    /** The address this wallet sent the payment to or the address we received this payment into, based on whether this payment is outgoing or incoming */
    var address: PayAddress? = null

    /** Some addtl information about this tx */
    var note: String = ""

    @Suppress("UNUSED_PARAMETER")
    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this()
    {
        BCHdeserialize(stream)
    }


    fun getOutpoint(): BCHoutpoint
    {
        return BCHoutpoint(txHash!!, txIdx)
    }

    /** Calculate the capital gain or loss of this transaction based on info from the provided transaction history */
    fun capGains(): BigDecimal
    {
        assert(!isInflow)  // No cap gains on the buy side
        assert(basisOverride != null)  // any sale must have a basis carried forward
        val bo = basisOverride!!

        if (saleOverride != null)
        {
            return saleOverride!! - bo
        }

        return (priceWhenIssued * BigDecimal(amount, currencyMath).setScale(currencyScale)) - bo
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        val bo = if (basisOverride == null) "" else serializeFormat.format(basisOverride)
        val po = if (saleOverride == null) "" else serializeFormat.format(saleOverride)
        val ret = BCHserialized(format).addUint64(date).add(isInflow).add(isChange).add(txHash!!).addInt32(txIdx).add(serializeFormat.format(priceWhenIssued))
          .add(priceWhatFiat).addUint64(amount).add(bo).add(po).add(note).addNullable(address)
        return ret
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        try
        {
            date = stream.deint64()
            isInflow = stream.deboolean()
            isChange = stream.deboolean()
            txHash = stream.denullHash()
            txIdx = stream.deuint32()
            priceWhenIssued = BigDecimal(stream.deString())
            priceWhatFiat = stream.deString()
            amount = stream.deuint64()
            val bo = stream.deString()
            val po = stream.deString()
            note = stream.deString()
            address = stream.deNullable({ PayAddress(it) })

            if (bo == "") basisOverride = null
            else basisOverride = BigDecimal(bo, currencyMath).setScale(currencyScale)
            if (po == "") saleOverride = null
            else saleOverride = BigDecimal(po, currencyMath).setScale(currencyScale)
            return stream
        }
        catch (e: NumberFormatException)
        {
            throw DeserializationException("Deserialization error in TransactionHistory")
        }
    }
}

@cli(Display.Simple, "A record of each transaction that interests a wallet")
class TransactionHistory(
  val chainSelector: ChainSelector,
  @property:cli(Display.Simple, "The transaction itself", 2) var tx: BCHtransaction
) : BCHserializable()
{
    @cli(Display.Simple, "Confirmation block's height, or -1 meaning unconfirmed")
    var confirmedHeight: Long = -1

    @cli(Display.Simple, "Confirmation block's hash, or null if unconfirmed")
    var confirmedHash: Hash256? = null

    @cli(Display.Simple, "Price of the crypto in fiat when this transaction was issued")
    var priceWhenIssued: BigDecimal = BigDecimal.ZERO

    @cli(Display.Simple, "What fiat currency the priceWhenIssued field is denominated in")
    var priceWhatFiat: String = ""

    @cli(Display.Simple, "Date this transaction was issued or confirmed")
    var date: Long = Instant.now().toEpochMilli()

    /** The wallet spent this quantity in this transaction */
    @cli(Display.Simple, "The wallet spent this amount")
    var outgoingAmt: Long = 0

    /** The wallet received this quantity in this transaction */
    @cli(Display.Simple, "The wallet received this amount")
    var incomingAmt: Long = 0

    /** Which indexes in the tx are this wallet paying someone else */
    @cli(Display.Dev, "Which tx output indexes are spends (to someone else)")
    val outgoingIdxes: MutableList<Long> = mutableListOf()

    /** Which indexes in this tx are this wallet receiving coins */
    @cli(Display.Dev, "Which tx outputs indexes are receives (to this wallet either incoming or change)")
    val incomingIdxes: MutableList<Long> = mutableListOf()

    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this(chainSelector, BCHtransaction(chainSelector))
    {
        BCHdeserialize(stream)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        val ret = BCHserialized(format) + date + confirmedHeight + (confirmedHash ?: Hash256()) + priceWhenIssued.toString() + priceWhatFiat + outgoingAmt + incomingAmt +
          tx + BCHserialized.list(outgoingIdxes, { BCHserialized.int32(it) }) + BCHserialized.list(incomingIdxes, { BCHserialized.int32(it) })
        return ret
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        try
        {
            date = stream.deint64()
            confirmedHeight = stream.deint64()
            confirmedHash = stream.denullHash()
            priceWhenIssued = BigDecimal(stream.deString())
            priceWhatFiat = stream.deString()
            outgoingAmt = stream.deuint64()
            incomingAmt = stream.deuint64()
            tx = BCHtransaction(chainSelector, stream)

            outgoingIdxes.clear()
            outgoingIdxes += stream.delist { it.deuint32() }
            incomingIdxes.clear()
            incomingIdxes += stream.delist { it.deuint32() }
            return stream
        }
        catch (e: NumberFormatException)
        {
            throw DeserializationException("Deserialization error in TransactionHistory")
        }
    }
}


fun chainStateDbKey(name: String, chainSelector: ChainSelector) = "wallet_" + name + "_chainstate_" + chainSelector
fun txStateDbKey(name: String) = "wallet_" + name + "_txstate"
fun unusedAddressesDbKey(name: String) = "wallet_" + name + "_unusedAddresses"
fun txHistoryDbKey(name: String) = "wallet_" + name + "_txHistory"
fun identityDomainsDbKey(name: String) = "wallet_" + name + "_identitydomains"
fun paymentHistoryDbKey(name: String) = "wallet_" + name + "_paymentHistory"

/** Delete a wallet on disk */
fun deleteWallet(ctxt: PlatformContext, walletName: String, chainSelector: ChainSelector)
{
    val db = OpenKvpDB(ctxt, "wallet_" + walletName)
    if (db == null) return
    deleteWallet(db, walletName, chainSelector)
}

fun deleteWallet(db: KvpDatabase, walletName: String, chainSelector: ChainSelector)
{
    val dbkey = "wallet_" + walletName + "chainstate_" + chainSelector
    db.delete(dbkey)
    db.delete("bip44wallet_" + walletName)
    db.delete(chainStateDbKey(walletName, chainSelector))
    db.delete(txStateDbKey(walletName))
    db.delete(unusedAddressesDbKey(walletName))
    db.delete(txHistoryDbKey(walletName))
    db.delete(paymentHistoryDbKey(walletName))
    db.delete(identityDomainsDbKey(walletName))
}

// Returns a predicate function that accepts a BCHspendable.  This function returns true if the BCHspendable is in the passed
// group and has properties based on passed flags.  If "normal" is true, any non-authority utxo will return true.
// If "authority" is a bitmap, returns true if these bits are set in the authorityFlags.
@kotlin.ExperimentalUnsignedTypes
fun groupedFilter(
  groupId: GroupId,
  normal: Boolean = true,
  authority: ULong = GroupAuthorityFlags.AUTHORITY
): (BCHspendable) -> Boolean
{
    // returns a filter function that selected only grouped inputs, either normal or authority (or both) depending on the flags
    return {
        val groupInfo = it.groupInfo()

        if (groupInfo == null) false
        else
        {
            if (groupInfo.groupId != groupId)
            {
                LogIt.info("groupId doesn't match")
                false
            }
            else
            {
                if (groupInfo.isAuthority())
                {
                    LogIt.info("Auth check: " + groupInfo.authorityFlags.toString(16) + " and " + authority.toString(16))
                    if ((groupInfo.authorityFlags and authority) == authority) true
                    else false
                }
                else
                {
                    if (normal) true
                    else false
                }
            }
        }
    }
}


fun signInput(tx: BCHtransaction, idx: Long, sigHashType: Int, serializedTx: ByteArray? = null): Boolean
{
    val inp = tx.inputs[idx.toInt()]
    val spendable = inp.spendable
    val secret = spendable.secret

    val flatTx = serializedTx ?: tx.BCHserialize(SerializationType.NETWORK).flatten()

    // if we don't know how to sign this input, then don't sign it.
    // this is also how the tx creator communicates to the wallet that this input need not be signed
    if ((secret != null && secret.getSecret().size != 0) || (inp.spendable.backingPayDestination != null))
    {
        /*
            //LogIt.info("Signing tx " + flatTx.ToHex() + ", " + sigHashType + ", " + count + ", " + inp.spendable.amount + ", " + inp.spendable.priorOutScript.flatten().ToHex() + ", " + secret.ToHex())
            val sig = signOneInputUsingECDSA(flatTx, sigHashType, idx, inp.spendable.amount, inp.spendable.priorOutScript.flatten(), secret)
            //val sigSchnorr = signOneInputUsingSchnorr(flatTx, sigHashType, count, inp.spendable.amount, inp.spendable.priorOutScript.flatten(), secret)

            // TODO call a function on the spendable that produces the satisfier script.  For now assume P2PKH
            val pubkey = PayDestination.GetPubKey(secret)
            inp.script = BCHscript(chainSelector, OP.push(sig), OP.push(pubkey))
            // DEBUG break the sig:
            //inp.script = BCHscript(chainSelector, OP.NOP, OP.push(pubkey))
         */
        val pd = inp.spendable.payDestination
        if (pd != null)  // This will be null if we can't understand the script.  But in that case secret ought to be null as well because the wallet was unable to bind one of its addresses to this output
            inp.script = pd.spendScript(flatTx, idx, sigHashType, inp.spendable.amount)
        return true
    }
    return false
}

// signs a transaction inplace, with all inputs that have a BCHspendable with secrets (skips those that do not so
// others can sign)
fun signTransaction(tx: BCHtransaction, sigHashType: Int = 0x41)
{
    val txSerialized = tx.BCHserialize(SerializationType.NETWORK)
    val flatTx = txSerialized.flatten()
    var changed = false

    for (idx in 0L until tx.inputs.size)
    {
        changed = signInput(tx, idx, sigHashType, flatTx) or changed
    }
    if (changed) tx.changed()  //  Clear out the hash (if it was requested) since it will have changed now that signatures are added.
}

/** This class provides implementations to for functions that are common to many blockchains and wallet types */
@cli(Display.Simple, "Most wallet functionality resides here")
abstract class CommonWallet(override val name: String, val chainSelector: ChainSelector) : Wallet()
{
    companion object
    {
        // Constants defining the data version of the wallet
        val CHAIN_STATE_SERIALIZED_VERSION: Byte = 1
        val TX_STATE_SERIALIZED_VERSION: Byte = 1
        val UNUSED_ADDRESSES_SERIALIZED_VERSION: Byte = 1
        val TX_HISTORY_SERIALIZED_VERSION: Byte = 1
        val PAYMENT_HISTORY_SERIALIZED_VERSION: Byte = 1
    }

    /** rebroadcast historical unconfirmed transactions every hour (and when we first start up) */
    val timeToResendHistoricalTx = Periodically(60L * 60000L)

    /** rebroadcast wallet transaction every minute */
    val timeToResendWalletTx = Periodically(60000)

    val WALLET_FLUSH_PERIOD = 30000   //!< Minimum wallet save interval, for recoverable operations

    /** If the wallet's state changes, it will call this function.  Use [setOnWalletChange]. */
    protected var walletChangeCallback: ((Wallet) -> Unit)? = null

    //  These variables wake up a processing thread to handle periodic tasks like chainstate sync and tx submission.
    //  These tasks are not implemented via co-routines triggered by events because we want both event trigger and periodic checking.
    //w val lock = ReentrantLock()
    //protected val cond = lock.newCondition()
    protected val CHECK_PERIOD: Long = 10000  // 10 seconds

    /**  Set to true to quit */
    protected var done = false

    // Set to true to pause, false to resume
    @Volatile
    override var pause = false

    /** protects the next 4 objects */
    protected val dataLock = ThreadCond()

    // Addresses that have already been generated and are in the bloom filter, but are not yet used
    @cli(Display.Dev, "available but unused address list.  Remove before using")
    protected val unusedAddresses: MutableList<PayAddress> = mutableListOf()

    /** Every receive address that has been generated (even if they have not been used or even provided to some external payer) */
    @cli(Display.Dev, "Every receive address that has been generated")
    protected var receiving: MutableMap<PayAddress, PayDestination> = mutableMapOf()

    /** all UTXOs that this wallet is capable of spending (and how to spend them) */
    @cli(Display.Dev, "all UTXOs that this wallet is capable of spending")
    protected var unspentTxos: MutableMap<BCHoutpoint, BCHspendable> = mutableMapOf()

    // offer only a read only version in the Wallet API because unspentByAddress, txHistory, paymentHistory, etc need to be
    // updated when a utxo is spent or received
    override val unspent: Map<BCHoutpoint, BCHspendable>
        get() = unspentTxos.toMap()


    /** all UTXOs that this wallet is capable of spending (like unspent), indexed by address */
    @cli(Display.Dev, "all UTXOs that this wallet is capable of spending, indexed by address")
    protected val unspentByAddress: MutableMap<PayAddress, MutableList<BCHoutpoint>> = mutableMapOf()

    /** All transactions that interest this wallet -- either sent or received */
    @cli(Display.User, "All transactions that interest this wallet, sent or received, indexed by transaction hash")
    override public var txHistory: MutableMap<Hash256, TransactionHistory> = mutableMapOf()

    /** All payments that interest this wallet -- either sent or received */
    @cli(Display.User, "All transactions that interest this wallet, sent or received, indexed by outpoint")
    override public var paymentHistory: MutableMap<BCHoutpoint, PaymentHistory> = mutableMapOf()

    data class WalletStatistics(val numUnusedAddrs: Int, val numUsedAddrs: Int, val numUnspentTxos: Int)

    fun statistics(): WalletStatistics
    {
        return WalletStatistics(unusedAddresses.size, receiving.size - unusedAddresses.size, unspentTxos.size)
    }

    /** After startup, chainstate should always be set.  It connects this wallet to a particular location in the blockchain */
    @cli(Display.User, "connection between this wallet and its blockchain")
    var chainstate: GlueWalletBlockchain? = null

    var walletDb: KvpDatabase? = null

    /** Thread that handles keeping wallet synced
     */
    var processingThread: Thread? = null

    /** Get a set of every address in this wallet */
    // w@cli(Display.User, "every address (generated so far) in this wallet")
    val allAddresses: MutableSet<PayAddress>
        get() = receiving.keys

    /** transactions that this wallet has generated that have not yet been confirmed */
    @cli(Display.User, "New transactions that have not yet been confirmed")
    var pendingTx: MutableMap<Hash256, ByteArray> = mutableMapOf()

    override val blockchain: Blockchain
        get() = chainstate!!.chain

    override val syncedHeight: Long
        get() = chainstate?.syncedHeight ?: -1

    /** wake up the processing loop early */
    protected var wakey = ThreadCond()

    /** for inessential wallet operations (like syncing with the blockchain), we allow periodic flush.  If the program fails between flush periods, a few seconds of work must be redone */
    protected val flushWalletPeriodically = Periodically(WALLET_FLUSH_PERIOD.toLong())

    var identityDomain: MutableMap<String, IdentityDomain> = mutableMapOf()
    var identityDomainChanged = false

    // enable long delays to be changed, primarily for tests where activities happen rapidly
    var LONG_DELAY_INTERVAL = 5000L

    public override fun toString(): String
    {
        val ret = StringBuilder()
        ret.append("""{ "type":"CommonWallet", "name": "$name", "blockchain" : "$chainSelector", "pause" : "$pause", "timeToResendWalletTx" : $timeToResendWalletTx, """)
        ret.append(""" "unusedAddresses":"[...${unusedAddresses.size}...]", "receiving": "[...${receiving.size}...]", "unspent" : "[...${unspentTxos.size}...]", "txHistory" : "{...${txHistory.size}...}", "paymentHistory" : "{...${paymentHistory.size}...}", """)
        ret.append(""" "pendingTx":"{...${pendingTx.size}...}", "identityDomain":"{...${identityDomain.size}...}" }""")
        return ret.toString()
    }

    @cli(Display.Dev, "find and return the secret corresponding to the passed pubkey.")
    override fun pubkeyToSecret(pubkey: ByteArray): Secret?
    {
        for ((_, d) in receiving)
        {
            if ((d.pubkey.contentEquals(pubkey)) && (d.secret != null)) return d.secret
        }
        return null
    }

    /** Forget about all unconfirmed transactions in the wallet.  It the transactions are in the network and are confirmed they will be added to the wallet at that point.
     * This API is used to clear out tx that will never confirm for some reason.
     * This API causes the wallet to forget about the inputs that weren't confirmed.  "Rediscover" can get those back
     */
    @cli(Display.Dev, "Forget about unconfirmed transactions in the wallet")
    fun cleanUnconfirmed(before: Long = Long.MAX_VALUE)
    {
        val muit = txHistory.iterator()
        val forget = mutableListOf<TransactionHistory>()
        for ((hash, hist) in muit)
        {
            if (hist.confirmedHeight <= 0) // this tx is not confirmed yet
            {
                LogIt.info("TX ${hash} is unconfirmed since ${hist.date}")
                if (hist.date < before) forget.add(hist)
            }
        }
        cleanUnconfirmed(forget)
    }

    /** Forget about certain transactions in the wallet.  If the transactions are in the network and are later confirmed they will be added to the wallet then.
     * This API is used to clear out tx that will never confirm for some reason.
     * This API causes the wallet to forget about the inputs that weren't confirmed.  "Rediscover" can get those back
     */
    @cli(Display.Dev, "Forget about unconfirmed transactions in the wallet")
    fun cleanUnconfirmed(lst: MutableList<TransactionHistory>)
    {
        for (hist in lst)
        {
            LogIt.info("Cleaning up ${hist.tx.hash}")
            txHistory.remove(hist.tx.hash)
            pendingTx.remove(hist.tx.hash)

            for (out: BCHoutpoint in hist.tx.outpoints)
            {
                unspentTxos.remove(out)
                paymentHistory.remove(out)
            }
            for (out: BCHoutput in hist.tx.outputs)
            {
                unspentByAddress.remove(out.script.address)
            }
        }
        save()
    }

    /** If you clear the "receiving" addresses map, you need to fill them back up with derived class injected destinations using this function */
    fun fillReceivingWithRetrieveOnly()
    {
        val rods = getRetrieveOnlyDestinations()

        for (r in rods)
        {
            r.address?.let {
                receiving[it] = r
            }
        }
    }

    /** Forget all transaction and blockchain state, and the redo the search for wallet transactions.
     * This is intended for testing and debug
     */
    @cli(Display.Dev, "Forget all transaction and blockchain state, and the redo the search for wallet transactions.")
    override fun rediscover(forgetAddresses: Boolean): Unit
    {
        val cs = chainstate
        if (cs != null)
        {
            synchronized(dataLock) {
                unspentTxos.clear()
                unspentByAddress.clear()
                txHistory.clear()
                paymentHistory.clear()
                if (forgetAddresses)
                {
                    unusedAddresses.clear()
                    receiving.clear()
                    generateDestinationsInto(unusedAddresses)
                    fillReceivingWithRetrieveOnly()
                }
            }
            try
            {
                cs.resetToPrehistory()
            }
            catch (_: Exception)
            {
                cs.resetToCheckpoint()
            }

            regenerateBloom { }
            save()
            walletChangeCallback?.invoke(this)
            wakey.wake()  // Wake up wallet processing
        }
    }

    /** Insert a record into the unspentByAddress structure */
    fun insertUnspentByAddress(addr: PayAddress?, outpoint: BCHoutpoint)
    {
        if (addr == null) return
        synchronized(dataLock) {
            var lst = unspentByAddress.getOrPut(addr) { mutableListOf() }
            if (!lst.contains(outpoint))
            {
                lst.add(outpoint)
            }
        }
    }

    override fun suggestFee(tx: BCHtransaction, pad: Int, priority: Int): Long
    {
        // This function just returns the size of the tx for BCH-style 1 sat/byte blockchains, but
        // really it should compute the size and other properties of the tx and pass this information to the underlying blockchain object for fee calculation.

        return tx.size.toLong() + pad
    }


    @Synchronized
    open fun delete()
    {
        walletDb?.let { deleteWallet(it, name, chainstate?.chain?.chainSelector!!) }
        chainstate?.delete()
        chainstate = null
    }

    @Synchronized
    override fun save(force: Boolean)
    {
        saveWalletTo(walletDb)
    }

    /** Save wallet transaction state to the database */
    @Synchronized
    open fun saveWalletTo(db: KvpDatabase?)
    {
        val st = SerializationType.DISK
        if (db == null) return
        chainstate?.let {
            // Even though this doesn't use chainstate, don't store it if for some reason we don't have a blockchain yet. Because it makes sense to be consistent
            // with chainstate
            var unspentSize: Int
            var receivingSize: Int
            val txSer = BCHserialized.uint8(TX_STATE_SERIALIZED_VERSION, st) + synchronized(dataLock)
            {
                unspentSize = unspentTxos.size
                receivingSize = receiving.size
                // val realUnspent = unspent.filter{ it.value.amount != -1L }
                val v = BCHserialized.map(unspentTxos, st)
                v.add(BCHserialized.map(receiving, st))
                v
            }
            assert(txSer.format == st)
            val unusedAddrs = synchronized(dataLock)
            {
                BCHserialized.uint8(UNUSED_ADDRESSES_SERIALIZED_VERSION, st)
                  .add(BCHserialized.list(unusedAddresses, SerializationType.DISK))
            }
            assert(unusedAddrs.format == st)
            val hist = synchronized(dataLock)
            {
                BCHserialized.uint8(TX_HISTORY_SERIALIZED_VERSION, st).add(BCHserialized.map(txHistory, SerializationType.DISK))
            }
            assert(hist.format == st)
            val phist = synchronized(dataLock)
            {
                BCHserialized.uint8(PAYMENT_HISTORY_SERIALIZED_VERSION, st).add(BCHserialized.map(paymentHistory, SerializationType.DISK))
            }
            assert(hist.format == st)
            val chainStateData = BCHserialized.uint8(CHAIN_STATE_SERIALIZED_VERSION, st).add(it.BCHserialize(st))
            assert(chainStateData.format == st)

            //val gwb = GlueWalletBlockchain(it.chain)
            //val tmp = gwb.BCHdeserialize(BCHserialized(chainStateData, SerializationType.DISK))  // DEBUG test deserialize right away
            db.set(chainStateDbKey(name, chainSelector), chainStateData.flatten())
            db.set(txStateDbKey(name), txSer.flatten())
            db.set(unusedAddressesDbKey(name), unusedAddrs.flatten())
            db.set(txHistoryDbKey(name), hist.flatten())
            db.set(paymentHistoryDbKey(name), phist.flatten())

            if (identityDomainChanged)
            {
                identityDomainChanged = false
                val identityDomainSerialized: ByteArray = BCHserialized.map(identityDomain, { BCHserialized(SerializationType.DISK).add(it) }, { it.BCHserialize(SerializationType.DISK) }, SerializationType.DISK).flatten()
                db.set(identityDomainsDbKey(name), identityDomainSerialized)

            }
            LogIt.info(sourceLoc() + name + ": Wallet state saved: ${unspentSize} utxos,  ${receivingSize} receiving addresses.")
        }
    }

    open fun loadPendingTxFromUnspent(throwIllegal: Boolean = true)
    {
        for ((k, v) in unspentTxos)
        {
            if (v.spentHeight == -1L && (v.spendableUnconfirmed > 0) && (v.spentUnconfirmed == false))
            // no longer needed: caused by messy database && (commitTx != null) && (commitTx.inputs.size != 0))
            {
                val tx = v.commitTx ?: txHistory[k.txid]?.tx
                if (tx != null)
                {
                    val txbytes = tx.BCHserialize(SerializationType.NETWORK).flatten()
                    if (txbytes.size < 100)  // TODO: reference chain consensus parameter rather than 100
                    {
                        if (throwIllegal) throw TransactionException("Transaction is too small at ${txbytes.size} bytes")
                    }
                    else
                    {
                        LogIt.info("pending TX:")
                        tx.debugDump()
                        LogIt.info("Hex TX:")
                        val hex = txbytes.toHex();
                        for (c in hex.chunked(900))
                        {
                            LogIt.info(c)
                        }

                        if (v.commitTxHash != Guid(tx.hash))
                        {
                            LogIt.severe("inconsistent data structure: tx and its hash")
                        }

                        pendingTx[tx.hash] = txbytes
                    }
                }
                else
                {
                    // Should never happen, the database is inconsistent
                    LogIt.info(sourceLoc() + name + ": Pending tx unrecoverable, try rediscovering")
                }
            }
        }
    }

    fun loadChainState(cs: GlueWalletBlockchain, db: KvpDatabase, key: String)
    {
        val chData = db.get(key)
        val chStream = BCHserialized(chData, SerializationType.DISK)

        val chainstateVer = chStream.debytes(1)[0]
        if (chainstateVer != CHAIN_STATE_SERIALIZED_VERSION) throw DeserializationException("Invalid version in chain state")
        cs.BCHdeserialize(chStream)
    }

    /** Load wallet transaction state from the database */
    @Synchronized
    open fun loadWalletTx(db: KvpDatabase): Boolean
    {
        var ret = false
        try
        {
            chainstate?.let {
                var corrupt = false

                try
                {
                    LogIt.info(sourceLoc() + " " + name + ": Loading wallet")
                    val txData = db.get(txStateDbKey(name))
                    val addrData = db.get(unusedAddressesDbKey(name))
                    val histData = db.get(txHistoryDbKey(name))
                    val paymentHistData = db.get(paymentHistoryDbKey(name))

                    val addrStream = BCHserialized(addrData, SerializationType.DISK)
                    val txStream = BCHserialized(txData, SerializationType.DISK)
                    val histStream = BCHserialized(histData, SerializationType.DISK)
                    val paymentHistStream = BCHserialized(paymentHistData, SerializationType.DISK)
                    LogIt.info(sourceLoc() + " " + name + ": wait for dataLock")
                    synchronized(dataLock)
                    {
                        LogIt.info(sourceLoc() + " " + name + ": got dataLock")
                        loadChainState(it, db, chainStateDbKey(name, chainSelector))

                        LogIt.info(sourceLoc() + " " + name + ": Loaded wallet blockchain state at: ${it.syncedHash}:${it.syncedHeight}")

                        val unspentVer = txStream.debytes(1)[0]
                        if (unspentVer != TX_STATE_SERIALIZED_VERSION) throw DeserializationException("Invalid version in transaction state data")
                        unspentTxos = txStream.demap({ BCHoutpoint(it) }, { BCHspendable(chainSelector, it) })
                        LogIt.info(sourceLoc() + " " + name + ": Loaded unspent")
                        receiving = txStream.demap({ PayAddress(it) }, { Pay2PubKeyHashDestination(chainSelector, it) })
                        LogIt.info(sourceLoc() + " " + name + ": Loaded receiving")

                        val histVer = histStream.debytes(1)[0]
                        if (histVer != TX_HISTORY_SERIALIZED_VERSION) throw DeserializationException("Invalid version in transaction history")
                        txHistory = histStream.demap({ Hash256(it) }, { TransactionHistory(chainSelector, it) })
                        LogIt.info(sourceLoc() + " " + name + ": Loaded tx history")

                        val payVer = paymentHistStream.debytes(1)[0]
                        if (payVer != PAYMENT_HISTORY_SERIALIZED_VERSION) throw DeserializationException("Invalid version in payment history")
                        paymentHistory = paymentHistStream.demap({ BCHoutpoint(it) }, { PaymentHistory(chainSelector, it) })
                        LogIt.info(sourceLoc() + " " + name + ": Loaded payment history")

                        unusedAddresses.clear()  // should be empty on load anyway
                        val uaVer = addrStream.debytes(1)[0]
                        if (uaVer != UNUSED_ADDRESSES_SERIALIZED_VERSION) throw DeserializationException("Invalid version in unused addresses")
                        var uaList = addrStream.delist { PayAddress(it) }
                        unusedAddresses.addAll(uaList)

                        unspentByAddress.clear()
                        for (u in unspentTxos)
                        {
                            insertUnspentByAddress(u.value.addr, u.key)
                        }
                        LogIt.info(name + ": Wallet state loaded: ${unspentTxos.size} utxos,  ${receiving.size} receiving addresses.")
                    }
                }
                catch (e: DeserializationException)
                {
                    LogIt.info("Deserialization exception: " + e.message)
                    val os = ByteArrayOutputStream()
                    e.printStackTrace(PrintStream(os))
                    LogIt.info(os.toString("UTF8"))
                    corrupt = true
                }
                catch (e: UnknownBlockchainException)
                {
                    LogIt.info("Unknown blockchain: " + e.message)
                    val os = ByteArrayOutputStream()
                    e.printStackTrace(PrintStream(os))
                    LogIt.info(os.toString("UTF8"))
                    corrupt = true
                }
                catch (e: DataMissingException)
                {
                    LogIt.info("Data missing: " + e.message)
                    val os = ByteArrayOutputStream()
                    e.printStackTrace(PrintStream(os))
                    LogIt.info(os.toString("UTF8"))
                    corrupt = true
                }

                if (corrupt)
                {
                    LogIt.info("Stored data is corrupt, rediscovering")
                    launch { rediscover(true) }
                }
                ret = !corrupt

                corrupt = false;  // Identity data failure does not require wallet reset
                try
                {
                    val idData = db.get(identityDomainsDbKey(name))
                    val idStream = BCHserialized(idData, SerializationType.DISK)
                    identityDomain = idStream.demap({ val s = it.deString(); LogIt.info(s); s }, { IdentityDomain(it) })
                    identityDomainChanged = false
                }
                catch (e: DeserializationException)
                {
                    LogIt.info("Deserialization exception: " + e.message)
                    val os = ByteArrayOutputStream()
                    e.printStackTrace(PrintStream(os))
                    LogIt.info(os.toString("UTF8"))
                    corrupt = true
                }
                catch (e: UnknownBlockchainException)
                {
                    LogIt.info("Unknown blockchain: " + e.message)
                    val os = ByteArrayOutputStream()
                    e.printStackTrace(PrintStream(os))
                    LogIt.info(os.toString("UTF8"))
                    corrupt = true
                }
                catch (e: DataMissingException)  // This is normal; identity is linked to only one blockchain
                {
                    LogIt.info("Data missing: " + e.message)
                    //val os = ByteArrayOutputStream()
                    //e.printStackTrace(PrintStream(os))
                    //LogIt.info(os.toString("UTF8"))
                    corrupt = true
                }
                if (corrupt)
                {
                    LogIt.info("Identitiy domain data is corrupt or missing, not loading")
                    // throw RuntimeException("data corruption")
                }
            }

            loadPendingTxFromUnspent(false)
        }
        catch (e: DataMissingException)
        {
            LogIt.warning("missing wallet chain and tx state data.")
            return false
        }
        catch (e: java.lang.IndexOutOfBoundsException)
        {
            LogIt.warning("corrupt wallet chain and tx state data.")
            return false
        }
        catch (e: PayAddressDecodeException)
        {
            LogIt.warning("corrupt wallet chain and tx state data (PayAddress)")
            return false
        }
        return ret
    }

    /** Install a change handler that will get called whenever this wallet's state changes */
    override fun setOnWalletChange(callback: ((wallet: Wallet) -> Unit)?)
    {
        walletChangeCallback = callback
    }

    override fun send(amountSatoshis: Long, destAddress: String, deductFeeFromAmount: Boolean, sync: Boolean): BCHtransaction
    {
        val dest = PayAddress(destAddress)
        if (dest.blockchain != chainSelector) throw WalletIncompatibleAddress("Cannot send.  The destination address is for a different cryptocurrency")
        return send(amountSatoshis, dest, deductFeeFromAmount, sync)
    }

    override fun send(amountSatoshis: Long, destAddress: PayAddress, deductFeeFromAmount: Boolean, sync: Boolean): BCHtransaction
    {
        if (destAddress.blockchain != chainSelector) throw WalletIncompatibleAddress("Cannot send.  The destination address is for a different cryptocurrency")
        return send(amountSatoshis, destAddress.outputScript(), deductFeeFromAmount, sync)
    }

    @cli(Display.Simple, "confirmed balance")
    override val balance: Long
        get()
        {
            var ret = 0.toLong()
            synchronized(dataLock) {
                for (u in unspentTxos.values)
                {
                    if ((u.spentHeight == -1L) && (u.spentUnconfirmed == false) && (u.spendableUnconfirmed == 0.toLong()))
                        ret += u.amount
                }
            }
            return ret
        }

    @cli(Display.User, "unconfirmed transaction list")
    fun getUnconfirmedTx(): MutableList<BCHspendable>
    {
        var ret = mutableListOf<BCHspendable>()
        synchronized(dataLock)
        {
            for (u in unspentTxos.values)
            {
                // Skip if the output was subsequently spent by an unconfirmed tx
                if ((u.spentHeight == -1L) && (u.spendableUnconfirmed > 0) && (u.spentUnconfirmed == false))
                    ret.add(u)
            }
        }
        return ret
    }

    @cli(Display.Simple, "unconfirmed balance")
    override val balanceUnconfirmed: Long
        get()
        {
            var ret = 0.toLong()
            synchronized(dataLock)
            {
                for (u in unspentTxos.values)
                {
                    // Skip if the output was subsequently spent by an unconfirmed tx
                    if ((u.spentHeight == -1L) && (u.spendableUnconfirmed > 0) && (u.spentUnconfirmed == false)
                      // And if this is not a grouped output (yes this could skip grouped BCH or BCH just in the group output)
                      && (u.groupInfo() == null))
                        ret += u.amount
                }
            }
            return ret
        }


    /** Return identity domain data if this domain has previously been used */
    override fun lookupIdentityDomain(name: String): IdentityDomain?
    {
        return identityDomain[name]
    }

    /** Add or update identity domain data */
    override suspend fun upsertIdentityDomain(id: IdentityDomain)
    {
        identityDomainChanged = true
        identityDomain[id.domain] = id
    }

    override fun allIdentityDomains(): Collection<IdentityDomain>
    {
        return identityDomain.values
    }

    /** track this transaction and periodically resubmit until it has been committed.
     * @param tx: the wallet transaction to commit
     * @param txbytes: OPTIONAL: the network-serialized wallet transaction (for efficiency if the caller has already serialized it)
     * @throws */
    suspend fun commitWalletTransaction(tx: BCHtransaction, txbytes: ByteArray? = null)
    {
        val txb = txbytes ?: tx.BCHserialize(SerializationType.NETWORK).flatten()
        pendingTx[tx.hash] = txb
        val req = chainstate?.chain?.req
        // other wallets in this device may not get notifications for this tx because I sent it
        // by pushing this into the req mgr, it calls back into ALL the connected wallets
        if (req != null)
            req.onUnconfirmedTx(mutableListOf(tx))
        else // but if I'm offline just notify this wallet only
            interestingUnconfirmedTx(mutableListOf(tx))
        launch { save() }  // do this out-of-band so UI response is quicker
        if (tx.size < 100)
        {
            throw IllegalStateException("txbytes is too small")
        }
        chainstate?.chain?.net?.broadcastTransaction(txb)
    }

    override fun prepareSend(outputs: MutableList<BCHoutput>, minConfirms: Int, deductFeeFromAmount: Boolean): BCHtransaction
    {
        var total = 0L
        for (out in outputs)
        {
            total += out.amount
        }

        val (signedTx, _) = txConstructor(total, deductFeeFromAmount, minConfirms)
        { tx, _ ->
            assert(deductFeeFromAmount == false)  // TODO: actually deduct
            for (out in outputs)
            {
                tx.outputs.add(out)
            }
            deductFeeFromAmount
        }

        return signedTx
    }

    /*
    override fun prepareSend(tx: BCHtransaction, minConfirms: Int)
    {

        txModifier(tx, totalIn, totalOut, false, minConfirms)

        return tx
    }
     */

    override fun abortTransaction(tx: BCHtransaction)
    {
        synchronized(dataLock) // Give all the unspent back to the general use pool since we've aborted this inprogress spend
        {
            for (inp in tx.inputs)
            {
                inp.spendable.reserved = 0
            }
        }
    }


    //* Send funds to this destination
    override fun send(amountSatoshis: Long, destScript: BCHscript, deductFeeFromAmount: Boolean, sync: Boolean): BCHtransaction
    {
        val (signedTx, serializedTx) = try  // Try at least 1 confirmation first
        {
            txConstructor(amountSatoshis, deductFeeFromAmount, 1) { tx, fee ->
                // Add the output that we are sending to
                var output = BCHoutput(chainSelector)
                output.amount = if (deductFeeFromAmount) amountSatoshis - fee else amountSatoshis
                output.script = destScript
                tx.outputs.add(output)
                deductFeeFromAmount
            }
        }
        catch (e: WalletNotEnoughBalanceException)  // Try unconfirmed
        {
            txConstructor(amountSatoshis, deductFeeFromAmount, 0) { tx, fee ->
                // Add the output that we are sending to
                var output = BCHoutput(chainSelector)
                output.amount = if (deductFeeFromAmount) amountSatoshis - fee else amountSatoshis
                output.script = destScript
                tx.outputs.add(output)
                deductFeeFromAmount
            }
        }

        LogIt.info(sourceLoc() + " " + name + ": Sending TX " + signedTx.hash.toHex())
        LogIt.info(sourceLoc() + " " + name + ": TX hex " + serializedTx.toHex())
        if (sync)
            runBlocking { commitWalletTransaction(signedTx, serializedTx) }
        else launch { commitWalletTransaction(signedTx, serializedTx) }
        return signedTx

    }

    //* Send funds to this destination
    override fun send(tx: BCHtransaction, sync: Boolean)
    {
        val serializedTx = tx.BCHserialize(SerializationType.NETWORK).flatten()
        LogIt.info(sourceLoc() + " " + name + ": Sending TX " + tx.hash.toHex())
        LogIt.info(sourceLoc() + " " + name + ": TX hex " + serializedTx.toHex())
        if (sync)
            runBlocking { commitWalletTransaction(tx, serializedTx) }
        else launch { commitWalletTransaction(tx, serializedTx) }
    }

    //* publish any unconfirmed transactions to the network
    @cli(Display.User, "sends all tx marked unconfirmed to the network.")
    fun resendUnconfirmedTx()
    {
        val v = chainstate?.chain?.net
        if (v != null)
        {
            val txHist = unconfirmedTx()

            for (i in txHist)
            {
                if (i.confirmedHeight < 1)
                {
                    if (!i.tx.isCoinbase())  // If a coinbase somehow got into our history, if we ever relay it we will be banned.  So don't.
                    {
                        v.broadcastTransaction(i.tx.BCHserialize(SerializationType.NETWORK).flatten())
                        LogIt.info("Resending ${i.tx.hash} confirmedHeight is: ${i.confirmedHeight}")
                        LogIt.info("Hex: ${i.tx.toHex()}")
                    }
                }
            }
        }
    }

    /** This function re-checks all tx marked unconfirmed in this wallet to see if they are actually unconfirmed.
     * It uses an electrumx server to do so, so may be unavailable if electrumx is not supported or no servers are available. */
    @cli(Display.User, "re-checks all tx marked unconfirmed in this wallet to see if they are actually unconfirmed.")
    fun reassessUnconfirmedTx()
    {
        val reqMgr = chainstate?.chain?.req ?: throw ElectrumNoNodesException()
        val unTxes = unconfirmedTx()
        val forgetTxes: MutableList<TransactionHistory> = mutableListOf()
        for (unTx in unTxes)
        {
            val retmsg = reqMgr.getTxDetails(unTx.tx.hash)
            val result = retmsg.jsonObject["result"]?.jsonObject
            if (result != null)
            {
                val confBlock: String? = result.get("blockhash")?.toString()?.replace(""""""", "")
                val confHeight = result.get("height")?.toString()?.toLong()
                val confTime = result.get("time")?.toString()?.toLong()
                // TODO don't trust the server; get a merkle proof

                if ((confBlock != null) && (confHeight != null) && (confTime != null))
                {
                    interestingConfirmedTx(listOf(unTx.tx), Hash256(confBlock), confHeight, confTime)
                }
                continue
            }
            val error = retmsg.jsonObject["error"]?.jsonObject
            if (error != null)
            {
                LogIt.info("Tx ${unTx.tx.hash} reassess error: " + error["code"])
                if (error["code"].toString() == "-32603")  // This transaction does not exist
                {
                    forgetTxes.add(unTx)
                }
            }
        }

        cleanUnconfirmed(forgetTxes)
    }


    //* get unconfirmed tx sorted by date
    fun unconfirmedTx(): MutableList<TransactionHistory>
    {
        val ret = mutableListOf<TransactionHistory>()
        synchronized(dataLock) {
            for (i in txHistory.values)
            {
                if (i.confirmedHeight < 1) ret.add(i)
            }
        }
        ret.sortBy { it.date }
        return ret
    }

    // This function analyses a script for template op codes, and fills in wallet specific data where requested.
    fun bindScriptToWallet(s: BCHscript): BCHscript
    {
        return s.replace {
            if (it.contentEquals(OP.TMPL_PUBKEYHASH))
            {
                val d = runBlocking { newDestination() }
                BCHscript(s.chainSelector) + OP.push(d.pkh()!!)
            }
            else if (it.contentEquals(OP.TMPL_SCRIPT))
            {
                val d = runBlocking { newDestination() }
                d.outputScript()
            }
            else if (it.contentEquals(OP.TMPL_PUBKEY))
            {
                val d = runBlocking { newDestination() }
                BCHscript(s.chainSelector) + OP.push(d.pubkey!!)
            }
            else
            {
                throw NotImplementedError()
            }
        }

    }

    val oneTxConstructor = true

    // Find inputs needed to supply funds for this transaction.
    // if inputAmount is non-null, assume existing inputs supply this number of satoshis (do not look up these inputs)
    // If change outputs are required, add them.
    // If mint baton passing outputs are possible then add them if equalizeAuthorities=true
    // If useAuthorities = true, pull in authorities if needed (and available) to handle (mint/melt) operations
    // If fund = true, add native crypto inputs to pay for the transaction
    // If signSingle = true, the signature just covers the corresponding (by idx) output
    @kotlin.ExperimentalUnsignedTypes
    override fun txCompleter(tx: BCHtransaction, minConfirms: Int, flags: Int, inputAmount: Long?) //, signSingle:Boolean = false)
    {
        val DUST: Long =
          Dust(chainSelector)  // Below this many satoshis its not even worth making an output -- just give to miner
        val MAX_FEE_OVERPAY: Long = DUST * 3
        val AVG_OUTPUT_SIZE: Long = 34

        // Track what's coming in and going out for each group
        data class GroupIO(
          var groupId: GroupId, var tokenI: Long, var tokenO: Long, var authorityFlagsI: ULong,
          var authorityFlagsO: ULong
        )

        val groupData = mutableMapOf<GroupId, GroupIO>()

        // Fill any parameterized scripts with addresses from this wallet and
        // Fill the groupData structure with all the output groups and token quantities.
        for (out in tx.outputs)
        {
            val constraint = if ((flags and TxCompletionFlags.BIND_OUTPUT_PARAMETERS) > 0)
            {
                val tmp = bindScriptToWallet(out.script)
                LogIt.info(sourceLoc() + ": Bind script parameterization: ${out.script.toHex()} -> ${tmp.toHex()}")
                out.script = tmp
                tmp
            }
            else out.script

            val cgdata = constraint.groupInfo(out.amount) ?: continue  // If ungrouped, nothing more to do
            val d = groupData[cgdata.groupId]
            if (d == null) groupData[cgdata.groupId] = GroupIO(cgdata.groupId, 0, cgdata.tokenAmt, 0.toULong(), cgdata.authorityFlags)
            else
            {
                d.tokenO += cgdata.tokenAmt
                d.authorityFlagsO = d.authorityFlagsI or cgdata.authorityFlags
            }

        }

        // Figure out what caller has supplied.
        for (inp in tx.inputs)
        {
            val prevout = inp.spendable
            val cgdata = prevout.priorOutScript.groupInfo(prevout.amount) ?: continue  // continue if ungrouped
            val d = groupData[cgdata.groupId] ?: continue // Well, there's an existing input with no corresponding output... either tx is melting or won't validate (not our problem)

            d.tokenI += cgdata.tokenAmt
            d.authorityFlagsI = d.authorityFlagsI or cgdata.authorityFlags  // if the caller supplied an authority bit
        }

        if ((flags and TxCompletionFlags.FUND_GROUPS) > 0)
        {
            synchronized(oneTxConstructor)
            {
                // supply inputs and outputs for the difference between what's coming in and going out

                for ((gid, g) in groupData)
                {
                    if (g.tokenI < g.tokenO) // Supply token input or mint authority
                    {
                        val inputs =
                          filterInputs(minConfirms, groupedFilter(g.groupId, true, GroupAuthorityFlags.NO_AUTHORITY))
                        val gAmt = inputs.fold(0L,
                          { acc, utxo -> acc + utxo.groupInfo()!!.tokenAmt })  // how much do I have available
                        if (gAmt + g.tokenI >= g.tokenO)  // Ok don't need to mint
                        {
                            // TODO quantity-sensitive selection
                            while (g.tokenI < g.tokenO)
                            {
                                val utxo = inputs.removeAt(0)
                                val gi = utxo.groupInfo()!!  // filter did not work if null
                                tx.inputs.add(BCHinput(utxo))
                                g.tokenI += gi.tokenAmt
                            }

                        }
                        else
                        {
                            if ((flags and TxCompletionFlags.USE_GROUP_AUTHORITIES) == 0)
                            {
                                LogIt.info(sourceLoc() + ": Token ${gid.toHex()} input qty: ${g.tokenI} output qty: ${g.tokenO} and wallet has not enough balance: ${gAmt}")
                                throw WalletNotEnoughTokenBalanceException(appI18n(RsendMoreThanBalance))
                            }
                            var auths = filterInputs(minConfirms, groupedFilter(g.groupId, false, GroupAuthorityFlags.MINT))
                            if (auths.isEmpty())
                            {
                                // If we have no mint auths, look to see if this is a subgroup and whether we have mint through the parent
                                if (g.groupId.isSubgroup())
                                {
                                    auths = filterInputs(minConfirms,
                                      groupedFilter(g.groupId.parentGroup(),
                                        false,
                                        GroupAuthorityFlags.SUBGROUP or GroupAuthorityFlags.MINT))
                                }
                                if (auths.isEmpty())
                                {
                                    LogIt.info(sourceLoc() + ": Token ${gid.toHex()} input qty: ${g.tokenI} output qty: ${g.tokenO} and wallet has not enough balance: ${gAmt} and no auths")
                                    throw WalletNotEnoughTokenBalanceException(appI18n(RsendMoreThanBalance))
                                }
                            }

                            val auth = auths[0]
                            val authGroupInfo = auth.groupInfo()
                            tx.inputs.add(BCHinput(auth))
                            // If this authority can be passed to a child, make an output so we don't lose this ability
                            if ((authGroupInfo!!.authorityFlags and GroupAuthorityFlags.BATON) > 0.toULong())
                            {
                                // TODO do not reuse the authority address, note that we need to handle subgroups here when fixing
                                tx.outputs.add(BCHoutput(Dust(chainSelector), auth.priorOutScript))
                            }
                        }
                    }
                    // The above code could include too many tokens, meaning we now need to make change,
                    // or the caller supplied tx could have done the same

                    if (g.tokenI > g.tokenO) // Create token change output
                    {
                        val co = createGroupedChangeOutput(gid, g.tokenI - g.tokenO)
                        tx.outputs.add(co)
                        g.tokenO = g.tokenI  // In case we use it later
                    }

                }
            }
        }

        // TODO now that the groups are equalized, equalize the satoshis
        if ((flags and TxCompletionFlags.FUND_NATIVE) > 0)
        {
            var outAmt = tx.outputs.fold(0L, { a, b -> a + b.amount })
            var inAmt = inputAmount ?: tx.inputs.fold(0L, { a, b -> a + b.spendable.amount })

            var outFee = feeForSize((tx.size + (APPROX_P2PKH_SIG_SCRIPT_LEN * tx.inputs.size)).toLong())

            if (inAmt < outAmt + outFee)  // Need to pull in more sats
            {
                val inputs = findInputs(outAmt + outFee - inAmt, minConfirms)

                if (inputs == null)
                {
                    LogIt.info(sourceLoc() + ": Wallet cannot find enough sats with ${minConfirms} confirms. Need ${outAmt} + ${outFee} - $inAmt = ${outAmt + outFee - inAmt}")
                    throw WalletNotEnoughBalanceException(appI18n(RsendMoreThanBalance))
                }

                while ((inAmt < outAmt + outFee) && !inputs.isEmpty())
                {
                    val utxo = inputs.removeAt(0)
                    tx.inputs.add(BCHinput(utxo))
                    inAmt += utxo.amount

                    outFee += feeForSize((APPROX_P2PKH_SIG_SCRIPT_LEN + TX_SCRIPTLESS_INPUT_SIZE).toLong())
                }
            }

            // Since the fee is increasing while we add inputs, its possible we'll run out of utxos, so double check that we did it
            if (inAmt < outAmt + outFee)
            {
                LogIt.info(sourceLoc() + ": Adding inputs caused too large fee. Provided ${inAmt}.  Needed ${outFee} + ${outAmt} = ${outAmt + outFee}")
                throw WalletNotEnoughBalanceException(appI18n(RsendMoreThanBalance))
            }

            // Now add some change if needed
            if (inAmt > outAmt + outFee + MAX_FEE_OVERPAY)  // TODO dump extra dust into group outputs for later fee paying
            {
                outFee += feeForSize(AVG_OUTPUT_SIZE)  // Add fee for the change output
                val co = createChangeOutput(inAmt - (outAmt + outFee))
                tx.outputs.add(co)
            }
        }

        // Sign the transaction
        //assert(signSingle == false)  // nothing else implemented
        if ((flags and TxCompletionFlags.SIGN) > 0)
        {
            val sighash = if ((flags and TxCompletionFlags.PARTIAL) > 0) // sign with sighash single and anyone can pay. NOTE: doesn't really do what we want -- we want a few outputs signed
                0xc3 // 0x80 or 0x43  // TODO actually sighash anyonecanpay would work, but payer would have to create 2 tx, the first makes an output with exact change for this tx
            else
                0x41

            signTransaction(tx, sighash)
        }
    }


    protected fun txConstructor(
      amountSatoshis: Long,
      deductFeeFromAmount: Boolean,
      minConfirms: Int = 1,
      outputFiller: ((BCHtransaction, Long) -> Boolean)
    ): Pair<BCHtransaction, ByteArray>
    {
        val AVG_INPUT_SIZE: Long = 147
        val AVG_OUTPUT_SIZE: Long = 34
        val TX_OVERHEAD_SIZE: Long = 16
        val DUST: Long =
          Dust(chainSelector)  // Below this many satoshis its not even worth making an output -- just give to miner

        if (amountSatoshis < DUST)
        {
            throw WalletDustException("Sending dust")
        }

        synchronized(oneTxConstructor)
        {
            var NinputsGuestimate = 1

            outerloop@ while (true)
            {
                // Find inputs
                val total =
                  if (deductFeeFromAmount) amountSatoshis else amountSatoshis + feeForSize(AVG_INPUT_SIZE * NinputsGuestimate + AVG_OUTPUT_SIZE * 2 + TX_OVERHEAD_SIZE)
                val inputs = findInputs(total, minConfirms)

                if (inputs != null)
                {
                    var newTx = BCHtransaction(chainSelector)
                    var inAmt: Long = 0

                    // Fill the TX with the chosen inputs
                    for (i in inputs)
                    {
                        LogIt.info(name + ": TX Input: " + i.outpoint.txid.toHex() + ":" + i.outpoint.idx + "   Amount: " + i.amount)
                        var bchinput = BCHinput(chainSelector)
                        bchinput.spendable = i  // a spendable carries with it the ability to sign a tx.  This is how the secret (if one is needed) is communicated to the tx under construction
                        bchinput.script = BCHscript(chainSelector)
                        bchinput.sequence = 0xffffffff
                        newTx.inputs.add(bchinput)
                        inAmt += i.amount
                    }

                    // How much do we want to pay for this tx?
                    var fee = feeForSize(newTx.inputs.size * AVG_INPUT_SIZE + AVG_OUTPUT_SIZE * 2 + TX_OVERHEAD_SIZE)

                    var signedTx: BCHtransaction?
                    var serializedTx: ByteArray?
                    while (true)  // Sign the TX and see if the fee is acceptable to the network for the real size of the tx.  If not try again with the new fee
                    {
                        newTx.outputs.clear()
                        val feeDeducted = outputFiller(newTx, fee)

                        // check the outputFiller to make sure it used what it claimed
                        var actualAmount = 0L
                        for (out in newTx.outputs)
                        {
                            if (out.amount < 0)
                            {
                                abortTransaction(newTx)
                                throw WalletFeeException(appI18n(RdeductedFeeLargerThanSendAmount))
                            }
                            actualAmount += out.amount
                        }
                        var oops = false
                        if (feeDeducted)
                        {
                            if (actualAmount + fee != amountSatoshis) oops = true
                        }
                        else if (actualAmount != amountSatoshis) oops = true
                        if (oops)
                        {
                            abortTransaction(newTx)
                            throw WalletImplementationException("Output filler filled the wrong number of satoshis")
                        }

                        if (inAmt - (actualAmount + fee) < 0)  // The quantity in all the inputs can't afford this tx.  We need to find another input
                        {
                            NinputsGuestimate = max(inputs.size,
                              (NinputsGuestimate + 1))  // By bumping the guess as to the number of inputs, we will increase the amount allocated for fees
                            abortTransaction(newTx)
                            continue@outerloop
                        }

                        // add change output only if needed
                        if (inAmt - (actualAmount + fee) > DUST)
                        {
                            val co = createChangeOutput(inAmt - amountSatoshis - fee)
                            newTx.outputs.add(co)
                        }

                        // TODO shuffle outputs
                        signedTx = signTransaction(newTx)

                        // SANITY CHECK for extremely large fee and abort if so.
                        var outputAmount: Long = 0
                        for (output in signedTx.outputs)
                        {
                            outputAmount += output.amount
                        }

                        // We don't need to check our input amounts against the blockchain for accuracy because if our values are incorrect then the signature hash that was generated
                        // as part of signedTx will be incorrect.
                        var inputAmount: Long = 0
                        for (input in signedTx.inputs)
                        {
                            inputAmount += input.spendable.amount
                        }
                        val finalFee = inputAmount - outputAmount

                        if (finalFee > MaxFee)
                        {
                            abortTransaction(newTx)
                            throw WalletFeeException(appI18n(RfeeExceedsFlatMax))
                        }

                        serializedTx = signedTx.BCHserialize(SerializationType.NETWORK).flatten()
                        val serializedTxSize = serializedTx.size.toLong()

                        val minFee = minFeeForSize(serializedTxSize)

                        if (fee < minFee)   // If the fee we chose is lower than the minimum fee for a tx of this size, try again with minfee as the fee.
                        {
                            fee = minFee
                            newTx.outputs
                        }
                        else break  // Ok worked!
                    }

                    if ((signedTx == null) || (serializedTx == null))  // Should never happen
                    {
                        abortTransaction(newTx)
                        throw WalletNotEnoughBalanceException(appI18n(RsendMoreThanBalance))
                    }

                    val outputSize = signedTx.outputs[0].BCHserialize(SerializationType.NETWORK).flatten().size
                    val inputSize = signedTx.inputs[0].BCHserialize(SerializationType.NETWORK).flatten().size
                    val inputScriptSize = signedTx.inputs[0].script.BCHserialize(SerializationType.NETWORK).flatten().size
                    LogIt.info(sourceLoc() + " " + name + ": TX info: size ${serializedTx.size} fee $fee rate ${fee.toDouble() / serializedTx.size.toDouble()}  inputSize $inputSize (script $inputScriptSize) outputSize $outputSize ")

                    return Pair(signedTx, serializedTx)
                }
                else
                {
                    throw WalletNotEnoughBalanceException(appI18n(RsendMoreThanBalance))
                }
            }
        }

    }

    fun signTransaction(tx: BCHtransaction, sigHashType: Int = 0x41): BCHtransaction
    {
        val txSerialized = tx.BCHserialize(SerializationType.NETWORK)
        var count: Long = 0
        val flatTx = txSerialized.flatten()

        //LogIt.info(flatTx.ToHex())
        for (inp in tx.inputs)
        {
            signInput(tx, count, sigHashType, serializedTx = flatTx)
            count += 1
        }
        tx.changed()  //  Clear out the hash (if it was requested) since it will have changed now that signatures are added.
        return tx
    }


    fun createChangeOutput(amtSatoshis: Long): BCHoutput
    {
        val d = runBlocking { newDestination() }
        var ret = BCHoutput(chainSelector)
        ret.amount = amtSatoshis
        ret.script = d.outputScript()
        return ret
    }

    // If the group holds native tokens, amtSatoshis is ignored
    fun createGroupedChangeOutput(groupId: GroupId, groupAmount: Long, amtSatoshis: Long = Dust(chainSelector)): BCHoutput
    {
        val d = runBlocking { newDestination() }
        val ret = BCHoutput(chainSelector)

        if (groupId.isFenced())
        {
            assert(false)  // not implemented
        }
        else
        {
            ret.amount = amtSatoshis
            ret.script = d.groupedOutputScript(groupId, groupAmount)
        }
        return ret
    }

    fun feeForSize(txSize: Long): Long
    {
        return ceil(txSize.toDouble() * DesiredFeeSatPerByte).toLong()  // Guess 1 satoshi per byte
    }

    fun minFeeForSize(txSize: Long): Long
    {
        return ceil(txSize * MinFeeSatPerByte).toLong()
    }

    //? Find inputs in this wallet to use in a new transaction.  This does NOT mark them as used, so cannot be called repeatedly until consumeInputs is called
    //? Pass a filter to select a subset of the available UTXOs.
    fun filterInputs(minConfirms: Int = 0, filter: ((BCHspendable) -> Boolean)? = null): MutableList<BCHspendable>
    {
        val curHeight: Long = chainstate?.syncedHeight ?: if (minConfirms == 0) 0L else throw WalletDisconnectedException()
        var ret: MutableList<BCHspendable> = mutableListOf()

        for (i in unspentTxos.values)
        {
            // If an unconfirmed or confirmed spend of this utxo exists, then don't use it.
            if (((minConfirms == 0) || !i.spentUnconfirmed) &&
              (i.spentHeight == -1L) && (i.amount > 0) &&
              // subtracting 1 from minConfirms because "0 confirms" means unconfirmed so the "1th" block is the tip, not the "0th"
              ((minConfirms == 0) || ((i.commitHeight != -1L) && (i.commitHeight + (minConfirms - 1) <= curHeight))) &&  // Or we need some number of confirmations
              (i.reserved == 0L) &&
              ((filter == null) || filter(i))
            )  // or this inputs has been used by some other in-progress transaction
                ret.add(i)
        }

        return ret
    }

    //? Find inputs in this wallet to use in a new transaction.  This does NOT mark them as used, so cannot be called repeatedly until consumeInputs is called
    //? Pass a filter to select a subset of the available UTXOs.
    fun findInputs(amountSatoshis: Long, minConfirms: Int = 0, filter: ((BCHspendable) -> Boolean)? = null): MutableList<BCHspendable>?
    {
        // LogIt.info("Looking for: " + amountSatoshis)
        var ret = mutableListOf<BCHspendable>()
        var currentTotal: Long = 0
        // grab all of our UTXOs as a list that's sorted by quantity of satoshis
        synchronized(dataLock)
        {
            var amounts: MutableList<BCHspendable> = filterInputs(minConfirms, filter)

            amounts.sortBy { it.amount }

            // Loop, adding one utxo per iteration, until we've found enough satoshis or run out of UTXOs
            while ((currentTotal < amountSatoshis) && (amounts.size > 0))
            {
                var loc = amounts.binarySearchBy(amountSatoshis - currentTotal) { it.amount }
                if (loc < 0)  // means we missed but -loc-1 is near (its the insertion position if the elem was added)
                {
                    loc = -loc - 1
                }

                if (loc >= amounts.size)  // We need more coins than exist in a UTXO
                {
                    loc = amounts.size - 1
                }

                if ((loc < 0) || (loc >= amounts.size)) return null  // loc is out of bounds because nothing left in the UTXO


                val utxo = amounts[loc]
                if (utxo.reserved == 0L)  // Final check because we released the lock
                {
                    // LogIt.info("found amount: " + utxo.amount)
                    ret.add(utxo)                // Add this utxo into our list of inputs
                    utxo.reserved = 1L
                    currentTotal += utxo.amount  // and add its quantity of satoshis into our running total
                }
                amounts.removeAt(loc)  // remove this from our list of possible utxos because we've used it
            }
        }

        // We have enough we are done
        if (currentTotal >= amountSatoshis) return ret

        // We used all the UTXOs and didn't get enough.  Undo the reservations.  This is an uncommon case since the wallet can determine whether there's enough balance before calling findInputs
        synchronized(dataLock)
        {
            for (utxo in ret)
            {
                utxo.reserved = 0L
            }
        }
        return null
    }

    override fun synced(epochTimeinMsOrBlockHeight: Long): Boolean
    {
        val TIME_FUDGE = 60 * 60 * 1000  // 1 hour in milliseconds
        val now = (epochTimeinMsOrBlockHeight == -1L)
        var tim = if (now) currentTimeMillis() else epochTimeinMsOrBlockHeight
        val bc = chainstate
        if (bc == null) return false  // can't be synced if we don't have a chainstate


        if (tim > 1262350000000) // Time stamp
        {
            if (bc.syncedHeight >= bc.chain.curHeight)  // Ok we are synced with our blockchain
            {
                val tip = bc.chain.nearTip ?: return false
                // Special case the "now" choice -- check if our blockchain up to date?
                if (now && (tip.time * 1000 < tim - TIME_FUDGE)) return false
                return true
            }
            else
            {
                if (now) return false; // we can't be synced if our blocks aren't even synced
                val hdr = bc.chain.getBlockHeader(bc.syncedHeight)
                if (hdr.time >= tim) return true  // Our synced header came in after the queried time
            }
        }
        else
        {
            // println("tim: " + tim + "  bc: " + bc.syncedHeight)
            if (tim <= bc.syncedHeight) return true
        }

        return false
    }

    /*
    override suspend fun synced(timeInMs: Long, height: Long): Boolean
    {
        var countdown = timeInMs
        var synced = false
        while (!synced && (countdown > 0))
        {
            val bc = chainstate
            if (bc != null)
            {
                if ((height != -1L) && (bc.syncedHeight >= height)) return true

                if ((height == -1L) && (bc.chain.curHeight == bc.syncedHeight))
                {
                    return true
                }
            }

            min(100, countdown).let {
                delay(it)
                countdown -= it
            }
        }
        LogIt.info(name + ": Wallet is at height: " + chainstate?.syncedHeight)
        return false
    }
    */

    //? undo 1 block's changes to this wallet
    fun rewind()
    {
        val cs = chainstate ?: throw WalletException("Cannot rewind with no blockchain")
        val syncedHash = cs.syncedHash

        LogIt.info(sourceLoc() + " " + name + ": Rewind away from ${cs.syncedHash.toHex()}:${cs.syncedHeight}")

        val header = cs.chain.blockHeader(syncedHash) ?: throw WalletException("Cannot rewind earlier than our header history")
        synchronized(dataLock) {
            val rewindData = cs.blockRewind[syncedHash]

            // before the prehistoryHeight by definition there is no wallet changes but rewindData will just return null
            if (rewindData != null)  // If I don't have data on this block, it did not change my wallet
            {
                for (spendable in rewindData.added)
                {
                    LogIt.info("Rewind removing: " + spendable.outpoint.txid.toHex() + ":" + spendable.outpoint.idx + " amt: " + spendable.amount)
                    if (unspentTxos.remove(spendable.outpoint) == null)
                    {
                        LogIt.severe("Missing utxo!")
                    }

                    spendable.addr?.let { addr ->
                        val v: MutableList<BCHoutpoint>? = unspentByAddress[addr]
                        if (v != null) v.remove(spendable.outpoint)
                    }

                }

                for (spendable in rewindData.spent)
                {
                    unspentTxos[spendable.outpoint] = spendable
                    insertUnspentByAddress(spendable.addr, spendable.outpoint)
                }
            }

            cs.syncedHeight -= 1
            if (cs.prehistoryHeight > cs.syncedHeight) cs.prehistoryHeight = cs.syncedHeight  // When we rock forward our new history will start where ever we had to rewind back to.
            if (cs.prehistoryDate > header.time) cs.prehistoryDate = header.time - PREHISTORY_SAFEFTY_FACTOR  // we set prehistory to the current block because we have it, but rewind from there.
            cs.syncedHash = header.hashPrevBlock
        }
        walletChangeCallback?.invoke(this)  // Update the GUI
    }

    fun generateDestinationsInto(newUnusedAddresses: MutableList<PayAddress>, minAmt: Int = GEN_ADDRESS_CHUNK_SIZE)
    {
        for (i in 0..minAmt)
        {
            // TODO switch to pay to script pubkey hash
            val dest = generateDestination()
            //LogIt.info("dest secret: " + dest.secret.ToHex() + "dest pubkey:" + dest.pubkey.ToHex())
            val destaddr = dest.address
            if (destaddr == null) throw WalletImplementationException("Generated destination needs to be representable as an address")
            synchronized(dataLock) {
                receiving[destaddr] = dest
                newUnusedAddresses.add(destaddr)  // I cannot add these addresses to the unused pile until they are installed into my bloom filters in connected nodes
            }
        }
    }

    var preparingDestinations: Boolean = false
    override fun prepareDestinations(minAmt: Int, chunk: Int)
    {
        synchronized(preparingDestinations)
        {
            // Only let one execution of this function happen at a time
            if (preparingDestinations) return
            try
            {
                preparingDestinations = true
                val newUnusedAddresses = mutableListOf<PayAddress>()
                if (unusedAddresses.size < minAmt)
                {
                    // Put all the new addrs into a temporary array so nobody uses them yet
                    synchronized(dataLock) {
                        generateDestinationsInto(newUnusedAddresses, chunk)
                    }
                    // Hand the new addrs to our connected nodes in the form of a bloom filter.
                    regenerateBloom() {
                        // OK bloom is installed so finally we can release these addresses.
                        for (a in newUnusedAddresses) unusedAddresses.add(a)
                    }
                }
            }
            finally
            {
                preparingDestinations = false
            }
        }
    }


    //* create a new address that funds can be sent to.  This function actually returns an existing pre-generated address, and generates new addresses in chunks of 'GEN_ADDRESS_CHUNK_SIZE when running out
    // This function makes sure that the destination is installed into bloom filters of connected nodes before it returns.  This adds complexity to this function, but eliminates a lot of problems where
    // a transaction is not seen because addresses are used before added to the bloom filter.
    override suspend fun newDestination(): PayDestination
    {
        var result: PayDestination?
        while (true)
        {
            synchronized(dataLock) {
                if (unusedAddresses.size > 0)
                {
                    val ret = unusedAddresses.removeAt(0)
                    result =
                      receiving[ret]  // The address must exist because all destinations are put into 'receiving at the time of creation
                    LogIt.info(sourceLoc() + " " + name + " Returning new address " + result?.address.toString())
                    if (result != null) return result!!
                }
                else prepareDestinations(1, GEN_ADDRESS_CHUNK_SIZE)
            }
            delay(20)  // Wait for destinations to be created
        }
    }


    // This function keeps this wallet synced up with the chainstate
    fun run()
    {
        try
        {
            var priorChainTip = Hash256()
            while (!done)
            {
                while (pause) wakey.delay(100)  // Implements blockchain processing pause feature at the API level.  Normally no pause here.
                // LogIt.info(name + ": wallet analysis at: " + (chainstate?.chain?.nearTip?.hash?.toHex() ?: "unavailable") + ":" + (chainstate?.chain?.nearTip?.height ?: "unavailable"))
                try
                {
                    val cs = chainstate
                    if (cs != null)
                    {
                        // Find the blockchain's most difficult tipFrac
                        cs.chain.nearTip?.let { tip: BlockHeader ->
                            if (tip.hash != priorChainTip)  // Update UI elements that show the wallet's blockchain since it has changed.
                            {
                                priorChainTip = tip.hash
                                walletChangeCallback?.invoke(this)
                            }

                            if (tip.hash != cs.syncedHash)
                            {
                                LogIt.info(name + ": Moving wallet to " + tip.hash.toHex() + ":" + tip.height + " from: " + cs.syncedHash.toHex() + ":" + cs.syncedHeight)
                                try  // Rewind to get onto the path to the most difficult tip
                                {
                                    // The if check makes this code work for regtest when the wallet starts not knowing the genesis block
                                    // And for when you "rediscover" the blockchain
                                    if (cs.syncedHeight != -1L) runBlocking {
                                        while (!cs.chain.isInMostWorkChain(cs.syncedHeight, cs.syncedHash))
                                        {
                                            try
                                            {
                                                rewind()
                                            }
                                            catch (e: WalletException)
                                            {
                                                LogIt.info("Wallet Exception:" + e.toString())
                                                delay(100)
                                                break
                                            }
                                        }
                                    }
                                }
                                catch (e: RequestedPrehistoryHeader)  // Rewound everything; move forward from here
                                {
                                }

                                val syncedH = cs.syncedHeight
                                if (cs.chain.curHeight > syncedH) // I need to sync with additional blocks
                                {
                                    // Load the headers from storage
                                    val hdrs = cs.chain.getHeaderChain(syncedH + 1, tip.hash, 500)
                                    while ((hdrs.size > 0) && (hdrs[0].height < syncedH + 1)) // too many returned, chop what I don't want
                                    {
                                        LogIt.info("Header at ${hdrs[0].height}")
                                        hdrs.removeAt(0)
                                    }

                                    if (hdrs.size > 0 && hdrs[0].height != syncedH + 1)
                                    {
                                        LogIt.severe(sourceLoc() + " " + name + ": Out of order headers. Asking starting at ${syncedH + 1}, loaded ${hdrs.size} headers starting at ${hdrs[0].height} (${hdrs[0].hash.toHex()})")
                                        //it.chain.reacquireHeaders(it.syncedHeight+1, hdrs[0].hash)
                                    }
                                    else
                                    {
                                        val earlyReq = mutableListOf<Guid>()
                                        for (hdr in hdrs)  // Skip early request for prehistory blocks
                                        {
                                            if (!((hdr.time <= cs.prehistoryDate) || (hdr.height <= cs.prehistoryHeight))) earlyReq.add(Guid(hdr.hash))
                                            else LogIt.info(sourceLoc() + " " + name + ": Skipping prehistory block time ${hdr.time} <= ${cs.prehistoryDate} or height ${hdr.height} <= ${cs.prehistoryHeight}")
                                        }
                                        launch { cs.chain.earlyRequestTxInBlock(earlyReq) }

                                        for (hdr in hdrs)
                                        {
                                            if ((hdr.time <= cs.prehistoryDate) || (hdr.height <= cs.prehistoryHeight))  // skip accessing any blocks that happened before this wallet was created
                                            {
                                                cs.prehistoryHeight = max(cs.prehistoryHeight, hdr.height)
                                                onSync(hdr.hash, hdr.height, hdr.time)
                                                walletChangeCallback?.invoke(this)
                                            }
                                            else
                                            {
                                                if (hdr.height != cs.syncedHeight + 1)  // ensure that hdrs is in order
                                                {
                                                    LogIt.severe(sourceLoc() + " " + name + ": Out of order headers received ${hdr.height} expecting ${cs.syncedHeight + 1}")
                                                    if (hdr.height > cs.syncedHeight + 1) break // and if they are beyond what we need then abort this work
                                                }
                                                else
                                                {
                                                    val txInBlock = runBlocking { cs.chain.getTxInBlock(Guid(hdr.hash)) }
                                                    interestingConfirmedTx(txInBlock, hdr.hash, hdr.height, hdr.time * 1000)
                                                    onSync(hdr.hash, hdr.height, hdr.time)
                                                    LogIt.info(sourceLoc() + " " + name + ": Synced Wallet at ${cs.syncedHeight} with: " + txInBlock.size + " interesting tx. hash: " + hdr.hash.toHex())

                                                    walletChangeCallback?.invoke(this)
                                                    // Write the chainstate data so we can pick up where we left off if the app stops and restarts
                                                    if (flushWalletPeriodically()) save()
                                                }
                                            }
                                        }

                                        // Write the chainstate data so we can pick up where we left off if the app stops and restarts
                                        if (hdrs.size > 0) save()
                                    }
                                }
                            }
                        }
                    }
                }
                catch (e: BlockNotForthcoming)  // Nodes may not provide blocks to us if we request ones that are not on the main chain.  In that case loop around so that we move the wallet to the main chain
                {
                    LogIt.info(sourceLoc() + " " + name + ": Block is not being supplied, ${e.toString()}")
                    //if (blockchain.net.numPeers() > 0)  // no blocks makes sense  if we have no peers
                    //    chainstate?.chain?.findAnotherTip()  // This fixes a weird, likely never to be seen in mainnet, case where there's a tie but one of the blocks was invalidated and yet we keep requesting that block rather than the active one.
                }
                catch (e: P2PNoNodesException)  // No connectivity; nothing to do but wait
                {
                    wakey.delay(LONG_DELAY_INTERVAL)
                }

                // Periodically resend all unconfirmed transactions to random nodes, most especially this happens once the wallet gets connected up
                val v = chainstate?.chain?.net
                if ((v != null) && (v.numPeers() > 0) && timeToResendHistoricalTx())
                {
                    resendUnconfirmedTx()
                }

                // Periodically resend pending transactions to random nodes
                // keep track of which nodes have INVed me with the tx, and don't send it to them
                if ((v != null) && (v.numPeers() > 0) && (pendingTx.size > 0))
                {
                    if (timeToResendWalletTx())
                    {
                        // Send all transactions to one node in case they are dependant
                        val pt = mutableListOf<ByteArray>()
                        for ((_, tx) in pendingTx)
                        {
                            pt.add(tx)
                        }
                        try
                        {
                            v.sendTransactions(pt)

                        }
                        catch (e: P2PDisconnectedException)  // Nothing to do but wait for another opportunity to send
                        {
                            timeToResendWalletTx.reset()
                        }
                        catch (e: P2PNoNodesException)
                        {
                            timeToResendWalletTx.reset()
                        }
                    }
                }

                wakey.delay(500)  // Periodic delay before rechecking
            }
        }
        catch (e: java.lang.Exception)
        {
            println("Wallet handler Exception: " + e.toString())
            handleThreadException(e, sourceLoc() + " " + name)
        }
        done = true  // If the wallet abort from its processing loop, set the done indicator
    }

    override fun getBalanceIn(dest: PayAddress): Long
    {
        synchronized(dataLock)
        {
            if (dest in unspentByAddress)
            {
                var ret: Long = 0
                unspentByAddress[dest]?.let {
                    for (outpt in it)
                    {
                        ret += unspentTxos[outpt]?.amount ?: 0
                    }
                }
                return ret
            }
        }
        return 0
    }

    override fun isUnspentWalletAddress(dest: PayAddress): Boolean
    {
        synchronized(dataLock)
        {
            val d = unspentByAddress[dest]
            return (d != null)
        }
    }

    override fun isWalletAddress(dest: PayAddress): Boolean
    {
        synchronized(dataLock)
        {
            val d = receiving[dest]
            return (d != null)
        }
    }

    fun regenerateBloom(onBloomInstalled: (() -> Unit)?)
    {
        // without a backing blockchain, there's no reason for a bloom, so this function is a no-op
        val cs = chainstate
        if (cs == null)
        {
            onBloomInstalled?.invoke()
            return
        }

        val data = synchronized(dataLock)
        {
            val data = Array<Any>(receiving.size + unspentTxos.size, { Unit })
            var idx = 0
            // Install all known addresses, so we find incoming money
            LogIt.info("Regenerate bloom with ${receiving.size} addresses")
            for (r in receiving)
            {
                data[idx] = r.key.data
                // need to create the log string here because it uses receiving
                //logStr = name + ": Installing bloom with " + receiving.size + " elements: " + receiving.map { it.key.toString() }.joinToString() + "bloom size: " + bloom.size
                LogIt.info(sourceLoc() + name + ": bloom addr " + idx + " is " + r.value.address)
                idx++
            }

            // Install all known outpoints so that we find spends
            for (u in unspentTxos)
            {
                data[idx] = u.key.BCHserialize(SerializationType.NETWORK).flatten()
                idx++
            }
            assert(idx == receiving.size + unspentTxos.size)
            data
        }

        cs.filterHandle = cs.chain.setFilterObjects(data, cs.filterHandle, onBloomInstalled)
    }

    fun addBlockchain(chain: Blockchain, checkpointHeight: Long, startPlace: Long?)
    {
        val cs = GlueWalletBlockchain(chain)
        val dbkey = chainStateDbKey(name, chain.chainSelector)
        var badChainState = false
        var noData = false
        try
        {
            loadChainState(cs, walletDb!!, dbkey)
        }
        catch (e: DataMissingException)
        {
            badChainState = true
            noData = true
        }
        catch (e: java.lang.IndexOutOfBoundsException)
        {
            LogIt.info(sourceLoc() + " " + name + ": DB deserialization error for key: " + dbkey)
            walletDb?.delete(dbkey)
            badChainState = true
        }
        catch (e: java.lang.IllegalArgumentException)  // illegal capacity
        {
            LogIt.warning(sourceLoc() + " " + name + ": database corruption " + dbkey + ". Deleting record")
            walletDb?.delete(dbkey)
            badChainState = true
        }
        catch (e: DeserializationException)
        {
            LogIt.info(sourceLoc() + " " + name + ": DB deserialization error for key: " + dbkey)
            walletDb?.delete(dbkey)
            badChainState = true
        }

        if (badChainState)
        {
            if (noData)  // If there no data on this wallet, assume that it was just created so we can set an initial time to now or what the creation recommended
            {
                if (startPlace == null)
                {
                    cs.prehistoryDate = (Date().time / 1000L) - PREHISTORY_SAFEFTY_FACTOR // Set prehistory to 2 hours ago to account for block timestamp variations
                }
                else
                {
                    cs.prehistoryDate = startPlace
                }
            }
            LogIt.info(sourceLoc() + " " + name + ": Bad or no persisted chainstate, assuming new wallet and setting prehistory to " + cs.prehistoryDate + "(" + epochToDate(cs.prehistoryDate) + ")")
            if ((checkpointHeight > 0) && (cs.prehistoryHeight <= 0.toLong()))  // If we couldn't set prehistoryHeight any other way, set it from the checkpoint
            {
                cs.prehistoryHeight = checkpointHeight
                cs.syncedHash = chain.checkpointId
                cs.syncedHeight = chain.checkpointHeight
            }
        }

        chainstate = cs
        loadWalletTx(walletDb!!)

        // Have a bunch of unused addresses ready and installed in the bloom filter
        if (unusedAddresses.size < 100) generateDestinationsInto(unusedAddresses, 100 - unusedAddresses.size)

        launch { regenerateBloom(null) }

        // Unconfirmed tx are communicated via callback.  Confirmed tx are handled by requesting a block and processing the result.
        chain.req.addUnconfirmedTxHandler({ txs -> interestingUnconfirmedTx(txs) })

        restart()
    }

    fun restart()
    {
        if ((processingThread == null) || (processingThread?.state == Thread.State.TERMINATED))
        {
            processingThread = thread(true, true, null, name + "_wallet") { run() }
        }
    }

    fun stop()
    {
        done = true
    }

    fun onSync(blockHash: Hash256, blockHeight: Long, @Suppress("UNUSED_PARAMETER") blockTime: Long)
    {
        // LogIt.info(sourceLoc() + " " + "onSync set synced height to:" + blockHeight + " time: " + blockTime)
        chainstate?.syncedHeight = blockHeight
        chainstate?.syncedHash = blockHash
        walletChangeCallback?.invoke(this)  // UPDATE the GUI
    }

    fun interestingConfirmedTx(txes: List<BCHtransaction>, blockHash: Hash256, blockHeight: Long, msSinceEpoch: Long)
    {
        // You can only give the wallet confirmed tx for the next block
        val cs = chainstate ?: return

        assert(blockHeight <= cs.syncedHeight + 1)
        //val hdr = cs.chain.blockHeader(blockHash)
        //assert(hdr!!.hashPrevBlock == cs.syncedHash)

        interestingTx(txes, blockHash, blockHeight, msSinceEpoch)
        for (tx in txes)
            pendingTx.remove(tx.hash)  // If one of our transactions was confirmed, then remove it from the pending list so we know not to keep broadcasting it
    }

    fun interestingUnconfirmedTx(txs: List<BCHtransaction>)
    {
        interestingTx(txs, null, null, null)
    }

    fun updateOutgoingPaymentHistory(outpoint: BCHoutpoint, amount: Long, addr: PayAddress, msSinceEpoch: Long?): PaymentHistory
    {
        val phist = paymentHistory[outpoint] ?: PaymentHistory()
        phist.isInflow = false
        phist.amount = amount
        phist.txIdx = outpoint.idx
        phist.txHash = outpoint.txid
        phist.address = addr

        // In general, take "now" as the transaction time since it is more accurate than the block time (we'll create this record for unconfirmed tx)
        // unless the current time is after the block time.  In that case, assume we are doing an IBD and so use the block time rather than the current time
        if ((msSinceEpoch != null) && (phist.date - msSinceEpoch > 10 * 60 * 1000)) // 10 min
        {
            phist.date = msSinceEpoch
        }
        else
        {
            phist.date = Instant.now().toEpochMilli()
        }

        // This outgoing payment might be change, so I don't want to touch the price or costbasis until I figure that out within the context of the whole tx
        return phist
    }

    /** Create or update a record of having received a payment */
    fun updateIncomingPaymentHistory(outpoint: BCHoutpoint, amount: Long, addr: PayAddress, msSinceEpoch: Long?)
    {
        var ph = paymentHistory[outpoint]
        if (ph == null)
        {
            ph = PaymentHistory()
            paymentHistory[outpoint] = ph
        }
        val phist = ph  // It must be nonnull because I just assigned it
        phist.isInflow = true // This is weird but an "output" of the transaction can be an inflow into this wallet if its paying to my address
        phist.amount = amount
        phist.txIdx = outpoint.idx
        phist.txHash = outpoint.txid
        phist.address = addr

        // In general, take "now" as the transaction time since it is more accurate than the block time (we'll create this record for unconfirmed tx)
        // unless the current time is well after the block time.  In that case, assume we are doing an IBD and so use the block time rather than the current time
        if ((msSinceEpoch != null) && (phist.date - msSinceEpoch > 10 * 60 * 1000)) // 10 min
        {
            phist.date = msSinceEpoch
        }
        else
        {
            phist.date = Instant.now().toEpochMilli()
        }
    }

    /** Get the price of this crypto's finest unit in fiat, using some external service */
    fun getPrice(whenInMsSinceEpoch: Long, currencyCode: String): BigDecimal
    {
        val now = Instant.now().toEpochMilli()

        try
        {
            if (now - whenInMsSinceEpoch < 24 * 60 * 60 * 1000) // 24 hours (historical price doesn't work for today)
            {
                val spfn = spotPrice
                if (spfn != null)
                {
                    return (spfn(currencyCode))
                }
            }
            else
            {
                val hfn = historicalPrice
                if (hfn != null)
                {
                    return hfn(currencyCode, whenInMsSinceEpoch / 1000)
                }
            }
        }
        catch (e: Exception)
        {
            LogIt.info(sourceLoc() + " " + name + " " + ": Cannot get price in " + currencyCode + " Error: " + e.toString())
        }

        // I can't get the price
        LogIt.info(sourceLoc() + " " + name + " " + ": Cannot get price in " + currencyCode)
        return BigDecimal.ZERO
    }

    fun interestingTx(txs: List<BCHtransaction>, blockHash: Hash256?, blockHeight: Long?, msSinceEpoch: Long?)
    {
        var changed = false
        var idx: Long
        synchronized(dataLock)
        {
            // Filter out any repeats
            val filteredTxes = mutableListOf<BCHtransaction>()

            for (tx in txs)
            {
                if (blockHash != null)
                {
                    // spams log until I get MERKLEBLOCK implemented
                    LogIt.info(name + ": Confirmed tx " + tx.hash.toHex() + " in block " + blockHash.toHex() + ":" + blockHeight)
                }
                else
                {
                    LogIt.info(name + ": Unconfirmed tx " + tx.hash.toHex())
                }

                val handledTx = txHistory[tx.hash]

                var process = if (handledTx == null) true  // We've never seen this transaction
                else if (handledTx.confirmedHeight > 0.toLong()) false  // We already have a confirmation of this tx
                else if (blockHash != null) true  // This is confirmed, and we only have unconfirmed
                else false // double unconfirmed

                // Discover if this tx really has something to do with this wallet, or is it a false positive or a different wallet's tx
                var relevantToMe = false
                if (process)
                {
                    // Is this transaction spending a UTXO tracked by this wallet?
                    for (i in tx.inputs)
                    {
                        val unsp = unspentTxos[i.spendable.outpoint]
                        if (unsp != null)
                        {
                            relevantToMe = true; break;
                        }
                        val phis = paymentHistory[i.spendable.outpoint]
                        if (phis != null)
                        {
                            relevantToMe = true; break;
                        }
                    }
                }
                if (process && !relevantToMe)
                {
                    for (i in tx.outputs)
                    {
                        val addr = i.script.address
                        if (addr == null)
                        {
                            LogIt.info(name + ": cannot understand tx: " + tx.hash.toHex() + " output: " + i.script.toHex())
                        }

                        val dest = receiving[addr]
                        if (dest != null)
                        {
                            relevantToMe = true;
                            break;
                        }
                    }
                }

                if (process && relevantToMe)
                {
                    // Add this tx into the list that needs wallet processing
                    filteredTxes.add(tx)

                    // Add this tx into the history
                    if (handledTx == null)
                    {
                        val entry = TransactionHistory(chainSelector, tx)
                        if (blockHeight != null) entry.confirmedHeight = blockHeight
                        if (blockHash != null) entry.confirmedHash = blockHash
                        // In general, take "now" as the transaction time since it is more accurate than the block time (we'll create this record for unconfirmed tx)
                        // unless the current time is well after the block time.  In that case, assume we are doing an IBD and so use the block time rather than the current time
                        if ((msSinceEpoch != null) && (entry.date - msSinceEpoch > 2 * 60 * 60 * 1000)) // 2 hours
                        {
                            entry.date = msSinceEpoch
                        }

                        //entry.priceWhenIssued =
                        //var priceInCurrency: String = ""
                        //var date: Date = Date()
                        txHistory[tx.hash] = entry
                    }
                    else
                    {
                        if (blockHeight != null) handledTx.confirmedHeight = blockHeight
                        if (blockHash != null) handledTx.confirmedHash = blockHash
                    }
                }

                if (!relevantToMe)
                {
                    if (handledTx != null)
                    {
                        LogIt.info("TX ${tx.hash} was in the history but is not relevant to this wallet")
                        txHistory.remove(tx.hash)
                    }
                }
            }

            // Now process all the tx in the list

            // Collect all the data we will need to figure out the money flow
            class HRecord(val inflow: MutableList<BCHoutpoint> = mutableListOf(), val outflow: MutableList<PaymentHistory> = mutableListOf(), val spent: MutableList<PaymentHistory> = mutableListOf())

            val hData: MutableMap<Hash256, HRecord> = mutableMapOf()

            // TX don't come in in dependency order, so need to do outs before inputs algorithm
            for (tx in filteredTxes)
            {
                var incomingAmt = 0L
                val walletIncomingIdxes = mutableListOf<Long>()
                idx = 0

                val hrecord = HRecord()
                hData[tx.hash] = hrecord

                //LogIt.info(name + ": looking for wallet output given ${receiving.size} addresses")
                for (utxo in tx.outputs)
                {
                    val addr = utxo.script.address
                    val payDest = receiving[addr]
                    val outpoint = BCHoutpoint(tx.hash, idx)


                    if (addr != null)  // If its a nonstandard payment give up any analysis
                    {
                        if (payDest != null)  // This utxo is interesting to us
                        {
                            if (true)
                            {
                                var created = false
                                // If it doesn't exist, initialize it, assume its an unconfirmed and we'll modify later if confirmed
                                val sp: BCHspendable = unspentTxos.getOrPut(outpoint)
                                {
                                    created = true
                                    val sp = BCHspendable(chainSelector)
                                    sp.outpoint = outpoint
                                    sp.spendableUnconfirmed = 1
                                    sp

                                }

                                assert(sp.outpoint == outpoint)
                                sp.amount = utxo.amount

                                if (blockHash != null)
                                { // This is a confirmed spend.  Overwrite any unconfirmed data with that of the confirmed tx, just in case malleated tx or doublespend
                                    if (sp.commitHeight > 0)
                                    {
                                        LogIt.info("RECONFIRM")
                                    }
                                    sp.spendableUnconfirmed = 0  // We got confirmed
                                    sp.commitBlockHash = Guid(blockHash)
                                    sp.commitHeight = blockHeight!!  // blockHeight must be something if blockHash is something

                                    // update the rewind data in case this blockchain is re-orged
                                    val rewindData = chainstate?.getRewindData(blockHash) ?: throw WalletException("cannot happen because these vars are always inited")
                                    rewindData.added.add(sp)
                                }
                                else
                                {
                                    if (created == false)
                                    {
                                        if (sp.commitHeight <= 0)
                                        {
                                            LogIt.info("RE-UNCONFIRMED")
                                        }
                                        else
                                        {
                                            LogIt.info("UNCONFIRMED notification of CONFIRMED TX")
                                        }
                                    }
                                }

                                sp.secret = payDest.secret  // TODO encrypt or reference an encrypted secret rather than holding it in plaintext
                                sp.redeemScript = BCHscript(chainSelector) // TODO
                                sp.priorOutScript = utxo.script  // Hang onto the output script since its part of the sighash when spending
                                sp.addr = addr
                                // Note we could check for a double spend by comparing this to an existing value
                                sp.commitTx = tx
                                sp.commitTxHash = Guid(tx.hash)

                                incomingAmt += sp.amount
                                walletIncomingIdxes.add(idx)

                                LogIt.info(name + ": Wallet tx received " + sp.amount + " in block " + (blockHash?.toHex()
                                  ?: "Unconfirmed") + " TX " + tx.hash.toHex() + " outpoint " + sp.outpoint.txid.toHex() + ":" + sp.outpoint.idx)

                                if (sp.spentBlockHash == Guid())
                                    insertUnspentByAddress(addr, sp.outpoint)
                                else
                                    LogIt.info("Output was already spent when received")

                                updateIncomingPaymentHistory(outpoint, sp.amount, addr, msSinceEpoch)
                                hrecord.inflow.add(outpoint)
                            }
                            changed = true
                        }
                        else  // If this UTXO is unknown to this wallet, then it might be an outgoing payment if we signed the inputs of the tx.
                        {     // But we can't figure that out until later, so create outgoing histories but keep them locally
                            val outpay = updateOutgoingPaymentHistory(outpoint, utxo.amount, addr, msSinceEpoch)
                            hrecord.outflow.add(outpay)
                        }
                    }
                    idx += 1
                }

                if (incomingAmt > 0)
                {
                    val history = txHistory[tx.hash]
                    history?.incomingAmt = incomingAmt
                    history?.incomingIdxes?.clear()
                    history?.incomingIdxes?.addAll(walletIncomingIdxes)
                }
            }

            for (tx in filteredTxes)
            {
                var walletOutgoingAmt = 0L
                val walletOutgoingIdxes = mutableListOf<Long>()
                // Go through all the inputs, confirming that we spent our UTXOs
                idx = 0

                val hrecord = hData[tx.hash]!!  // Must be non-null because we create it in the loop above
                val spentHistory = hrecord.spent

                for (spent in tx.inputs)
                {
                    synchronized(dataLock)
                    {
                        val outpoint = spent.spendable.outpoint

                        // This code relies on us getting the output tx before we get the spend.  This may not be the case for unconfirmed tx at least in theory
                        // The alternative, commented out above, stores a lot of spends by other people in the array with a negative amount.  When the spend analysis code sees a negative amount it knows we've already seen the spend
                        val v: BCHspendable? = unspentTxos.get(outpoint)
                        if (v != null) // One of our inputs was spent
                        {
                            LogIt.info(name + ": Wallet tx spent " + v.amount + " in block " + (blockHash?.toHex()
                              ?: " Unconfirmed") + " TX " + tx.hash.toHex() + " prevout " + spent.spendable.outpoint.txid.toHex() + ":" + spent.spendable.outpoint.idx)
                            changed = true
                            if (blockHash == null)  // This is an unconfirmed tx
                            {
                                // TODO check other stuff like that spentHeight == -1 (not yet spent)
                                v.spentUnconfirmed = true
                            }
                            else  // confirmation that our input was spent
                            {
                                v.spentUnconfirmed = false
                                val rewindData = chainstate?.getRewindData(blockHash) ?: throw WalletException("cannot happen because these vars are always inited")

                                rewindData.spent.add(v)
                                //  just mark it as spent: unspent.remove(spent.spendable.outpoint)
                                // and clean up much later
                                v.spentBlockHash = Guid(blockHash)
                                v.spentHeight = blockHeight ?: -1
                            }
                            walletOutgoingAmt += v.amount
                            walletOutgoingIdxes.add(idx)

                            // We should have prior payment history on every payment because the OTI (outs-then-ins) algorithm added all payments
                            paymentHistory[outpoint]?.let { spentHistory.add(it) }
                        }


                    }
                    idx += 1
                }

                if (walletOutgoingAmt > 0)
                {
                    val history = txHistory[tx.hash]
                    history?.outgoingAmt = walletOutgoingAmt
                    history?.outgoingIdxes?.clear()
                    history?.outgoingIdxes?.addAll(walletOutgoingIdxes)
                }
            }

            var giveUp = 0
            // Figure out what this transaction actually did and update the relevant history records
            // This code assumes that if this wallet spent any coins (i.e. signed any inputs) then self outputs are change
            while (hData.size > 0)
            {
                var progress = 0
                val txdataIter = hData.iterator()
                for (txdata in txdataIter)
                {
                    // Look for a transaction whose inputs are all resolved with cost basis information
                    var inputsResolved = true
                    var inputAmount = 0L
                    var totalPrice = BigDecimal(0, currencyMath).setScale(currencyScale)
                    var priceWhatFiat = "USD"
                    for (inp in txdata.value.spent)
                    {
                        val priorSpend = paymentHistory[inp.getOutpoint()]
                        if (priorSpend == null) inputsResolved = false
                        else
                        {
                            if (priorSpend.priceWhatFiat == "") inputsResolved = false  // I haven't figured out the cost basis yet
                            else
                            {
                                priceWhatFiat = priorSpend.priceWhatFiat  // TODO make sure same fiat
                                inputAmount += priorSpend.amount
                                val basisOverride = priorSpend.basisOverride

                                if (basisOverride != null)
                                {
                                    totalPrice += basisOverride
                                }
                                else
                                {
                                    totalPrice = totalPrice + (priorSpend.priceWhenIssued * BigDecimal(priorSpend.amount))
                                }
                            }
                        }
                        if (!inputsResolved) break
                    }

                    // We have enough information to resolve this whole tx
                    if (inputsResolved)
                    {
                        if (inputAmount > 0)  // Wallet paid someone
                        {
                            val avgPrice = totalPrice / BigDecimal(inputAmount)  // fiat/satoshi

                            for (outflow in txdata.value.outflow)  // The outputs that actually went to someone
                            {
                                outflow.basisOverride = avgPrice * BigDecimal(outflow.amount)
                                outflow.priceWhatFiat = priceWhatFiat
                                outflow.priceWhenIssued = getPrice(outflow.date, priceWhatFiat)
                                paymentHistory[outflow.getOutpoint()] = outflow  // Since this wallet signed some inputs in this tx, I'm going to assume that all the outputs are this wallet spending
                            }

                            for (inflow in txdata.value.inflow)  // The outputs that went to myself (change) should get the cost basis of the inputs
                            {
                                val inflowRecord = paymentHistory[inflow]!!
                                inflowRecord.priceWhenIssued = avgPrice
                                inflowRecord.priceWhatFiat = priceWhatFiat
                                inflowRecord.isChange = true
                                // TODO inherit the most recent date of all parents
                            }
                        }
                        else  // Wallet received money
                        {
                            for (inflow in txdata.value.inflow)  // The outputs that went to myself (change) should get the cost basis of the inputs
                            {
                                val inflowRecord = paymentHistory[inflow]!!
                                inflowRecord.priceWhenIssued = getPrice(inflowRecord.date, priceWhatFiat)
                                inflowRecord.priceWhatFiat = priceWhatFiat
                                inflowRecord.isChange = false
                            }
                        }

                        // remove this tx from txdata since its finished processing
                        txdataIter.remove()
                        progress += 1
                    }

                }
                if (progress == 0)
                {
                    //throw WalletException("No progress")
                    LogIt.warning("No progress")
                    giveUp += 1
                    if (giveUp == 5)
                    {
                        rediscover()
                        return
                    }
                }
            }
        }

        if (changed) walletChangeCallback?.let { it(this) }
    }

    /** print debugging data to the log */
    @kotlin.ExperimentalUnsignedTypes
    fun debugDump()
    {
        val s = StringBuilder()
        synchronized(dataLock) {
            s.append("Wallet " + name + " Balance: " + balance + " Unconfirmed: " + balanceUnconfirmed + "\n")
            s.append("  " + "Unspent:\n")
            for ((k, v) in unspentTxos)
            {
                s.append("    " + v.amount + " on " + v.addr + " outpoint " + k.txid.toHex() + ":" + k.idx + "\n")
            }

            s.append("  " + "Unconfirmed:\n")

            for ((k, v) in unspentTxos)
            {
                // Skip if the output was subsequently spent by an unconfirmed tx
                if ((v.spentHeight == -1L) && (v.spendableUnconfirmed > 0) && (v.spentUnconfirmed == false))
                {
                    val tx = v.commitTx ?: txHistory[k.txid]?.tx


                    s.append("    " + v.amount + " on " + v.addr + " outpoint " + k.txid.toHex() + ":" + k.idx)
                    if (tx != null)
                    {
                        val SperB = tx.fee.toBigDecimal(currencyMath).setScale(currencyScale) / tx.size.toBigDecimal(currencyMath).setScale(currencyScale)
                        val feeWarning = if (SperB < MinFeeSatPerByte.toBigDecimal()) " !!UNRELAYABLE FEE!! " else ""
                        s.append(" fee: " + tx.fee + " sat/byte: " + bchFormat.format(SperB) + feeWarning + "\n")
                        s.append("      TX: " + tx.hash + " hex: " + tx.toHex())
                    }
                    s.append("\n")
                }
            }
        }

        // Java log line is limited to 1000 chars
        val logLines = s.toString().split("\n")
        for (line in logLines)
        {
            for (chunkedLine in line.chunked(900))  // Log max length is 1000 bytes, this gives room for the log prefix
                LogIt.info(chunkedLine)
        }
    }
}


sealed class WalletStartup
object NEW_WALLET : WalletStartup()
//object LOAD_WALLET : WalletStartup()

/** Helper class that saves/loads data needed by the Bip44Wallet */
class Bip44WalletData() : BCHserializable()
{
    var id: String = ""

    var secretWords: String = ""

    //var secret: ByteArray = byteArrayOf()
    var maxAddress: Int = 0

    var chainSelector: ChainSelector = ChainSelector.BCHMAINNET

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        return BCHserialized(format) + id + secretWords + BCHserialized.uint32(maxAddress.toLong()) + BCHserialized.uint16(chainSelector.v.toLong())
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        id = stream.deString()
        secretWords = stream.deString()
        maxAddress = stream.deint32()
        try  // TODO remove after a bit
        {
            chainSelector = ChainSelectorFromValue(stream.deuint16().toByte())
        }
        catch (e: Exception)
        {
            // old save format so use default chainselector
        }
        return stream
    }
}

fun GetWalletData(name: String, db: KvpDatabase): Bip44WalletData
{
    LogIt.info("deserialize " + name)
    val wd: Bip44WalletData = Bip44WalletData()
    wd.BCHdeserialize(BCHserialized(db.get("bip44wallet_" + name), SerializationType.DISK))
    LogIt.info("done deserialize " + name)
    return wd
}


/** Return the Bip44 address number based on this blockchain */
fun Bip44AddressDerivationByChain(chainSelector: ChainSelector): Long
{
    return when (chainSelector)
    {
        ChainSelector.BCHMAINNET -> AddressDerivationKey.BCH
        ChainSelector.BCHTESTNET -> AddressDerivationKey.BCH
        ChainSelector.BCHREGTEST -> AddressDerivationKey.BCH
        ChainSelector.BCHNOLNET -> AddressDerivationKey.BCH
        ChainSelector.NEXTCHAIN -> AddressDerivationKey.BCH
    }
}

/** This wallet uses a single piece of random data, deriving new private keys and addresses using the technique described in [BIP-0032](https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki) with key derivation paths
 *  specified by [BIP-043](https://github.com/bitcoin/bips/blob/master/bip-0043.mediawiki) and [BIP-044](https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki).
 *  In essence, key derivation is:  m/<purpose=44>'/<coinType=0x91 for BCH>'/<account>'/<change=0 or 1>/index
 */
class Bip44Wallet(name: String, chainSelector: ChainSelector, var wdb: KvpDatabase) : CommonWallet(name, chainSelector)
{
    companion object
    {
        val COMMON_IDENTITY_SEED = ""

        /** The seed used to specify an identity used across all web sites where the user didn't choose a unique identity */
        val SECRET_SIZE = 16
        val SEED_SIZE = 64
        val MIN_RESAVE_TIME = 10000
    }

    /** Wallet secret seed */
    public var secretWords: String = ""

    /** User enters this to unlock the wallet */
    var passCode: String = ""

    /** Wallet seed derived from words + password */
    var secret: ByteArray = ByteArray(0) // initialize with a secret of the wrong size so it can't be accidentally used uninitialized // TODO keep this encrypted until needed

    /** No addresses have be calculated beyond this index */
    var maxAddress: Int = 0

    /** when was this wallet last saved */
    @OptIn(kotlin.time.ExperimentalTime::class)
    public var lastSave = TimeSource.Monotonic.markNow()

    var addressDerivationCoin: Long = Bip44AddressDerivationByChain(chainSelector)


    data class HdDerivationPath(val secret: ByteArray?, val purpose: Long, val coinType: Long, val account: Long, val change: Int, var index: Int)

    /** This variable captures HD derivation paths that should be monitored and spent but never offered as new payment destinations */
    val retrieveOnlyDerivationPaths = mutableListOf<HdDerivationPath>()

    public override fun toString(): String
    {
        val ret = StringBuilder(super.toString())
        ret.append("maxAddress=$maxAddress\n")
        return ret.toString()
    }

    init
    {
        walletDb = wdb
    }

    /** Load an existing wallet */
    constructor(wdb: KvpDatabase, wd: Bip44WalletData) : this(wd.id, wd.chainSelector, wdb)
    {
        secretWords = wd.secretWords
        maxAddress = wd.maxAddress
        secret = GenerateBip39Seed(secretWords, passCode)
    }

    /** Load an existing wallet */
    constructor(wdb: KvpDatabase, name: String) : this(wdb, GetWalletData(name, wdb))

    /** Create a new wallet with a random secret */
    constructor(wdb: KvpDatabase, name: String, chainSelector: ChainSelector, wop: WalletStartup) : this(name, chainSelector, wdb)
    {
        when (wop)
        {
            is NEW_WALLET ->
            {
                //SecRandom(secret)
                secretWords = GenerateBip39SecretWords(GenerateEntropy(SECRET_SIZE * 8))
                fillReceivingWithRetrieveOnly()
                saveBip44Wallet()
                secret = GenerateBip39Seed(secretWords, passCode)
            }
        }

        if (DEBUG)
        {
            // LogIt.info(sourceLoc() + " " + name + ": secret words: '" + secretWords + "' secret: " + secret.toHex())
        }
    }

    /** Create a new wallet given secret words */
    constructor(wdb: KvpDatabase, name: String, chainSelector: ChainSelector, secretWordList: String, maxAddr: Int = -1) : this(name, chainSelector, wdb)
    {
        secretWords = secretWordList
        secret = GenerateBip39Seed(secretWords, passCode)
        if (maxAddr == -1)  // TODO Search for addresses
        {
            maxAddress = 0
        }
        else
        {
            maxAddress = maxAddr
            // TODO add all prior addresses into this wallet and find balances
        }
        fillReceivingWithRetrieveOnly()
        saveBip44Wallet()
    }

    public fun saveBip44Wallet()
    {
        val wd = Bip44WalletData()
        wd.id = name
        wd.secretWords = secretWords
        wd.maxAddress = maxAddress
        wd.chainSelector = chainSelector
        wdb.set("bip44wallet_" + name, wd.BCHserialize(SerializationType.DISK).flatten())  // walletDb must be inited by the app before any wallets are created
    }

    override fun getRetrieveOnlyDestinations(): MutableList<PayDestination>
    {
        val ret = mutableListOf<PayDestination>()
        for (path in retrieveOnlyDerivationPaths)
        {
            for (idx in 0..path.index)
            {
                val secret = path.secret ?: secret  // If the secret isn't specified, then use this account's secret
                val newSecret = AddressDerivationKey.Hd44DeriveChildKey(secret, path.purpose, path.coinType, path.account, path.change, idx)
                val dest = Pay2PubKeyHashDestination(chainSelector, UnsecuredSecret(newSecret))
                LogIt.info(sourceLoc() + " " + name + ": retrieve only: " + dest.address.toString())
                ret.add(dest)
            }
        }
        return ret
    }

    @Synchronized
    @OptIn(kotlin.time.ExperimentalTime::class)
    override fun save(force: Boolean)
    {

        if (force || lastSave.elapsedNow().inWholeMilliseconds > MIN_RESAVE_TIME)
        {
            lastSave = TimeSource.Monotonic.markNow()
            saveBip44Wallet()
            super.save(force)
        }
    }

    /** Forget all transaction and blockchain state, and the redo the search for wallet transactions.
     * This is intended for testing and debug
     */
    override fun rediscover(forgetAddresses: Boolean): Unit
    {
        if (forgetAddresses) maxAddress = 0
        super.rediscover(forgetAddresses)
    }

    /** Forget all transaction and blockchain state, regenerate the first N addresses
     */
    public fun rediscoverAddresses(lastAddr: Long): Unit
    {
        LogIt.info(sourceLoc() + ": Generating ${lastAddr} addresses")
        for (i in 0..lastAddr)
        {
            val dest = generateDestination()
            val destaddr = dest.address
            if (destaddr == null) throw WalletImplementationException("Generated destination needs to be representable as an address")
            synchronized(dataLock) {
                receiving[destaddr] = dest
            }
        }

        maxAddress = lastAddr.toInt()
        LogIt.info(sourceLoc() + ": rediscovering tx in blockchain")
        rediscover(false)
    }

    override fun generateDestination(): PayDestination
    {
        val tmp = maxAddress
        maxAddress += 1
        val newSecret = AddressDerivationKey.Hd44DeriveChildKey(secret, AddressDerivationKey.BIP43, addressDerivationCoin, 0, 0, tmp)

        val dest = Pay2PubKeyHashDestination(chainSelector, UnsecuredSecret(newSecret))  // Note, if multiple destination types are allowed, the wallet load/save routines must be updated
        LogIt.info(sourceLoc() + " " + name + ": New Destination " + tmp.toString() + ": " + dest.address.toString())
        return dest
    }

    override fun destinationFor(seed: String): PayDestination
    {
        val index = 0
        /* TODO: only the common identity is supported right now
        val index = if (seed == COMMON_IDENTITY_SEED) 0  // Common identity
            else
            {
                val uniquifier = Key.Hd44DeriveChildKey(secret, 44, Key.ANY, 0, 0, 0xffffffff)
                val hash = Hash.hash256(seed.toByteArray() + uniquifier)
                hash[0].toPositiveLong()&~31 + hash[1].toPositiveLong()*256.toLong() + hash[2].toPositiveLong()*256.toLong()*256.toLong() + (hash[3].toPositiveLong() and 0x7f).toLong() * 256.toLong()*256.toLong()*256.toLong()
            }
        */

        return getDestinationAtIndex(index)
    }

    public fun getDestinationAtDerivationPath(addrIndex: Int): PayDestination
    {
        assert(addrIndex >= 0)
        val privateKey = AddressDerivationKey.Hd44DeriveChildKey(secret, AddressDerivationKey.BIP43, AddressDerivationKey.ANY, AddressDerivationKey.hardened(0), 0, addrIndex)

        return Pay2PubKeyHashDestination(chainSelector, UnsecuredSecret(privateKey))
    }

    public fun getDestinationAtIndex(addrIndex: Int): PayDestination
    {
        assert(addrIndex >= 0)
        val privateKey = AddressDerivationKey.Hd44DeriveChildKey(secret, AddressDerivationKey.BIP43, AddressDerivationKey.ANY, AddressDerivationKey.hardened(0), 0, addrIndex)

        return Pay2PubKeyHashDestination(chainSelector, UnsecuredSecret(privateKey))
    }
}

