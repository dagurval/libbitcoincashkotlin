// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash

import java.lang.IndexOutOfBoundsException

private val LogIt = GetLog("BU.transaction")

open class TransactionException(msg: String) : BUException(msg, appI18n(RbadTransaction), ErrorSeverity.Expected)


data class BCHoutpoint(val txid: Hash256, var idx: Long) : BCHserializable()
{
    constructor(stream: BCHserialized) : this(Hash256(stream), stream.deuint32())
    {
    }

    constructor(txhex: String, index: Long) : this(Hash256(txhex), index)

    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        return BCHserialized(format) + txid + BCHserialized.uint32(idx)
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized //!< Deserializer
    {
        txid.BCHdeserialize(stream)
        idx = stream.deuint32()
        return stream
    }

    fun getTransactionOrder(): Long
    {
        return idx
    }

    override fun toString(): String
    {
        return "BCHoutpoint(\"" + txid.toHex() + "\"," + idx + ")"
    }
}

//@Serializable
class BCHspendable(val chainSelector: ChainSelector) : BCHserializable()
{
    val SPENDABLE_ID = 0x58581.toInt()
    var secret: Secret? = null
    var outpoint = BCHoutpoint(Hash256(), 0)
    var priorOutScript = BCHscript(chainSelector)   // The script that constrains spending these coins
    var addr: PayAddress? = null  // We could get this from the priorOutScript, but for ease of use its replicated here.  This could be null, post initialization, if priorOutscript is anyone-can-spend.
    var amount: Long = -1
    var redeemScript = BCHscript(chainSelector) //ByteArray( 0)

    // TODO: secret and addr could/should be replaced by a PayDestination object in
    // BCHspendable.  This is transitional
    var backingPayDestination: PayDestination? = null  // RAM only
    val payDestination: PayDestination?
        get()
        {
            // P2SH multisig is an interesting problem here.  The P2SH address will depend on external data (the pubkeys of cooperating wallets)
            // so the wallet cannot determine if an address is spendable by this wallet without knowing cooperating wallets' pubkeys.

            // Allow override
            if (backingPayDestination != null) return backingPayDestination
            // Otherwise try to figure out the script type to construct the appropriate destination
            // We can short-cut this if we already know the address
            var a = addr
            if (a == null)
            {
                a = priorOutScript.address  // We can attempt to recognize the prior out script.  In this case we can extract an address from it.
            }

            if (a != null)  // Now use that address to determine the script type to create a payment destination
            {
                val sec = secret ?: return null // Can't build a payment if don't know the secret
                if (a.type == PayAddressType.P2PKH)
                {
                    backingPayDestination = Pay2PubKeyHashDestination(chainSelector, sec)
                    return backingPayDestination
                }
                // We can't construct the other types because we don't know info (like the redeem script)
                // We could guess the redeem script if there are P2SH redeem script types.  One common type that would be useful is to wrap every P2PKH into P2SH
                // to increase payment anonymity for those using p2sh (and make the UTXO set a tiny bit smaller per UTXO)
            }
            return null
        }

    var commitHeight: Long = -1
    var commitBlockHash: Guid = Guid()
    var commitTxHash: Guid = Guid()
    var commitTx: BCHtransaction? = null

    var spentHeight: Long = -1
    var spentBlockHash: Guid = Guid()
    var spentTxHash: Guid = Guid()
    var spentUnconfirmed: Boolean = false  // False if spend tx exists and is unconfirmed or spend tx does not exist.  True if an unconfirmed spend exists
    var spendableUnconfirmed: Long = 0 // > 0 if this UTXO is unconfirmed.  Number indicates the length of the longest spend chain

    var reserved: Long = 0 //*< Has an in-progress (being readied by this wallet) transaction used this?  0 means no, > 0 means yes.  RAM ONLY

    // TODO: Constructor assumes txid is encoded in big-endian, while normally it is encoded as little-endian.
    constructor(chainSelector: ChainSelector, tid: String, index: Long, qty: Long) : this(chainSelector)
    {
        assert(tid.length == 64)
        outpoint = BCHoutpoint(Hash256(tid.fromHex()), index)
        amount = qty
    }

    /**
     * @param txid Little-endian encoded transaction ID.
     */
    constructor(chainSelector: ChainSelector, txid: ByteArray, index: Long, amount: Long) : this(chainSelector)
    {
        // TODO: Workaround - It appears Hash256 assumes big-endian and reverses the bytes.
        val reversed = txid.copyOf()
        reversed.reverse()
        outpoint = BCHoutpoint(Hash256(reversed), index)
        this.amount = amount
    }

    constructor(chainSelector: ChainSelector, txid: Hash256, index: Int, amount: Long) : this(chainSelector)
    {
        outpoint = BCHoutpoint(txid, index.toLong())
        this.amount = amount
    }

    constructor(chainSelector: ChainSelector, txid: Hash256, index: Long, amount: Long) : this(chainSelector)
    {
        outpoint = BCHoutpoint(txid, index)
        this.amount = amount
    }

    // Serialization
    constructor(chainSelector: ChainSelector, data: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(data)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        if (format == SerializationType.NETWORK)
        {
            // coinbase txes have an idx of 0xffffffff
            assert(outpoint.idx >= 0)

            val ret = BCHserialized(format) + outpoint.txid + BCHserialized.uint32(outpoint.idx)
            return ret
        }
        else if (format == SerializationType.DISK)
        {
            val sec = secret?.getSecret() ?: byteArrayOf()
            val a = addr ?: PayAddress(chainSelector, PayAddressType.NONE, byteArrayOf())
            val ret = BCHserialized(format) + SPENDABLE_ID + variableSized(sec) + outpoint.BCHserialize(format) + priorOutScript + amount + redeemScript + commitHeight + commitBlockHash + commitTxHash +
              /* commitTx +  since the commitTx contains this BCHinput, that would recurse */
              spentHeight + spentBlockHash + spentTxHash + BCHserialized.boolean(spentUnconfirmed) + spendableUnconfirmed
            assert(ret.format == format)

            ret.add(a.BCHserialize(format))
            return ret
        }
        else if (format == SerializationType.HASH)
        {
            val ret = BCHserialized(format) + outpoint.txid + BCHserialized.uint32(outpoint.idx.toLong())
            return ret
        }
        else
        {
            throw NotImplementedError()
        }
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized //!< Deserializer
    {
        if (stream.format == SerializationType.NETWORK)
        {
            outpoint = BCHoutpoint(Hash256(stream), stream.deuint32())
        }
        else if (stream.format == SerializationType.DISK)
        {
            val id = stream.deint32()
            if (id != SPENDABLE_ID)
            {
                LogIt.info("deserialization corruption!")
                throw DeserializationException("Spendable identifier incorrect")
            }
            val secBytes = stream.deByteArray()
            if (secBytes?.size != 0) secret = UnsecuredSecret(secBytes)
            else secret = null
            outpoint.BCHdeserialize(stream)
            priorOutScript.BCHdeserialize(stream)
            amount = stream.deuint64()
            redeemScript.BCHdeserialize(stream)
            commitHeight = stream.deuint64()
            commitBlockHash.BCHdeserialize(stream)
            commitTxHash.BCHdeserialize(stream)
            // commitTx.BCHdeserialize(stream)
            commitTx = null
            spentHeight = stream.deuint64()
            spentBlockHash.BCHdeserialize(stream)
            spentTxHash.BCHdeserialize(stream)
            spentUnconfirmed = stream.deboolean()
            spendableUnconfirmed = stream.deuint64()
            val p = PayAddress(stream)
            addr = if (p.type != PayAddressType.NONE) p else null
        }
        return stream
    }

    override fun toString(): String
    {
        return amount.toString() + " from " + outpoint.toString()
    }

    @cli(Display.Simple, "Return group token information, or null if not grouped")
    fun groupInfo(): GroupInfo? = priorOutScript.groupInfo(amount)
}

/** Spend a coin (aka UTXO, prevout) in a transaction */
@cli(Display.Simple, "Input of a bitcoin transaction")
class BCHinput(val chainSelector: ChainSelector) : BCHserializable()
{
    @cli(Display.Simple, "What UTXO is being spent")
    var spendable = BCHspendable(chainSelector) //!< What prevout to spend

    @cli(Display.Simple, "Satisfier script")
    var script = BCHscript(chainSelector) //!< Satisfier script that proves you can spend this prevout

    @cli(Display.Dev, "enables locktime if not 0xffffffff")
    var sequence: Long = 0xffffffff //!< enable locktime if not 0xffffffff

    override fun toString(): String
    {
        return """{ "utxo" : ${spendable.toString()}, "satisfier" : "${script.toHex()}" }"""
    }

    /** Copy constructor */
    constructor(copy: BCHinput) : this(copy.chainSelector)
    {
        spendable = BCHspendable(copy.chainSelector, copy.BCHserialize(SerializationType.NETWORK))
        val ser = copy.script.BCHserialize(SerializationType.NETWORK)
        ser.flatten()
        script = BCHscript(copy.chainSelector, ser)
        sequence = copy.sequence
    }

    @cli(Display.Dev, "constructor")
    constructor(chainSelector: ChainSelector, spend: BCHspendable, scriptp: BCHscript, seq: Long = 0xffffffff) : this(chainSelector)
    {
        spendable = spend
        script = scriptp
        sequence = seq
    }

    @cli(Display.Dev, "constructor")
    constructor(spend: BCHspendable, scriptp: BCHscript, seq: Long = 0xffffffff) : this(scriptp.chainSelector)
    {
        spendable = spend
        script = scriptp
        sequence = seq
    }

    @cli(Display.Dev, "constructor, fill in the satisfier script later")
    constructor(spend: BCHspendable, seq: Long = 0xffffffff) : this(spend.chainSelector)
    {
        spendable = spend
        script = BCHscript(spend.chainSelector)
        sequence = seq
    }

    /** deserialization constructor */
    @cli(Display.Dev, "deserialization constructor")
    constructor(chainSelector: ChainSelector, buf: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(buf)
    }

    @cli(Display.Dev, "serialization")
    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        var ret = spendable.BCHserialize(format) + script + BCHserialized.uint32(sequence)
        return ret
    }

    @cli(Display.Dev, "deserialization")
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized  //!< Deserializer
    {
        spendable = BCHspendable(chainSelector, stream)
        script = BCHscript(chainSelector, stream)
        sequence = stream.deuint32()
        return stream
    }
}

//@Serializable
/** Output of a bitcoin transaction */
@cli(Display.Simple, "Output of a bitcoin transaction")
class BCHoutput(val chainSelector: ChainSelector) : BCHserializable()
{
    @cli(Display.Simple, "Spend quantity in satoshis")
    var amount: Long = 0 //!< Amount in satoshis assigned to this output

    @cli(Display.Simple, "Constraint script")
    var script = BCHscript(chainSelector)  //!< The "predicate" script that controls spendability

    /** explicit constructor */
    @cli(Display.Dev, "Constructor")
    constructor(chainSelector: ChainSelector, amt: Long, scr: BCHscript = BCHscript(chainSelector)) : this(chainSelector)
    {
        amount = amt
        script = scr
    }

    @cli(Display.Dev, "Constructor")
    constructor(amt: Long, scr: BCHscript) : this(scr.chainSelector)
    {
        amount = amt
        script = scr
    }

    /** Deserialization constructor */
    @cli(Display.Dev, "Deserialization constructor")
    constructor(chainSelector: ChainSelector, buf: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(buf)
    }

    /** Serializer */
    @cli(Display.Dev, "Serialize this output")
    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        var ret = BCHserialized(format) + amount + script
        return ret
    }

    @cli(Display.Dev, "Deserializer: Overwrite this object with the contents in the passed stream")
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized  //!< Deserializer
    {
        amount = stream.deint64()
        script = BCHscript(chainSelector, stream)
        return stream
    }

    @cli(Display.Dev, "Convert to a human readable form")
    override fun toString(): String
    {
        return amount.toString() + " to " + (script.address?.toString() ?: ("script " + script.toString()))
    }

    override fun hashCode(): Int
    {
        return amount.hashCode() xor script.hashCode()  // java 1.7 (android API 23-) does not contain Long.hashCode
    }

    override fun equals(other: Any?): Boolean
    {
        if (other !is BCHoutput) return false
        if (other.amount != amount) return false
        if (!other.script.contentEquals(this.script)) return false
        return true
    }

    @cli(Display.Simple, "Return group token information, or null if not grouped")
    fun groupInfo(): GroupInfo? = script.groupInfo(amount)
}

//@Serializable
/** Bitcoin transaction */
@cli(Display.Simple, "A blockchain transaction")
class BCHtransaction(val chainSelector: ChainSelector) : BCHserializable()
{
    protected var hashData: Hash256? = null  //!< cached hash of this transaction
    protected var sizeData: Int? = null  //!< cached size of this transaction

    @cli(Display.Simple, "inputs to the transaction")
    var inputs = mutableListOf<BCHinput>()  //!< inputs to this transaction

    @cli(Display.Simple, "transaction outputs")
    var outputs = mutableListOf<BCHoutput>() //!< outputs of this transaction

    @cli(Display.Simple, "version")
    var version: Int = 1  //!< transaction version

    @cli(Display.Simple, "transaction lock time")
    var lockTime: Long = 0  //!< transaction lock time

    companion object
    {
        @JvmStatic
        fun fromHex(chain: ChainSelector, hex: String, serializationType: SerializationType = SerializationType.UNKNOWN): BCHtransaction
        {
            val bytes = hex.fromHex()
            return BCHtransaction(chain, BCHserialized(bytes, serializationType))
        }
    }

    /** Return the outpoints in this transaction */
    @cli(Display.Dev, "transaction outpoints")
    val outpoints: Array<BCHoutpoint>
        get()
        {
            val txhash = hash
            val ret = mutableListOf<BCHoutpoint>()
            for (count in 0..outputs.size)
            {
                ret.add(BCHoutpoint(txhash, count.toLong()))
            }
            return ret.toTypedArray()
        }

    @cli(Display.Dev, "Satoshis being spent by this transaction")
    val inputSatoshis: Long
        get()
        {
            return inputs.fold(0L) { sum, inp -> sum + inp.spendable.amount }
        }

    @cli(Display.Dev, "transaction outpoints")
    fun spendable(index: Int): BCHspendable
    {
        if (index >= outputs.size || index < 0)
        {
            throw IndexOutOfBoundsException("illegal transaction output index")
        }
        val ret = BCHspendable(chainSelector, hash, index, outputs[index].amount)
        ret.priorOutScript = outputs[index].script
        ret.commitTxHash = Guid(this.hash)
        ret.commitTx = this
        return ret
    }

    @cli(Display.Dev, "return the output index corresponding to this constraint script")
    fun findOutput(s: BCHscript): Int
    {
        for ((idx, out) in outputs.withIndex())
        {
            if (out.script.contentEquals(s)) return idx
        }
        throw NoSuchElementException("output script not found")
    }

    @cli(Display.Dev, "return the output index corresponding to this constraint script")
    fun findOutput(addr: PayAddress): Int = findOutput(addr.constraintScript())

    /** Force recalculation of hash. To access the hash just use #hash */
    @Synchronized
    fun calcHash(): Hash256
    {
        val ser = BCHserialize(SerializationType.HASH)
        val serbytes = ser.flatten()
        // LogIt.info(serbytes.toHex())
        val hash = Hash256(bitcoinunlimited.libbitcoincash.Hash.hash256(serbytes))
        hashData = hash
        sizeData = serbytes.size
        return hash
    }

    /** If you change transaction data, call this function to force lazy recalculation of the hash */
    @Synchronized
    fun changed()
    {
        hashData = null
        sizeData = null
    }


    @cli(Display.Simple, "transaction size in bytes")
    val size: Int
        @Synchronized
        get()
        {
            while (true)  // in theory at least a thread could nullify sizeData between my calcHash() and loading into ret, so while loop
            {
                val ret = sizeData
                if (ret != null) return ret
                calcHash()
            }
        }

    @cli(Display.Simple, "transaction fee")
    val fee: Long
        get()
        {
            var fee: Long = 0
            for (i in inputs)
            {
                fee += i.spendable.amount
            }
            for (out in outputs)
            {
                fee -= out.amount
            }
            return fee
        }

    @cli(Display.Simple, "transaction fee rate in satoshi/byte")
    val feeRate: Double
        get()
        {
            return fee.toDouble() / size.toDouble()
        }

    /** Return this transaction's hash.  Uses a cached value if one exists.  If you change the
     * transaction be sure to call calcHash() or invalidateHash() to update this value either greedily or lazily respectively */
    @cli(Display.Simple, "transaction id")
    var hash: Hash256
        @Synchronized
        get()
        {
            val temp = hashData
            if (temp != null) return temp
            return calcHash()
        }
        set(value)
        {
            hashData = value
        }

    /** Returns true if this transaction is a coinbase tx */
    @cli(Display.Dev, "is this transaction a coinbase?")
    fun isCoinbase(): Boolean
    {
        // As per satoshi codebase
        if (inputs.size != 1) return false  // there must be exactly 1 input
        if ((inputs[0].spendable.outpoint.txid == Hash256() && inputs[0].spendable.outpoint.idx == 0xffffffff)) return true  // whose prevout must have a 0 hash and -1 idx
        return false
    }

    /** Serialize this transaction and return it as a hex encoded string */
    @cli(Display.Simple, "return the serialized hex representation")
    fun toHex(): String
    {
        var ser = BCHserialize(SerializationType.NETWORK)
        return ser.flatten().toHex()
    }

    /** Default display: print the transaction hash */
    override fun toString(): String = hash.toHex()

    fun debugDump()
    {
        LogIt.info("size: $size fee: $fee feerate: ${feeRate} inputs: ${inputs.size} outputs: ${outputs.size}")
        LogIt.info("INPUTS:")
        for (i in inputs)
        {
            LogIt.info("  ${i.spendable.amount} ${i.spendable.outpoint.txid.toHex()}:${i.spendable.outpoint.idx} confirmed at: ${i.spendable.commitHeight}")
        }
    }

    /** Deserialization constructor */
    constructor(chainSelector: ChainSelector, buf: ByteArray, format: SerializationType) : this(chainSelector, BCHserialized(buf, format))

    /** Deserialization constructor */
    constructor(chainSelector: ChainSelector, buf: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(buf)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized  //!< Serializer
    {
        // If HASH serialization differs from NETWORK, then the "size" calculation will be incorrect!
        var ret = BCHserialized(format) + version + inputs + outputs + BCHserialized.uint32(lockTime)
        return ret
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized  //!< Deserializer
    {
        version = stream.deint32()
        inputs = stream.delist({ b -> BCHinput(chainSelector, b) })
        outputs = stream.delist({ b -> BCHoutput(chainSelector, b) })
        lockTime = stream.deuint32()
        changed()
        return stream
    }

}
