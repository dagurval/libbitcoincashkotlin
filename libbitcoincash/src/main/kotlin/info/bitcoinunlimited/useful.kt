// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash

//import com.sun.org.apache.xpath.internal.operations.Bool
import java.io.File
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.*
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode
import java.net.URL
import java.net.URLDecoder
import java.security.SecureRandom
import java.text.DecimalFormat
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.concurrent.Executors
import kotlin.time.*
import kotlin.time.TimeSource

import java.util.logging.Logger
import kotlin.coroutines.CoroutineContext

private val LogIt = GetLog("BU.useful")


val DEBUG = false
val MY_CNXNS_ONLY = false // true
val REG_TEST_ONLY = false

val INCLUDE_NEXTCHAIN = true


val SATinMBCH = 1000L * 100L
val bchDecimals = 8   // !< The number of decimal places needed to express 1 satoshi in the "normal" currency units
val mBchDecimals = 5  //!< The number of decimal places needed to express 1 Satoshi (or equivalent) in the units used in the GUI
val currencyScale = 16  //!<  How many decimal places we need to do math without creating cumulative rounding errors
val fiatFormat = DecimalFormat("##,##0.00")  //!< How all fiat currencies are displayed (2 decimal places)
val mBchFormat = DecimalFormat("##,##0.#####")  //!< How the mBCH crypto is displayed (5 optional decimal places)
val bchFormat = DecimalFormat("#######0.########")  //!< How to display BCH crypto
val serializeFormat = DecimalFormat("###########0.########")  //!< How convert BigDecimals to strings in preparation for serialization/deserialization
val currencyMath = MathContext(currencyScale, RoundingMode.HALF_UP)  //!< tell the system details about how we want bigdecimal math handled

/** callback to notify app of any exception that happened within the coroutine launch.
 * Return False to raise the exception, causing program abort.
 * If null, exception is logged and ignored */
var launchExceptionHandler: ((e: java.lang.Exception) -> Boolean)? = null

fun min(a: Long, b: Long) = if (a < b) a else b
fun max(a: Long, b: Long) = if (a < b) b else a

/** Create a BigDecimal that is appropriate for currency mathematics (with lots of decimal places) */
fun CurrencyDecimal(a: Long) = BigDecimal(a, currencyMath).setScale(currencyScale)

/** Create a BigDecimal that is appropriate for currency mathematics (with lots of decimal places) */
fun CurrencyDecimal(a: String) = BigDecimal(a, currencyMath).setScale(currencyScale)

// Application should set this variable to a function that does localization & translation
// This translation needs to work with the Error values defined as integers
public var appI18n: ((Int) -> String) = { v -> "STR" + v.toString(16) }


class EJ(val json: JsonElement?)
{
    operator fun get(s: String): EJ
    {
        if (json == null) throw NoSuchElementException(s)
        val jo = json as JsonObject
        return EJ(jo[s])
    }

    operator fun get(i: Int): EJ
    {
        val jo = json as JsonArray
        return EJ(jo[i])
    }

    val double: Double
        get() = (json as JsonPrimitive).double

    val int: Int
        get() = (json as JsonPrimitive).int

    val long: Long
        get() = (json as JsonPrimitive).long

    val float: Float
        get() = (json as JsonPrimitive).float

    val boolean: Boolean
        get() = (json as JsonPrimitive).boolean

    val doubleOrNull: Double?
        get() = (json as JsonPrimitive).doubleOrNull

    val intOrNull: Int?
        get() = (json as JsonPrimitive).intOrNull

    val longOrNull: Long?
        get() = (json as JsonPrimitive).longOrNull

    val floatOrNull: Float?
        get() = (json as JsonPrimitive).floatOrNull

    val booleanOrNull: Boolean?
        get() = (json as JsonPrimitive).booleanOrNull


    val string: String
        get() = (json as JsonPrimitive).content

    val content: String
        get() = (json as JsonPrimitive).content

    val contentOrNull: String?
        get() = (json as JsonPrimitive).contentOrNull

    override fun toString(): String
    {
        val format = Json { prettyPrint = true }
        return format.encodeToString(json)
    }
}


fun sourceLoc(): String
{
    val fr = Exception().stackTrace[1]
    return "[" + fr.fileName + ":" + fr.lineNumber + "]"
}

fun handleThreadException(e: Exception, s: String = "")
{
    LogIt.warning(s + "Unexpected Exception: " + e.toString())
    var stkData = ByteArrayOutputStream()
    var stkStream = PrintStream(stkData)
    e.printStackTrace(stkStream)
    stkStream.flush()
    LogIt.warning(String(stkData.toByteArray()))
    launchExceptionHandler?.let { if (!it(e)) throw(e) }
    Thread.sleep(1000)
}

fun launch(scope: CoroutineScope? = null, fn: (suspend () -> Unit)?)
{
    var s = scope ?: GlobalScope
    s.launch {
        try
        {
            if (fn != null) fn()
        }
        catch (e: java.lang.Exception)
        {
            handleThreadException(e)
            launchExceptionHandler?.let { if (!it(e)) throw(e) }
        }
    }
}

fun range(start: Int, count: Int): IntRange
{
    if (count > 0)
        return IntRange(start, start + count - 1)
    else if (count < 0)
        return IntRange(start - count + 1, start)
    //else if (count == 0)
    return IntRange.EMPTY
}


fun epochToDate(epochSeconds: Long): String
{
    val fmt = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withZone(ZoneId.systemDefault())
    val epochSec = Instant.ofEpochSecond(epochSeconds)
    return fmt.format(epochSec)
}

/** Any object that responds to changes in a Reactive object */
abstract class Reactor<T>
{
    /** Implement your reaction by overriding this function.  Call obj->access to actually get the changed value.
     *
     */
    public abstract fun change(obj: Reactive<T>)

    /** Change the scope in which the reaction is called.  This allows you to make sure that the GUI thread is being used for GUI calls, or to isolate reaction calls to specific threads. */
    val executionScope: CoroutineScope? = null
}

/** The default context to execute reactions in */
val ReactionCtxt: CoroutineContext = Executors.newFixedThreadPool(4).asCoroutineDispatcher()
val ReactionScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(ReactionCtxt)


/** A class that encapsulates the idea that when an object changes, other entities should be notified of the change.
 * This is extremely useful in GUI development since onscreen object can be made to "automatically" update based on
 * changes to the "Reactive" underlying object that is being displayed.
 *
 * This class does not necessarily "react" once for every change to the Reactive object.  If multiple rapid changes
 * occur, intermediate values may be skipped.  This ensures that the reactions "keep up" with the changes.
 * */
class Reactive<T>(var value: T)
{
    private var oldValue: T? = null
    private var reactedToNum: Long = 0  // If reactedToNum == changeNum, we've already applied the change.  This allows the reaction to skip a bunch of values
    private var changeNum: Long = 0

    public var reactor: Reactor<T>? = null
        set(value)
        {
            field = value
            changeNum++
            react()
        }

    /** Change the value of this object */
    @Synchronized
    public operator fun invoke(newVal: T, force: Boolean = false)
    {
        if (force || (newVal != value))
        {
            oldValue = value
            value = newVal
            changeNum++

            reactor?.let {
                (it.executionScope ?: ReactionScope).launch { react() }
            }

        }

    }

    /** Change the value of this object, but only if it can accept a change */
    @Synchronized
    public operator fun invoke(force: Boolean = false, newValFn: () -> T)
    {
        if (force || (reactor != null))
        {
            val newVal = newValFn()
            if (force || (newVal != value))
            {
                oldValue = value
                value = newVal
                changeNum++

                reactor?.let {
                    (it.executionScope ?: ReactionScope).launch { react() }
                }
            }
        }

    }

    /** Efficiently return the values and mark that we've reacted to this change.
     * This call actually marks that the change has been reacted to, so call this as late in your processing as possible
     * to allow multiple changes in the underlying to be efficiently skipped. */
    @Synchronized
    fun access(): Pair<T, T?>?
    {
        if (changeNum == reactedToNum) return null  // Already handled so skip it
        reactedToNum = changeNum
        return Pair(value, oldValue)
    }

    /** Handle calling the reactor if something has changed.  This is normally called asynchronously */
    protected fun react()
    {
        if (reactedToNum != changeNum)
            reactor?.let { it.change(this) }  // Delay the access() of the data for as long as possible to maximize the number of changes that might occur before we access the variable (and minimize the latency between access an use)
    }

}

@OptIn(ExperimentalTime::class)
class Periodically(var periodInMs: Long)
{
    var last: TimeMark? = null
    operator fun invoke(): Boolean
    {
        val lastv = last
        if (lastv == null)
        {
            last = TimeSource.Monotonic.markNow()
            return true
        }
        else
        {
            if (lastv.elapsedNow().inWholeMilliseconds >= periodInMs)
            {
                last = TimeSource.Monotonic.markNow()
                return true
            }
        }
        return false
    }

    /** invoke again right away */
    fun reset()
    {
        last = null
    }
}

class ThreadCond() : java.lang.Object()
{
    @Synchronized
    fun wake()
    {
        notifyAll()
    }

    @Synchronized
    fun delay(mSec: Long, cb: (() -> Boolean)? = null)
    {
        if (cb == null || cb())
            wait(mSec)
    }
}

class CallAfter(var doneBits: Long = 0, var fn: (() -> Unit)? = null)
{
    var bitmap: Long = 0

    fun reset(undoneBits: Long)
    {
        bitmap = bitmap and undoneBits
    }

    fun setCallback(callwhen: Long, f: (() -> Unit)?)
    {
        doneBits = callwhen
        fn = f
        invoke(0)  // calls the fn if all the bits were already set
    }

    operator fun invoke(completed: Long)
    {
        bitmap = bitmap or completed
        var tmp = bitmap xor doneBits
        fn?.let {
            if (tmp == 0.toLong()) it()
        }
    }
}

/**  Call the passed function after this function has been called 'count times */
class CallAfterN(val fn: (() -> Unit)?, val count: Long)
{
    var cur: Long = 0

    constructor(fn: (() -> Unit)?, c: Int) : this(fn, c.toLong())

    operator fun invoke()
    {
        if (fn == null) return
        cur += 1
        if (cur == count) fn.invoke()
    }
}


/** Behaves like a condition, but for co-routines */
class CoCond<T>(val scope: CoroutineScope)
{
    val ch = Channel<T>()

    //* Wait for a maximum of 'timeout' ms, for this cond to be awakened.  If awakened, invoke 'until' if not null and
    //  return true if 'until' returns true.
    @OptIn(ExperimentalTime::class)
    suspend fun yield(until: ((T) -> Boolean)? = null, timeout: Int = Int.MAX_VALUE): T?
    {

        val start = TimeSource.Monotonic.markNow()
        while (start.elapsedNow().inWholeMilliseconds < timeout)
        {
            try
            {
                val r = withTimeout(timeout - start.elapsedNow().inWholeMilliseconds) {
                    ch.receive()
                }

                if (until == null)
                {
                    //LogIt.info ("yield was triggered")
                    return r
                }
                else if (until(r))
                {
                    //LogIt.info ("yield satisfied predicate")
                    return r
                }
            }
            catch (e: TimeoutCancellationException)
            {
            }
        }
        // LogIt.info ("yield timed out")
        return null
    }

    //? Wake up any coroutine that is yielding on this condition
    fun wake(obj: T)
    {
        scope.launch { ch.send(obj) }
    }
}

fun Dust(chain: ChainSelector): Long
{
    // All the supported chains currently have the same dust limit
    return 546L;
}

/** Create a new map, where the keys and values are swapped */
fun <K, V> Map<K, V>.invert(): Map<V, K>
{
    var ret = mutableMapOf<V, K>()
    for ((k, v) in this) ret[v] = k
    return ret
}

/*
fun List<String>.jsonify():String
{
    if (this.size == 0) return "[]"
    return this.joinToString("\",\"", "[\"", "\"]")
}
 */

@kotlin.ExperimentalUnsignedTypes
fun List<Any>.jsonify(): String
{
    if (this.size == 0) return "[]"
    val s = StringBuilder("[")
    var first = true
    for (item in this)
    {
        if (!first)
        {
            s.append(",")
        }
        else
        {
            first = false
        }

        var itemStr = when (item)
        {
            is String -> "\"" + item + "\""
            is Int -> item.toString()
            is Long -> item.toString()
            is UInt -> item.toString()
            is ULong -> item.toString()
            is Boolean -> if (item) "true" else "false"
            // is Jsonifyable -> item.jsonify() -- TODO add an interface to convert objects to json format
            else -> item.toString()
        }
        s.append(itemStr)
    }
    s.append("]")
    return s.toString()
}

/** template replacement using a string like "%a foo %b" % mapOf("a" to "first", "b" to "second") returns "first foo second"*/
operator fun String.rem(kv: Map<String, String>): String
{
    var result = StringBuilder()
    val split = this.split("%")
    var first = true
    for (s in split)
    {
        if (first)  // First split is not on a %
        {
            result.append(s)
            first = false
        }
        else
        {
            // Now find the end of the key
            val end = Regex("[^\\w.]+").find(s)
            val key = if (end == null) s else s.substring(0, end.range.start)
            val rest = if (end == null) "" else s.substring(end.range.start)
            // Look it up
            if (kv.containsKey(key))
            {
                result.append(kv[key])
            }
            else // Not in this dictionary so restore
            {
                result.append("%")
                result.append(key)
            }
            result.append(rest)
        }
    }

    return result.toString()
}

/**
 * Convert a uri scheme to a url, and then plug it into the java URL parser.
 * @return java.net.URL
 */
fun String.toUrl(): URL
{
    // replace the scheme with http so we can use URL to parse it
    val index = indexOf(':')
    // val scheme = take(index)
    val u = URL("http" + drop(index))
    return u
}

private val HEX_CHARS = "0123456789abcdef"

// Convert a hex string to a ByteArray
fun String.fromHex(): ByteArray
{

    val result = ByteArray(length / 2)

    for (i in 0 until length step 2)
    {
        val firstIndex = HEX_CHARS.indexOf(this[i]);
        val secondIndex = HEX_CHARS.indexOf(this[i + 1]);

        val octet = firstIndex.shl(4).or(secondIndex)
        result.set(i.shr(1), octet.toByte())
    }

    return result
}

@Deprecated("member function use lowercase", replaceWith = ReplaceWith("fromHex"))
fun String.FromHex(): ByteArray = fromHex()


fun Byte.toUint() = toInt() and 0xFF

fun ByteArray.copyInto(to: ByteArray, location: Int = 0): ByteArray
{
    var pos = location;
    for (i in this)
    {
        to[pos] = i
        pos += 1
    }
    return to
}

// Convert a ByteArray to a hex string
val hexRep = "0123456789abcdef"
fun ToHexStr(ba: ByteArray): String
{
    val ret = StringBuilder()
    for (b in ba)
    {
        ret.append(hexRep[b.toUint() shr 4])
        ret.append(hexRep[b.toUint() and 0xf])
    }
    return ret.toString()
}

@Deprecated("member function use lowercase", replaceWith = ReplaceWith("toHex"))
fun ByteArray.ToHex(): String = ToHexStr(this)
fun ByteArray.toHex(): String = ToHexStr(this)


// Join a list of ByteArray into a single ByteArray
fun MutableList<ByteArray>.join(): ByteArray
{
    var len = 0
    for (ba in this)
    {
        len += ba.size
    }
    var ret = ByteArray(len)
    len = 0
    for (ba in this)
    {
        for (b in ba)
        {
            ret[len] = b
            len++
        }
    }
    return ret
}


/** Receive from a channel with timeout */
suspend fun <T> Channel<T>.receive(timeout: Long): T?
{
    var ret: T? = null
    try
    {
        withTimeout(timeout.toLong()) {
            ret = receive()
        }
    }
    catch (e: TimeoutCancellationException)
    {
    }
    return ret
}


// see https://stackoverflow.com/questions/13592236/parse-a-uri-string-into-name-value-collection
fun URL.queryMap(): Map<String, String>
{
    val query_pairs = LinkedHashMap<String, String>()
    val query = this.getQuery()
    if (query == null) return mapOf()
    val pairs = query.split("&")
    for (pair in pairs)
    {
        val idx = pair.indexOf("=")
        query_pairs[URLDecoder.decode(pair.substring(0, idx), "UTF-8")] = URLDecoder.decode(pair.substring(idx + 1), "UTF-8")
    }
    return query_pairs
}


// Provide secure random numbers to the C++ layer
open class Initialize
{
    companion object
    {
        @JvmStatic
        external fun LibBitcoinCash(chainSelector: Byte): String

        // Android native does not have access to a good secure random number generator so it will ask
        // the java layer for data
        @JvmStatic
        fun SecRandom(data: ByteArray)
        {
            val random = SecureRandom()
            //data = ByteArray(length,{ _ -> 0})
            random.nextBytes(data)
            //return data
        }
    }
}

/** split a (IP or FQDN):port formatted string (e.g 192.168.100.10:54 or www.example.com:54) into a string IP address (or FQDN) and an integer port
 * @param s The IP address or FQDN
 * @param defaultPort The port number to use if a port (i.e. a colon) is not part of the string
 * @throws Exception if the input string is bad
 */
fun SplitIpPort(s: String, defaultPort: Int): Pair<String, Int>
{
    val ipAndPort = s.split(":")
    if (ipAndPort.size == 2)
    {
        return Pair(ipAndPort[0], ipAndPort[1].toInt())
    }
    return Pair(s, defaultPort)
}

