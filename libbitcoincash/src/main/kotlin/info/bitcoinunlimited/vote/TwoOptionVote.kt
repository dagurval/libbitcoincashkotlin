package bitcoinunlimited.libbitcoincash.vote

import bitcoinunlimited.libbitcoincash.*

/**
 * TwoOptionVote is a on-chain transparent election protocol.
 * Specification: https://nerdekollektivet.gitlab.io/votepeer-documentation/two-option-vote-contract.html
 *
 * Helper functions for working with TwoOptionVote.
 */
class TwoOptionVote {
    companion object {

        fun calculate_proposal_id(
            salt: ByteArray,
            description: String,
            optA: String,
            optB: String,
            endheight: Int,
            participants: Array<ByteArray>
        ): ByteArray {
            var participantsCombined = ByteArray(0)

            for (p in UtilVote.sortByteArray(participants)) {
                participantsCombined = participantsCombined + p
            }
            var proposalIDParts = arrayOf(
                salt,
                description.toByteArray(),
                optA.toByteArray(),
                optB.toByteArray(),
                UtilVote.longToUInt32ByteArray(endheight.toLong()),
                participantsCombined
            )

            var proposalID = ByteArray(0)
            for (p in proposalIDParts) {
                proposalID = proposalID + p
            }

            return Hash.hash160(proposalID)
        }

        fun deriveContractAddress(
            blockchain: ChainSelector,
            proposalID: ByteArray,
            optA: ByteArray,
            optB: ByteArray,
            voterPKH: ByteArray
        ): PayAddress {

            var hash = Hash.hash160(redeemScript(proposalID, optA, optB, voterPKH))
            return PayAddress(blockchain, PayAddressType.P2SH, hash)
        }

        /**
         * Get the full redeemscript.
         */
        fun redeemScript(
            proposalID: ByteArray,
            optA: ByteArray,
            optB: ByteArray,
            voterPKH: ByteArray
        ): ByteArray {
            val scriptHex = (
                "5479a988547a5479ad557a5579557abb537901147f75537a887b01147f77" +
                    "767b8778537a879b7c14beefffffffffffffffffffffffffffffffff" +
                    "ffff879b"
                )
            val script = UtilStringEncoding.hexToByteArray(scriptHex)

            return (
                OP.push(proposalID) +
                    OP.push(optB) +
                    OP.push(optA) +
                    OP.push(voterPKH) +
                    script
                )
        }

        fun createVoteMessage(
            proposalID: ByteArray,
            option: ByteArray
        ): ByteArray {
            if (proposalID.size != 20) {
                error("Invalid proposalID length")
            }
            if (option.size != 20) {
                error("Invalid option length")
            }
            return proposalID + option
        }

        fun signVoteMessage(
            secret: ByteArray,
            message: ByteArray
        ): ByteArray {
            if (message.size != 40) {
                error("Invalid message length")
            }
            return Key.signDataUsingSchnorr(message, secret)
        }

        /**
         * If size is a valid signature size.
         */
        fun isPlausibleSignatureSize(signatureSize: Int): Boolean {
            return signatureSize in 64..73
        }

        /**
         * Parses the vote option from a transaction.
         */
        fun parseVote(tx: BCHtransaction): ByteArray {
            if (tx.inputs.size > 1) {
                throw IndexOutOfBoundsException("Multiple inputs NYI")
            } else if (tx.inputs.size == 0) {
                throw IndexOutOfBoundsException("tx.inputs.size == 0, should be one or more")
            }
            val script = tx.inputs[0].script.flatten()
            var pos = 0
            if (script.isEmpty())
                throw IllegalStateException("tx.inputs[0].script is an empty array")

            val msgSigSize = script[pos++]
            if (!isPlausibleSignatureSize(msgSigSize.toInt())) {
                // TODO: Write test for this error?
                throw IllegalStateException("Unexpected message siganture size ${msgSigSize.toInt()}")
            }
            pos += msgSigSize
            if (script[pos++] != 40.toByte()) {
                // TODO: Write test for this error?
                throw IllegalStateException("Expected push 40")
            }
            // Next 40 bytes is the message, which is (election id + vote). Copy out the vote.
            // TODO: Write test for this return
            return script.copyOfRange(pos + 20, pos + 40)
        }
    }
}