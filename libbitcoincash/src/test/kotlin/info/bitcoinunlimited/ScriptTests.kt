package info.bitcoinunlimited.libbitcoincash

import bitcoinunlimited.libbitcoincash.BCHscript
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.OP
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class ScriptTests {
    /**
     * Push 520 bytes to the stack
     */
    @Test
    fun testPush520() {
        val payload = ByteArray(520) { _ -> 0xAA.toByte() }
        val result = OP.push(payload)

        val expect = OP.PUSHDATA2 + 0x08.toByte() + 0x02.toByte() + payload
        assertArrayEquals(expect, result)
    }

    @Test
    fun testGetDirectPush() {

        assertArrayEquals(byteArrayOf((-1).toByte()), OP.getDirectPush(OP.OP1NEGATE[0])!!)
        assertArrayEquals(byteArrayOf(), OP.getDirectPush(OP.OP0[0])!!)
        assertArrayEquals(byteArrayOf(16.toByte()), OP.getDirectPush(OP.OP16[0])!!)

        // outside direct push range
        assertEquals(null, OP.getDirectPush((OP.OP1NEGATE[0].toInt() - 1).toByte()))
        assertEquals(null, OP.getDirectPush((OP.OP16[0].toInt() + 1).toByte()))
    }

    @Test
    fun testCopyFront() {

        // Test cases

        // special case with empty result
        val directPushData0 = OP.OP0
        val directPushData16 = OP.OP16

        val push0Data = ByteArray(42) { _ -> 0xAA.toByte() }
        val push1Data = ByteArray(OP.LAST_DATAPUSH + 1) { _ -> 0xAA.toByte() }
        val push2Data = ByteArray(0xff + 1) { _ -> 0xAA.toByte() }
        // check that byte 2 is decoded properly (LE vs BE)
        val push2Data2 = ByteArray(0xffee) { _ -> 0xAA.toByte() }

        fun pushScript(data: ByteArray): BCHscript {
            return BCHscript(
                ChainSelector.BCHMAINNET,
                OP.push(data)
            )
        }

        // tests

        val (directResult0, directRange0) = BCHscript(
            ChainSelector.BCHMAINNET,
            directPushData0
        ).copyFront()
        assertTrue(directResult0.isEmpty())
        assertEquals(0, directRange0.first)
        assertEquals(0, directRange0.last)

        val (directResult16, directRange16) = BCHscript(
            ChainSelector.BCHMAINNET,
            directPushData16
        ).copyFront()
        assertArrayEquals(byteArrayOf(16.toByte()), directResult16)
        assertEquals(0, directRange16.first)
        assertEquals(1, directRange16.last)

        val (push0Result, push0Range) = pushScript(push0Data).copyFront()
        assertArrayEquals(push0Data, push0Result)
        assertEquals(1, push0Range.start)
        assertEquals(1 + push0Data.size - 1, push0Range.last)

        val (push1Result, push1Range) = pushScript(push1Data).copyFront()
        assertArrayEquals(push1Data, push1Result)
        assertEquals(2, push1Range.start)
        assertEquals(2 + push1Data.size - 1, push1Range.last)

        val (push2Result, push2Range) = pushScript(push2Data).copyFront()
        assertArrayEquals(push2Data, push2Result)
        assertEquals(3, push2Range.start)
        assertEquals(3 + push2Data.size - 1, push2Range.last)

        assertArrayEquals(push2Data2, pushScript(push2Data2).copyFront().first)

        // should ignore additional pushes
        val multiPush = BCHscript(
            ChainSelector.BCHMAINNET,
            OP.push(push2Data),
            OP.push(push0Data)
        )
        assertArrayEquals(
            push2Data, multiPush.copyFront().first
        )

        // special case: empty element
        var (emptyResult, emptyRange) = pushScript(ByteArray(0)).copyFront()
        assertTrue(emptyResult.isEmpty())
        assertEquals(0, emptyRange.first)
        assertEquals(0, emptyRange.last)
    }

    @Test
    fun testCopyStackElementAt() {
        val push1Data = ByteArray(42) { _ -> 0xAA.toByte() }
        val push2Data = ByteArray(0xff + 1) { _ -> 0xAA.toByte() }

        val multiPush = BCHscript(
            ChainSelector.BCHMAINNET,
            OP.push(push1Data),
            OP.push(push2Data)
        )
        val (_, push1Range) = multiPush.copyFront()
        val (push2Result, push2Range) = multiPush.copyStackElementAt(push1Range.last + 1)
        assertArrayEquals(push2Data, push2Result)
        assertEquals(46, push2Range.first)
        assertEquals(multiPush.flatten().size - 1, push2Range.endInclusive)
    }
}