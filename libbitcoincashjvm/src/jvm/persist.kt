// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash
import kotlinx.coroutines.*

import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.*

import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.statements.api.ExposedBlob
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import org.sqlite.SQLiteException
import java.math.BigInteger
import java.sql.Blob
import java.sql.Connection
import java.sql.ResultSet
import java.util.logging.Logger
import javax.sql.rowset.serial.SerialBlob


open class DataMissingException(): BUException("", appI18n(RdataMissing))

private val LogIt = GetLog("BU.persist")

val SHA256_HASH_LEN = 32

val MAX_SQL_PRECISION = 38

val CHAINTIP_MARKER:Hash256 = { var v = Hash256(); v.set(0,1); v }()

interface BlockHeaderDao
{
    //@Query("SELECT * FROM blockHeader")
    fun getAll(): List<BlockHeader>

    //@Query("SELECT * FROM blockHeader WHERE height IN (:heights)")
    fun loadAllByHeight(heights: IntArray): List<BlockHeader>

    //@Query("SELECT * FROM blockHeader WHERE height = :height")
    fun get(height: Long): BlockHeader?

    /** Returns all block headers at this height (in case there's a blockchain fork */
    fun getAtHeight(height: Long): List<BlockHeader>

    //@Query("SELECT * FROM blockHeader WHERE id = :blockid")
    abstract fun get(blockid: ByteArray): BlockHeader?

    //@Query("SELECT * FROM blockHeader x INNER JOIN (SELECT height, MAX(height) FROM blockHeader GROUP BY height) y ON x.height = y.height")
    //@Query("SELECT a.* FROM blockHeader a LEFT OUTER JOIN blockHeader b ON a.height < b.height WHERE b.height IS NULL")
    fun getLast(): BlockHeader?

    //@Query("SELECT a.* FROM blockHeader a LEFT OUTER JOIN blockHeader b ON a.cumulativeWork < b.cumulativeWork WHERE b.cumulativeWork IS NULL")
    fun getMostWork(): Array<BlockHeader>

    //@Insert
    fun insert(vararg bh: BlockHeader)

    //@Delete
    fun delete(bh: BlockHeader)

    //@Query("DELETE FROM blockHeader WHERE height = :height")
    fun delete(height: Long)

    //@Query("DELETE FROM blockHeader")
    fun deleteAll()

    fun upsert(vararg bh: BlockHeader)

}

// See https://github.com/JetBrains/Exposed/issues/167
fun <T : Table> T.insertOrUpdate(vararg onDuplicateUpdateKeys: Column<*>, body: T.(InsertStatement<Number>) -> Unit) =
    InsertOrUpdate<Number>(onDuplicateUpdateKeys,this).apply {
        body(this)
        execute(TransactionManager.current())
    }

class InsertOrUpdate<Key : Any>(
    private val onDuplicateUpdateKeys: Array< out Column<*>>,
    table: Table,
    isIgnore: Boolean = false
) : InsertStatement<Key>(table, isIgnore)
{
    override fun prepareSQL(transaction: Transaction): String
    {
        val onUpdateSQL = if(onDuplicateUpdateKeys.isNotEmpty())
        {
            //" ON DUPLICATE KEY UPDATE " + onDuplicateUpdateKeys.joinToString { "${transaction.identity(it)}=VALUES(${transaction.identity(it)})" }
            // SQLITE (should work post 3.24.0 but doesn't)
            val id = onDuplicateUpdateKeys.joinToString { "${transaction.identity(it)}" }
            " ON CONFLICT(${id}) DO UPDATE SET ${id}=VALUES(${id})"
        }
        else ""

        return super.prepareSQL(transaction) + onUpdateSQL
    }
}


fun load(input: ResultRow): BlockHeader
{
    var ret = BlockHeader()
    ret.hashData = Hash256(input[PBlockHeaders.hash])
    ret.version = input[PBlockHeaders.version]
    ret.hashPrevBlock = Hash256(input[PBlockHeaders.hashPrevBlock])
    ret.hashMerkleRoot = Hash256(input[PBlockHeaders.hashMerkleRoot])
    ret.time = input[PBlockHeaders.time]
    ret.diffBits = input[PBlockHeaders.diffBits]
    ret.nonce = input[PBlockHeaders.nonce]
    ret.numTx = input[PBlockHeaders.numTx]
    ret.blockSize = input[PBlockHeaders.blockSize]
    ret.cumulativeWork = input[PBlockHeaders.cumulativeWork].toBigInteger()
    ret.height = input[PBlockHeaders.height]
    return ret
}

fun load(input: ResultSet): BlockHeader
{
    var ret = BlockHeader()
    ret.hashData = Hash256(input.getBytes(PBlockHeaders.hash.name))
    ret.version = input.getInt(PBlockHeaders.version.name)
    ret.hashPrevBlock = Hash256(input.getBytes(PBlockHeaders.hashPrevBlock.name))
    ret.hashMerkleRoot = Hash256(input.getBytes(PBlockHeaders.hashMerkleRoot.name))
    ret.time = input.getLong(PBlockHeaders.time.name)
    ret.diffBits = input.getLong(PBlockHeaders.diffBits.name)
    ret.nonce = input.getLong(PBlockHeaders.nonce.name)
    ret.numTx = input.getLong(PBlockHeaders.numTx.name)
    ret.blockSize = input.getLong(PBlockHeaders.blockSize.name)
    ret.cumulativeWork = input.getBigDecimal(PBlockHeaders.cumulativeWork.name).toBigInteger()
    ret.height = input.getLong(PBlockHeaders.height.name)
    return ret
}

class BlockHeaderDaoImpl(val db:Database):BlockHeaderDao
{
    override fun getAll(): List<BlockHeader>
    {
        throw NotImplementedError("BlockHeaderDaoImpl")
        //return listOf()
    }

    override fun loadAllByHeight(heights: IntArray): List<BlockHeader>
    {
        throw NotImplementedError("BlockHeaderDaoImpl")
        //return listOf()
    }

    override fun get(height: Long): BlockHeader?
    {
        var ret:BlockHeader? = null
        while(true)
        {
            try
            {
                transaction(db) {
                    val query = PBlockHeaders.select { PBlockHeaders.height eq height }
                    for (q in query) {
                        val r:BlockHeader = load(q)
                        if (r.hashData != CHAINTIP_MARKER)
                        {
                            ret = r
                            break
                        }
                    }
                }
                return ret
            }
            catch(e: ExposedSQLException)
            {
                //LogIt.info(e.toString())
                // retry on busy exception
                val sqle = e.cause as SQLiteException
                if (sqle.resultCode.name != "SQLITE_BUSY") throw(e)
            }
        }
    }

    override fun getAtHeight(height: Long): List<BlockHeader>
    {
        var ret:MutableList<BlockHeader> = mutableListOf()

        while(true)
        {
            try
            {
                transaction(db) {
                    val query = PBlockHeaders.select { PBlockHeaders.height eq height }
                    for (q in query)
                    {
                        var tmp = load(q)
                        if (tmp.hashData != CHAINTIP_MARKER)
                            ret.add(tmp)
                    }
                }
                return ret
            }
            catch(e: ExposedSQLException)
            {
                // retry on busy exception
                val sqle = e.cause as SQLiteException
                if (sqle.resultCode.name != "SQLITE_BUSY")
                {
                    //LogIt.info(e.toString())
                    throw(e)
                }
            }
        }
    }

    override fun get(blockid: ByteArray): BlockHeader?
    {
        while (true)
        {
            try
            {
                var ret: BlockHeader? = null
                transaction(db)
                {
                    val query = PBlockHeaders.select { PBlockHeaders.hash eq blockid }
                    for (q in query)   // There can only be one because its the primary key
                    {
                        ret = load(q)
                        if (ret != null) break
                    }
                }
                return ret
            }
            catch (e: ExposedSQLException)
            {
                //LogIt.info(e.toString())
                // retry on busy exception
                val sqle = e.cause as SQLiteException
                if (sqle.resultCode.name != "SQLITE_BUSY")
                    throw(e)
                Thread.sleep(10)
            }
        }
    }

    override fun getLast(): BlockHeader?
    {
        var ret:BlockHeader? = null
        transaction(db) {
            this.exec("SELECT a.* FROM pblockHeaders a LEFT OUTER JOIN pblockHeaders b ON a.height < b.height WHERE b.height IS NULL") {
                    while (it.next()) {
                        ret = load(it)
                        break
                    }
                }
        }
        return ret
    }

    override fun getMostWork(): Array<BlockHeader>
    {
        while (true)
        {
            try
            {

                var ret = mutableListOf<BlockHeader>()
                transaction(db) {

                    this.exec("SELECT a.* FROM pblockHeaders a LEFT OUTER JOIN pblockHeaders b ON a.cumulativeWork < b.cumulativeWork WHERE b.cumulativeWork IS NULL") {
                            while (it.next())
                            {
                                ret.add(load(it))
                            }
                        }
                }

                // remove illegal or well-known hash values.  There are markers and should be repeats of other stored headers
                val r2 = ret.filter({ ((it.hashData.hash.size == 32)&&(it.hashData.hash != CHAINTIP_MARKER.hash)) })
                return r2.toTypedArray()
            }
            catch (e: ExposedSQLException)
            {
                LogIt.info(e.toString())
                // retry on busy exception
                val sqle = e.cause as SQLiteException
                if (sqle.resultCode.name != "SQLITE_BUSY")
                    throw(e)
                Thread.sleep(10)
            }
        }
    }

    override fun insert(vararg bh: BlockHeader)
    {
        for (b in bh)
        {
            try
            {
                transaction(db) {
                    PBlockHeaders.insert {
                        it[hash] = b.hashData.hash
                        it[version] = b.version
                        it[hashPrevBlock] = b.hashPrevBlock.hash
                        it[hashMerkleRoot] = b.hashMerkleRoot.hash
                        it[time] = b.time
                        it[diffBits] = b.diffBits
                        it[nonce] = b.nonce
                        it[numTx] = b.numTx
                        it[blockSize] = b.blockSize
                        it[cumulativeWork] = b.cumulativeWork.toBigDecimal()
                        it[height] = b.height
                    }
                }

            }
            catch(e: ExposedSQLException)
            {
                //LogIt.info(e.toString())
                // retry on busy exception
                val sqle = e.cause as SQLiteException
                if (sqle.resultCode.name == "SQLITE_CONSTRAINT_PRIMARYKEY")
                {
                    LogIt.info("Inserting duplicate block header")
                    return  // Its ok to insert the same block header, but a waste
                }
                if (sqle.resultCode.name != "SQLITE_BUSY")
                    throw(e)

            }
        }
    }

    override fun upsert(vararg bh: BlockHeader)
    {
        for (b in bh)
        {

            val dataIhave = get(b.hashData.hash)
            if (dataIhave == null)
            {
                insert(b)
            }
            else
            {
                transaction(db) {
                    PBlockHeaders.update({ PBlockHeaders.hash eq b.hashData.hash }) {
                        it[version] = b.version
                        it[hashPrevBlock] = b.hashPrevBlock.hash
                        it[hashMerkleRoot] = b.hashMerkleRoot.hash
                        it[time] = b.time
                        it[diffBits] = b.diffBits
                        it[nonce] = b.nonce
                        it[numTx] = b.numTx
                        it[blockSize] = b.blockSize
                        it[cumulativeWork] = b.cumulativeWork.toBigDecimal()
                        it[height] = b.height
                    }
                }
            }
        }

        /*  relies on custom SQL that is database dependent
transaction(db) {
    for (bh in bhlst)
    {
        PBlockHeaders.insertOrUpdate(PBlockHeaders.hash) {
            it[hash] = bh.hashData.hash
            it[version] = bh.version
            it[hashPrevBlock] = bh.hashPrevBlock.hash
            it[hashMerkleRoot] = bh.hashMerkleRoot.hash
            it[time] = bh.time
            it[diffBits] = bh.diffBits
            it[nonce] = bh.nonce
            it[numTx] = bh.numTx
            it[blockSize] = bh.blockSize
            it[cumulativeWork] = bh.cumulativeWork.toBigDecimal()
            it[height] = bh.height
        }
    }
}
 */

        /*  Works but prints a nasty stack trace before that cannot be caught
        try
        {

            // its less efficient to try the insert first since most will update but reversing it fails silently
            transaction(db) {
                PBlockHeaders.insert {
                    it[hash] = bh.hashData.hash
                    it[version] = bh.version
                    it[hashPrevBlock] = bh.hashPrevBlock.hash
                    it[hashMerkleRoot] = bh.hashMerkleRoot.hash
                    it[time] = bh.time
                    it[diffBits] = bh.diffBits
                    it[nonce] = bh.nonce
                    it[numTx] = bh.numTx
                    it[blockSize] = bh.blockSize
                    it[cumulativeWork] = bh.cumulativeWork.toBigDecimal()
                    it[height] = bh.height
                }
            }

        }
        catch(e: ExposedSQLException)
        {
            val sqle = e.cause as SQLiteException
            if (sqle.resultCode.name == "SQLITE_CONSTRAINT_PRIMARYKEY")
            {
            transaction(db) {
                PBlockHeaders.update ({PBlockHeaders.hash eq bh.hashData.hash}) {
                    it[version] = bh.version
                    it[hashPrevBlock] = bh.hashPrevBlock.hash
                    it[hashMerkleRoot] = bh.hashMerkleRoot.hash
                    it[time] = bh.time
                    it[diffBits] = bh.diffBits
                    it[nonce] = bh.nonce
                    it[numTx] = bh.numTx
                    it[blockSize] = bh.blockSize
                    it[cumulativeWork] = bh.cumulativeWork.toBigDecimal()
                    it[height] = bh.height
                }
            }
                return
            }
            if (sqle.resultCode.name != "SQLITE_BUSY")
                throw(e)
        }

         */
    }

    override fun delete(bh: BlockHeader)
    {
         transaction(db) {
             PBlockHeaders.deleteWhere { PBlockHeaders.hash eq bh.hash.hash }
         }
    }

    override fun delete(height: Long)
    {

    }

    override fun deleteAll()
    {
        try
        {
            transaction(db) {
                PBlockHeaders.deleteAll()
            }
        }
        catch(e: ExposedSQLException)
        {
            LogIt.info(e.toString())
            // retry on busy exception
            val sqle = e.cause as SQLiteException
            if (sqle.resultCode.name != "SQLITE_BUSY") throw(e)
        }
    }

}

fun BlockHeaderDao.getCachedTip(): BlockHeader?
{
    val ret = get(CHAINTIP_MARKER.hash)
    ret?.calcHash()
    return ret
}

fun BlockHeaderDao.setCachedTip(header: BlockHeader)
{
    val svhashData = header.hashData
    header.hashData = CHAINTIP_MARKER
    upsert(header)
    header.hashData = svhashData
}

abstract class BlockHeaderDatabase
{
    abstract fun blockHeaderDao(): BlockHeaderDao

    abstract fun clearAllTables()
}

fun clearAllTables(db: BlockHeaderDatabase)
{
    db.clearAllTables()
}


class DesktopDb(name: String):BlockHeaderDatabase()
{
    //val db = Database.connect("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", driver = "org.h2.Driver")
    val db:Database = Database.connect("jdbc:sqlite:" + name + ".db", "org.sqlite.JDBC")
    // There can be only one sqlite connection or you get SQLITE_BUSY exceptions when multiple connections try to
    // simultaneously access.  So create just 1 dao object and always return it when asked
    val dao = BlockHeaderDaoImpl(db)

    init
    {
        // for sqlite:
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE // Or Connection.TRANSACTION_READ_UNCOMMITTED

        transaction(db) {
            addLogger(StdOutSqlLogger)
            SchemaUtils.create(PBlockHeaders)  // for multiple tables add as params
        }
        LogIt.info("table create complete\n")
    }
    override fun blockHeaderDao(): BlockHeaderDao
    {
        return dao
    }

    override fun clearAllTables()
    {
        TODO("Not yet implemented")
    }
}

object PBlockHeaders: Table()
{
    val hash = binary("hash", SHA256_HASH_LEN)
    val version: Column<Int> = integer("version")
    val hashPrevBlock = binary("hashPrevBlock", SHA256_HASH_LEN)
    val hashMerkleRoot = binary("hashMerkleRoot", SHA256_HASH_LEN)
    val time: Column<Long> = long("time")
    val diffBits: Column<Long> = long("diffBits")
    val nonce: Column<Long> = long("nonce")

    val numTx: Column<Long> = long("numTx")
    val blockSize: Column<Long> = long("blockSize")
    val cumulativeWork = decimal("cumulativeWork", MAX_SQL_PRECISION, 0)
    val height: Column<Long> = long("height")

    override val primaryKey = PrimaryKey(hash, name = "hash")
}

fun OpenBlockHeaderDB(context: PlatformContext, name: String): BlockHeaderDatabase?
{
    return DesktopDb(name)
}

@Synchronized  // synchronized so that the get and insert are atomic WRT other threads calling this
fun PersistInsert(dbdao: BlockHeaderDao, bh: BlockHeader)
{
    val curbh = dbdao.get(bh.hash.hash)
    if (curbh == null)
        dbdao.insert(bh)
    else
    {
        if (curbh != bh)
        {
            LogIt.warning("Same hash, different header! ${bh.hash.toHex()}")
        }
        dbdao.upsert(bh)
    }
}

class KvpData()
{
    var id: ByteArray = byteArrayOf()
    var value: ByteArray = byteArrayOf()

    constructor(k:ByteArray, v:ByteArray):this()
    {
        id = k
        value = v
    }
}


object KvpTable: Table()
{
    val MAX_KEY_SIZE = 64
    val id = binary("id", MAX_KEY_SIZE) //.primaryKey()
    val value = blob("value")
    override val primaryKey = PrimaryKey(id, name = "id")
}


interface KvpDao
{
    //@Query("SELECT * FROM KvpData WHERE id = :key")
    abstract fun get(key: ByteArray): KvpData?

    //@Insert
    fun insert(bh: KvpData)

    //@Update
    fun update(bh: KvpData)

    //@Delete
    fun delete(bh: KvpData)

    //@Query("DELETE FROM KvpData")
    fun deleteAll()
}

class KvpDaoImpl(val db:Database):KvpDao
{
    //@Query("SELECT * FROM KvpData WHERE id = :key")
    override fun get(key: ByteArray): KvpData?
    {
        while(true)
        {
            try
            {
                var ret:ByteArray? = null
                transaction(db) {
                    val query = KvpTable.select { KvpTable.id eq key }
                    for (q in query) {
                        //ret = q[KvpTable.value].getBytes(1,q[KvpTable.value].length().toInt())
                        val blb = q[KvpTable.value]
                        ret = blb.bytes

                        //ret = blb.getBytes(1.toLong(), blb.length().toInt())
                        break
                    }
                }
                val r = ret
                if (r != null)
                {
                    return KvpData(key, r.clone())
                }
                else return null
                
            }
            catch(e: ExposedSQLException)
            {
                // retry on busy exception
                val sqle = e.cause as SQLiteException
                if (sqle.resultCode.name != "SQLITE_BUSY")
                {
                    throw(e)
                }
            }
        }
    }

    //@Insert
    override fun insert(bh: KvpData)
    {
        while (true)
        {
            try
            {
                transaction(db) {
                    KvpTable.insert {
                        it[id] = bh.id
                        it[value] = ExposedBlob(bh.value)
                    }
                }
                break
            }
            catch (e: ExposedSQLException)
            {
                // retry on busy exception
                val sqle = e.cause as SQLiteException
                if (sqle.resultCode.name != "SQLITE_BUSY")
                {
                    throw(e)
                }
            }
        }
    }

    //@Update
    override fun update(bh: KvpData)
    {
        while (true)
        {
            try
            {
                transaction(db) {
                    KvpTable.update({ KvpTable.id eq bh.id }) {
                        it[value] = ExposedBlob(bh.value)
                    }
                }
                break
            }
            catch (e: ExposedSQLException)
            {
                // retry on busy exception
                val sqle = e.cause as SQLiteException
                if (sqle.resultCode.name != "SQLITE_BUSY")
                {
                    throw(e)
                }
            }
        }
    }

    //@Delete
    override fun delete(bh: KvpData)
    {
        while (true)
        {
            try
            {

                transaction(db) {
                    KvpTable.deleteWhere { KvpTable.id eq bh.id }
                }
                break
            }
            catch (e: ExposedSQLException)
            {
                // retry on busy exception
                val sqle = e.cause as SQLiteException
                if (sqle.resultCode.name != "SQLITE_BUSY") throw(e)
            }
        }
    }

    //@Query("DELETE FROM KvpData")
    override fun deleteAll()
    {
        while (true)
        {
            try
            {
                transaction(db) {
                    PBlockHeaders.deleteAll()
                }
                break
            }
            catch (e: ExposedSQLException)
            {
                // retry on busy exception
                val sqle = e.cause as SQLiteException
                if (sqle.resultCode.name != "SQLITE_BUSY") throw(e)
            }
        }
    }
}


fun KvpDao.upsert(d: KvpData): Boolean
{
    // alg: update then insert on exception doesn't work: updating a nonexistent key silently fails.
    // alg: insert then update on exception doesn't work: exception is printed to stdout
    val exists = get(d.id)
    if (exists == null) insert(d)
    else update(d)
    return true
}

abstract class KvpDatabase
{
    abstract fun dao(): KvpDao

    /** update or insert a key value pair into the database */
    fun set(key: ByteArray, value: ByteArray) = dao().upsert(KvpData(key, value))
    /** look up the passed key, throwing DataMissingException if it does not exist */
    fun get(key: ByteArray): ByteArray
    {
        val kvp = dao().get(key)
        if (kvp == null) throw DataMissingException()
        return kvp.value
    }
    /** look up the passed key, returning the value or null if it does not exist */
    fun getOrNull(key: ByteArray): ByteArray?
    {
        val kvp = dao().get(key)
        if (kvp == null) return null
        return kvp.value
    }

    /** update or insert a key value pair into the database */
    fun set(key: String, value: ByteArray) = set(key.toByteArray(), value)
    /** look up the passed key, returning the value or throwing DataMissingException */
    fun get(key: String): ByteArray = get(key.toByteArray())
    /** look up the passed key, returning the value or null if it does not exist */
    fun getOrNull(key: String): ByteArray? = getOrNull(key.toByteArray())

    /** delete a record */
    fun delete(key: String) = delete(key.toByteArray())

    /** delete a record */
    fun delete(key:ByteArray)
    {
        dao().delete(KvpData(key,byteArrayOf()))
    }
}

class KvpDb(name: String):KvpDatabase()
{
    //val db = Database.connect("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", driver = "org.h2.Driver")
    val db:Database = Database.connect("jdbc:sqlite:" + name + ".db", "org.sqlite.JDBC")
    // There can be only one sqlite connection or you get SQLITE_BUSY exceptions when multiple connections try to
    // simultaneously access.  So create just 1 dao object and always return it when asked
    val thedao = KvpDaoImpl(db)

    init
    {
        // for sqlite:
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE // Or Connection.TRANSACTION_READ_UNCOMMITTED

        transaction(db) {
            // addLogger(StdOutSqlLogger)
            SchemaUtils.create(KvpTable)  // for multiple tables add as params
        }
        // LogIt.info("table create complete\n")
    }
    override fun dao(): KvpDao
    {
        return thedao
    }
}

fun OpenKvpDB(context: PlatformContext, name: String): KvpDatabase?
{
   return KvpDb(name)
}
