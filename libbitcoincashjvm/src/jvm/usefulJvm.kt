// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash

import java.util.logging.FileHandler
import java.util.logging.Level
import java.util.logging.Logger
import java.util.logging.SimpleFormatter

var backingLog: Logger? = null

private val LogIt = GetLog("BU.usefulJvm")

var xlatMap: Map<Int, String> = mapOf(1 to "a")

// Lookup strings in strings.xml
fun i18n(id: Int):String
{
    val s = xlatMap[id]
    if (s != null) return s

    LogIt.severe("Missing strings.xml translation for " + id.toString())
    return "STR" + id.toString()
}


class PlatformContext  // similar to android context
{
    var filesDir = "todo"
}

fun iHaveInternet():Boolean
{
    // todo
    return true
}

@Synchronized
fun GetLog(module:String): Logger
{
    backingLog?.let { return it }
    val l = GetzLog("BU")
    backingLog = l
    return l
}


fun GetzLog(module: String): Logger
{
    //if (theLog != null) return theLog!!

    val LogIt = Logger.getLogger(module)
    LogIt.useParentHandlers = false
    val h = FileHandler("dwally.log", 256*1024*1024, 10, true)
    h.formatter = SimpleFormatter()
    LogIt.addHandler(h)
    LogIt.level = Level.ALL
    return LogIt
}